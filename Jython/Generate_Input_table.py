'''


'''

import os
import re

from automic.table import TableModel


#@ File (label = "Folder with input data", value="",style="directory") inputDataDirectory
#@ String (label = "Image file extension", value="nd2") imageFileExtension

rootFolder=inputDataDirectory.getParent()

tbModel = TableModel(rootFolder)
tbModel.addFileColumns('RAW','IMG')
tbModel.addValueColumn("Success", "BOOL")

pattern = re.compile('(.*)\.%s' %imageFileExtension)

for root, directories, filenames in os.walk(inputDataDirectory.getAbsolutePath()):
    for filename in filenames:
        match = re.search(pattern, filename)
        if match is not None:
            tbModel.addRow()
            rowIndex=tbModel.getRowCount()-1
            tbModel.setFileAbsolutePath(root, filename, rowIndex, "RAW","IMG")
            tbModel.setBooleanValue(True, rowIndex, "Success")

tbModel.writeNewFile("summary_RAW.txt",True)

print 'Script finished'

