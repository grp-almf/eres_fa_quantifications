from ij import IJ
from ij.process import ImageConverter

from  net.haesleinhuepf.clij2 import CLIJ2
#from net.haesleinhuepf.clij.clearcl import ClearCLBuffer
from net.haesleinhuepf.clij.utilities import CLKernelExecutor


sm_xy=1.0            #median filter to remove noise
sm_z=0.0

sm_back_xy=3.0      #median filter to create background
sm_back_z=2.0

lmax_xy=3.0          #local maximum filter to create seeds
lmax_z=2.0

if __name__ in ('__main__','__builtin__'):
    
    originalImage=IJ.getImage()
    
    clij2 = CLIJ2.getInstance()
        
    CLKernelExecutor.MAX_ARRAY_SIZE=(int)(sm_back_xy*sm_back_xy*sm_back_z*100)
    
    originalImage32=originalImage.duplicate()
    ImageConverter(originalImage32).convertToGray32()
    
    originalBuffer = clij2.push(originalImage32)
    smoothBuffer = clij2.create(originalBuffer)
    backBuffer = clij2.create(originalBuffer)
    cleanBuffer = clij2.create(originalBuffer)
    seedsBuffer = clij2.create(originalBuffer)
    
    
    clij2.median3DBox(originalBuffer, smoothBuffer, sm_xy, sm_xy, sm_z)
    clij2.median3DBox(originalBuffer, backBuffer, sm_back_xy, sm_back_xy, sm_back_z)
    clij2.subtractImages(smoothBuffer, backBuffer, cleanBuffer)

    clij2.detectMaxima3DBox(cleanBuffer, smoothBuffer, lmax_xy, lmax_xy, lmax_z)
    clij2.multiplyImages(cleanBuffer, smoothBuffer, seedsBuffer)
    
    imageSeeds=clij2.pull(seedsBuffer)
    imageSeeds.show()
    imageClean=clij2.pull(cleanBuffer)
    imageClean.show()
     
    clij2.release(originalBuffer)
    clij2.release(smoothBuffer)
    clij2.release(backBuffer)
    clij2.release(cleanBuffer)
    clij2.release(seedsBuffer)
    
    print 'Processing Finished'
