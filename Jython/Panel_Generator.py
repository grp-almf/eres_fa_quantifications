
from ij import IJ,ImagePlus,ImageStack,WindowManager
from ij.plugin import MontageMaker

montageRows=1
montageColumns=2
montageBorder=2

gausFilterRadius=1.0

minValue=970
maxValue=7000

pixelsInMicron=10.6231
    
    
if __name__ == '__main__':

	print("Start Script")

	imageTitles=WindowManager.getImageTitles()
	imageTitles=sorted(imageTitles)

	imageTitles = [title for title in imageTitles if title.startswith('0')]

	firstImage=WindowManager.getImage(imageTitles[0])
	floatStack= ImageStack(firstImage.getWidth(), firstImage.getHeight())
	byteStack= ImageStack(firstImage.getWidth(), firstImage.getHeight())

	for title in imageTitles:
		currentImage=WindowManager.getImage(title).duplicate()
		IJ.run(currentImage, "Gaussian Blur...", "sigma=%f" %gausFilterRadius)
		currentProcessor=currentImage.getProcessor()
		currentProcessor.setMinAndMax(minValue,maxValue)
		floatStack.addSlice(currentProcessor)
		btProcessor=currentProcessor.convertToByteProcessor(True)
		byteStack.addSlice(btProcessor)

	floatStackImage=ImagePlus('Float Stack', floatStack)
	floatStackImage.show()

	byteStackImage=ImagePlus('Byte Stack', byteStack)
	byteStackImage.show()


	montageImage=MontageMaker().makeMontage2(byteStackImage,montageColumns,montageRows,1,1,byteStack.getSize(),1,montageBorder,False)

	IJ.run(montageImage, "Set Scale...", "distance=%f known=1 unit=micron" %pixelsInMicron)
	#IJ.run(montageImage, "Scale Bar...", "width=10 height=5 font=18 color=White background=None location=[Lower Right] bold hide")


	montageImage.show()
	
	print("End Script")