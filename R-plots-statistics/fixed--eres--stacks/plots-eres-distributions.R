#Plot Time-lapse data of quantified Stc1 intensities
#e-mail: halavatyi@gmail.com.
#Last update: May 30,2017.


if(!require(ggplot2)) install.packages("ggplot2")
if(!require(GGally)) install.packages("GGally")


rm(list=ls())#clear all variables

rootw<-'root\\'

#
inputPath<-dataPath<-'C:/tempDat/STC1_project_(FV)/siRNA--HelaKyoto/20170412--HelaKyoto--Sec31-A568--Sec24B-A488--fixed--siRNA--49h/Analysis-20170417--selected-cells-20170607'
inputFileName<-'analysisSummary_Step_QuatifyCellAndEresDistribIsolated_step12_final.txt'

r.Output.Folder<-'R.Output'

setwd(inputPath)


if (dir.exists(r.Output.Folder)){
  unlink(r.Output.Folder,recursive = TRUE)  
}
dir.create(r.Output.Folder)

#open summary file
Summary.Data<-read.delim(inputFileName,header=T,sep="\t",as.is=T,na.strings='')


ndata=nrow(Summary.Data)

library("BBmisc")

## merge data to single Table
eres.Data.Combined<-data.frame(Treatment=vector())
for(idataset in 1:ndata){

  relative.Path.Eres=file.path(gsub(rootw,'',Summary.Data[idataset,'PathName_Eres.Values_TXT'],fixed=T),Summary.Data[idataset,'FileName_Eres.Values_TXT'],fsep='\\')
  eres.Data<-read.delim(relative.Path.Eres,header=T,sep="\t",as.is=T,na.strings='')
  eres.Data$Treatment<-Summary.Data$Treatment_FACT[idataset]
  eres.Data$Cell.ID<-substr(Summary.Data$FileName_SingleCell.Roi_ROI[idataset],9,nchar(Summary.Data$FileName_SingleCell.Roi_ROI[idataset])-4)
  
  eres.Data.Combined<-rbind(eres.Data.Combined,eres.Data)
}

Dataset.IDs<-unique(eres.Data.Combined[,c('Treatment','Cell.ID')])

##create plots
nrowPdf=3
ncolPdf=2

p_list<-list()
i=1;



# histogram plots
library(ggplot2)
for (iDataset in 1:nrow(Dataset.IDs)){
  dataset.Data<-eres.Data.Combined[eres.Data.Combined$Treatment==Dataset.IDs$Treatment[iDataset] & eres.Data.Combined$Cell.ID== Dataset.IDs$Cell.ID[iDataset],]

  p_list[[i]]<-ggplot(data=dataset.Data, aes(x=Intensity_NUM)) +
    geom_histogram()+#geom_histogram(binwidth=1e5)+
    xlab('ERES_Intensity')+
    scale_x_log10(limits = c(10000, max(eres.Data.Combined$Intensity_NUM)))+
    ggtitle(paste(dataset.Data$Treatment[iDataset],dataset.Data$Cell.ID[iDataset],sep='--'))
  i=i+1

  p_list[[i]]<-ggplot(data=dataset.Data, aes(x=Volume_NUM)) +
    geom_histogram(binwidth=20)+
    xlab('ERES_Volume')+
    scale_x_continuous(limits = c(1, max(eres.Data.Combined$Volume_NUM)))+
    ggtitle(paste(dataset.Data$Treatment[iDataset],dataset.Data$Cell.ID[iDataset],sep='--'))
  i=i+1
  

}

library(gridExtra)
m_plots<-marrangeGrob(p_list,nrow=nrowPdf,ncol=ncolPdf)
ggsave(filename = file.path(r.Output.Folder,'Intensity_plots_log.pdf'),plot=m_plots,width=300,height=90*3,units='mm')

# correlaton matrices plot
correlation.Parameters<-c('Intensity_NUM','Volume_NUM','Area_NUM')
n.Correlation.Parameters<-length(correlation.Parameters)
max.Parameter.Values<-apply(eres.Data.Combined[,correlation.Parameters],2,max)
min.Parameter.Values<-apply(eres.Data.Combined[,correlation.Parameters],2,min)


library(GGally)
pdf(file.path(r.Output.Folder,'Correlation matrix.pdf'),width=10,height = 10)
for (iDataset in 1:nrow(Dataset.IDs)){
  dataset.Data<-eres.Data.Combined[eres.Data.Combined$Treatment==Dataset.IDs$Treatment[iDataset] & eres.Data.Combined$Cell.ID== Dataset.IDs$Cell.ID[iDataset],]
  
  p<-ggpairs(dataset.Data[,correlation.Parameters],title = paste(dataset.Data$Treatment[iDataset],dataset.Data$Cell.ID[iDataset],sep='--'))
  for (j in 1:n.Correlation.Parameters){
    p[j,j]<-p[j,j]+ scale_x_log10(limits = c(min.Parameter.Values[j], max.Parameter.Values[j]))
    row.Indexes<-(j:n.Correlation.Parameters)[-1]
    for (i in row.Indexes){
      p[i,j]<-p[i,j]+scale_y_log10(limits = c(min.Parameter.Values[i], max.Parameter.Values[i]))
      if (j<4){
        p[i,j]<-p[i,j]+scale_x_log10(limits = c(min.Parameter.Values[j], max.Parameter.Values[j]))
      }else{
        p[i,j]<-p[i,j]+scale_x_continuous(limits = c(0, max.Parameter.Values[j]))
      }
    }
  }
  
  print(p)
}
dev.off()

#https://stackoverflow.com/questions/30720455/how-to-set-same-scales-across-different-facets-with-ggpairs
#implement the same scales for variables on different plots

cat('Script end')
