#1.
#2.
#3.
#Author: Aliaksandr Halavatyi.      
#e-mail: halavatyi@gmail.com.
#Last update: December 12, 2016.

#Check for/upload required packages
# b_plotrix=require(plotrix)
# b_ggplot=require(ggplot2)
# b_gridExtra=require(gridExtra)
# if(!(b_ggplot&&b_gridExtra&&b_plotrix)){#some packages are missing, let's install them
#   source("http://bioconductor.org/biocLite.R")
#   if(!b_plotrix)
#     biocLite("plotrix",ask=F)
#   
#   if(!b_ggplot)
#     biocLite("ggplot2",ask=F)
#   if(!b_gridExtra)
#     biocLite("reshape",ask=F)
# }

rm(list=ls())#clear all variables

globalOutputPath<-'C:/Users/halavatyi/Desktop/Figures Fatima 09122016/Plot_generation/test1'
dir.create(globalOutputPath)



#### I. Drug in Sec23-YFP (130F5) cell line
####Currently 3 replicates mixed together



###input parameters
dataPath1<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Drug--Sec23-YFP/24062016--Sec23 stacks--fixed/Analysis1-03082016/Final_checked_table.txt'
dataPath2<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Drug--Sec23-YFP/29062016--Sec23 stacks--fixed/Analysis1-05082016/Final table checked all.txt'
dataPath3<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Drug--Sec23-YFP/04072016--Sec23 stacks--fixed/Analysis1-06082016/Final table checked all.txt'

PlotData<-read.delim(dataPath1,header=T,sep="\t",as.is=T,na.strings='')
PlotData<-rbind(PlotData,read.delim(dataPath2,header=T,sep="\t",as.is=T,na.strings=''))
PlotData<-rbind(PlotData,read.delim(dataPath3,header=T,sep="\t",as.is=T,na.strings=''))

#prepare reconditioning Data Frame
Treatment_FACT    <-c('E' ,'2E','A' ,'2A','G' ,'2G')
Drug_FACT         <-c('E' ,'E' ,'A' ,'A' ,'G' ,'G' )
Concentration_FACT<-c('1x','2x','1x','2x','1x','2x')

drugLevels<-c('E','A','G')
concentrationLevels<-c('1x','2x')


Refactoring<-data.frame(Treatment_FACT,Drug_FACT,Concentration_FACT)

PlotData<-PlotData[PlotData$Success_BOOL==1,]
PlotData<-PlotData[PlotData$Treatment_FACT %in% Treatment_FACT,]

Refactoring$Treatment_FACT    <-factor(Refactoring$Treatment_FACT    ,levels=Treatment_FACT     )
Refactoring$Drug_FACT         <-factor(Refactoring$Drug_FACT         ,levels=drugLevels         )
Refactoring$Concentration_FACT<-factor(Refactoring$Concentration_FACT,levels=concentrationLevels)





#Calculate ERES data
PlotData$ERES.Integrated.Intensity_NUM<-PlotData$Cell.Integrated.Intensity_NUM*PlotData$Eres.Fraction_NUM
PlotData$Intensity.Per.ERES_NUM<-PlotData$ERES.Integrated.Intensity_NUM/PlotData$Eres.Count_NUM
PlotData[is.nan(PlotData$Intensity.Per.ERES_NUM),'Intensity.Per.ERES_NUM']=NA
str(PlotData)


PlotData$rowIndex<-1:nrow(PlotData)
PlotData$Treatment_FACT<-factor(PlotData$Treatment_FACT,levels = Treatment_FACT)
PlotDataRefactored<-merge(PlotData,Refactoring,by='Treatment_FACT')
PlotDataRefactored<-PlotDataRefactored[order(PlotDataRefactored$rowIndex),]
str(PlotDataRefactored)


#DataCellsIdentified<-DataRefactored[!is.na(DataRefactored$FileName_SingleCell.Roi_ROI),]

tsveta<-c('white','cyan','orange')

aggregationVariables<-c('Eres.Count_NUM','Eres.Fraction_NUM','ERES.Integrated.Intensity_NUM','Intensity.Per.ERES_NUM')
upperLimits<-c(3000,0.4,2e09,1e06)

# plot test
grouping.factor='Drug_FACT'
condition.factor='Concentration_FACT'

dodge <- position_dodge(width = 0.75)
p_list<-list()


p_list[[1]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Eres.Count_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 3000),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[1]]

p_list[[2]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Eres.Fraction_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 0.4),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[2]]

p_list[[3]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=ERES.Integrated.Intensity_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 2e09),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[3]]

p_list[[4]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Intensity.Per.ERES_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 1e6),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[4]]




#### II. Drug in HelaKyoto cell line
####Currently 3 replicates mixed together



###input parameters
dataPath1<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Drug--HeLaKyoto/17082016--HelaKyoto--drugs--stacks--fixed/Analysis-quantify_ERES_2_channles/Mamual_check_all.txt'

PlotData<-read.delim(dataPath1,header=T,sep="\t",as.is=T,na.strings='')

#prepare reconditioning Data Frame
Treatment_FACT    <-c('E' ,'2E','A' ,'2A','G' ,'2G')
Drug_FACT         <-c('E' ,'E' ,'A' ,'A' ,'G' ,'G' )
Concentration_FACT<-c('1x','2x','1x','2x','1x','2x')

drugLevels<-c('E','A','G')
concentrationLevels<-c('1x','2x')


Refactoring<-data.frame(Treatment_FACT,Drug_FACT,Concentration_FACT)

PlotData<-PlotData[PlotData$Success_BOOL==1,]
PlotData<-PlotData[PlotData$Treatment_FACT %in% Treatment_FACT,]

Refactoring$Treatment_FACT    <-factor(Refactoring$Treatment_FACT    ,levels=Treatment_FACT     )
Refactoring$Drug_FACT         <-factor(Refactoring$Drug_FACT         ,levels=drugLevels         )
Refactoring$Concentration_FACT<-factor(Refactoring$Concentration_FACT,levels=concentrationLevels)





#Calculate ERES data
PlotData$ERES.A568.Integrated.Intensity_NUM<-PlotData$Cell.A568.Integrated.Intensity_NUM*PlotData$Eres.A568.Fraction_NUM
PlotData$Intensity.A568.Per.ERES_NUM<-PlotData$ERES.A568.Integrated.Intensity_NUM/PlotData$Eres.A568.Count_NUM
#PlotData[is.nan(PlotData$Intensity.Per.ERES_NUM),'Intensity.Per.ERES_NUM']=NA
str(PlotData)


PlotData$rowIndex<-1:nrow(PlotData)
PlotData$Treatment_FACT<-factor(PlotData$Treatment_FACT,levels = Treatment_FACT)
PlotDataRefactored<-merge(PlotData,Refactoring,by='Treatment_FACT')
PlotDataRefactored<-PlotDataRefactored[order(PlotDataRefactored$rowIndex),]
str(PlotDataRefactored)


#DataCellsIdentified<-DataRefactored[!is.na(DataRefactored$FileName_SingleCell.Roi_ROI),]

tsveta<-c('white','cyan','orange')



# plot test
grouping.factor='Drug_FACT'
condition.factor='Concentration_FACT'

dodge <- position_dodge(width = 0.75)
p_list<-list()


p_list[[1]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Eres.A568.Count_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 5000),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[1]]

p_list[[2]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Eres.A568.Fraction_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 0.5),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[2]]

p_list[[3]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=ERES.A568.Integrated.Intensity_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 4e09),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[3]]

p_list[[4]]<-ggplot(PlotDataRefactored, aes(x=Concentration_FACT, y=Intensity.A568.Per.ERES_NUM,fill =Drug_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 1.25e6),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[4]]




#i=1;
# for (variable in aggregationVariables){
#   # nameBarValues<-paste(variable,'mean',sep='.')
#   # nameErrValues<-paste(variable,'std.error',sep='.')
#   # 
#   # 
#   # p_single<-ggplot(experimentSummary, aes_string(x=condition.factor, y=nameBarValues,fill =grouping.factor)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
#   #   geom_bar(stat="identity", position = "dodge")+ 
#   #   scale_fill_brewer(palette = "Set1")+
#   #   theme(legend.position='top')+
#   #   geom_errorbar(aes_string(ymax = paste(nameBarValues,'+',nameErrValues), ymin=paste(nameBarValues,'-',nameErrValues)), position = dodge,width=0.25)
#   # p_list[[i]]<-p_single
#   # i=i+1;
#   
#   pb_single<-ggplot(PlotDataRefactored, aes_string(x=condition.factor, y=variable,fill =grouping.factor)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
#     scale_x_discrete()+
#     geom_boxplot(outlier.color = NA,position = dodge)+
#     scale_fill_brewer(palette = "Set1")+
#     geom_point(position=position_jitterdodge(dodge.width=0.9),size=0.5)+
#     theme(legend.position='top')
#   
#     
#   p_list[[i]]<-pb_single
#   i=i+1;
#   
# }

m_plots<-marrangeGrob(p_list,nrow=1,ncol=1)
ggsave(filename = file.path(globalOutputPath,'plots_drugs_130.pdf'),plot=m_plots,width=150,height=150,units='mm')

# p_eresCount<-ggplot(experimentSummary, aes_string(x=condition.factor, y=nameBarValues,fill =grouping.factor)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
#   geom_bar(stat="identity", position = "dodge")+ 
#   scale_fill_brewer(palette = "Set1")+
#   theme(legend.position='top')+
#   geom_errorbar(aes_string(ymax = nameBarValues + nameErrValues, ymin=nameBarValues - nameErrValues), position = position_dodge(width=0.9),width=0.25)
# 
# p_eresFraction<-ggplot(experimentSummary, aes(x=Drug_FACT, y=Eres.Fraction_NUM.mean, fill = Concentration_FACT)) + 
#   geom_bar(stat="identity", position = "dodge")+ 
#   scale_fill_brewer(palette = "Set1")+
#   theme(legend.position='top')+
#   geom_errorbar(aes(ymax = Eres.Fraction_NUM.mean + Eres.Fraction_NUM.std.error, ymin=Eres.Fraction_NUM.mean - Eres.Fraction_NUM.std.error), position = position_dodge(width=0.9),width=0.25)
# 
# dodge <- position_dodge(width = 0.9)
# #DataCellsSelected$numericPosition<-as.numeric(DataCellsSelected$Drug_FACT)+3*(as.numeric(DataCellsSelected$Concentration_FACT)-0.2)
# ggplot(DataCellsSelected, aes(x=Drug_FACT, y=Eres.Fraction_NUM, fill = Concentration_FACT)) + 
#   scale_x_discrete()+
#   geom_boxplot(outlier.color = NA,position = dodge)+
#   scale_fill_brewer(palette = "Set1")+
#   geom_point(position=position_jitterdodge(dodge.width=0.9))+
#   theme(legend.position='top')
# 
#   
# 
# p_all<-grid.arrange(p_eresCount, p_eresFraction, ncol = 2)
# 
# ggsave(filename = file.path(outputDirectoryName,'barplots.pdf'),plot=p_all)


#### III. siRNA in Sec23-YFP (130F5) cell line




###input parameters
dataPath1<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/siRNA--Sec23-YFP/02122016--Sec23-YFP--Sec31-A568--fixed--siRNA/Analysis-03122016/Manually_checked_Table.txt'
#dataPath2<-'C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/siRNA--Sec23-YFP/04112016--Sec23-YFP--Sec31-A568--fixed--siRNA/Analysis-23112016/Manually_checked_table.txt'

PlotData<-read.delim(dataPath1,header=T,sep="\t",as.is=T,na.strings='')
#PlotData<-rbind(PlotData,read.delim(dataPath2,header=T,sep="\t",as.is=T,na.strings=''))

#prepare reconditioning Data Frame
Treatment_FACT    <-c('Neg9' ,'119')
Drug_FACT         <-c('E' ,'E' ,'A' ,'A' ,'G' ,'G' )
Concentration_FACT<-c('1x','2x','1x','2x','1x','2x')

siRNA.Levels<-c('Neg9','119')
transfection.Time.Levels<-c('49h')


#Refactoring<-data.frame(Treatment_FACT,Drug_FACT,Concentration_FACT)

PlotData<-PlotData[PlotData$Success_BOOL==1,]
PlotData<-PlotData[PlotData$Treatment_FACT %in% siRNA.Levels,]
PlotData<-PlotData[PlotData$SiRNA.Time_FACT %in% transfection.Time.Levels,]

PlotData$Treatment_FACT<-factor(PlotData$Treatment_FACT    ,levels=Treatment_FACT     )
PlotData$SiRNA.Time_FACT<-factor(PlotData$SiRNA.Time_FACT  ,levels= transfection.Time.Levels         )
#Refactoring$Concentration_FACT<-factor(Refactoring$Concentration_FACT,levels=concentrationLevels)


#Calculate ERES data
PlotData$ERES.A568.Integrated.Intensity_NUM<-PlotData$Cell.A568.Integrated.Intensity_NUM*PlotData$Eres.A568.Fraction_NUM
PlotData$Intensity.A568.Per.ERES_NUM<-PlotData$ERES.A568.Integrated.Intensity_NUM/PlotData$Eres.A568.Count_NUM
#PlotData[is.nan(PlotData$Intensity.Per.ERES_NUM),'Intensity.Per.ERES_NUM']=NA
str(PlotData)

PlotData$rowIndex<-1:nrow(PlotData)
PlotData$Treatment_FACT<-factor(PlotData$Treatment_FACT,levels = Treatment_FACT)
#PlotDataRefactored<-merge(PlotData,Refactoring,by='Treatment_FACT')
#PlotDataRefactored<-PlotDataRefactored[order(PlotDataRefactored$rowIndex),]
PlotDataRefactored<-PlotData
str(PlotDataRefactored)


#DataCellsIdentified<-DataRefactored[!is.na(DataRefactored$FileName_SingleCell.Roi_ROI),]

tsveta<-c('white','cyan')

p_list<-list()


p_list[[1]]<-ggplot(PlotDataRefactored, aes(x=SiRNA.Time_FACT, y=Eres.A568.Count_NUM,fill =Treatment_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 2000),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[1]]

p_list[[2]]<-ggplot(PlotDataRefactored, aes(x=SiRNA.Time_FACT, y=Eres.A568.Fraction_NUM,fill =Treatment_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 0.5),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[2]]

p_list[[3]]<-ggplot(PlotDataRefactored, aes(x=SiRNA.Time_FACT, y=ERES.A568.Integrated.Intensity_NUM,fill =Treatment_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 1.3e09),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[3]]

p_list[[4]]<-ggplot(PlotDataRefactored, aes(x=SiRNA.Time_FACT, y=Intensity.A568.Per.ERES_NUM,fill =Treatment_FACT)) + #aes(x=Drug_FACT, y=Eres.Count_NUM.mean, fill = Concentration_FACT))
  scale_y_continuous(limits = c(0, 1.25e6),expand = c(0.01,0))+#ylim(0,3000)+
  theme_bw()+
  scale_x_discrete(expand = c(0,0.1))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.75),size=1)+
  theme(panel.background= element_rect(color = 'black',size=1.1),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))
p_list[[4]]
