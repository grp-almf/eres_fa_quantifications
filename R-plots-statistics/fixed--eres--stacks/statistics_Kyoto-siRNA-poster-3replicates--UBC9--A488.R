

rm(list=ls())#clear all variables





# data input
inputPathFormat<-'C:/tempDat/STC1_project_(FV)/drug_siRNA--HelaKyoto/%s--HelaKyoto--Sec24B-A488--Sec31A-A568--siRNA--49h--drug/%s/R.output max threshold poster-A488/Merged_Data.csv'
replicateDatasets<-list(c('20170901','Analysis-20170902--siRNAf'),c('20170902','Analysis-20170903--siRNAf'),c('20170904','Analysis-20170906--siRNAf'))#,c('20170407','Analysis-20170802--all-good')###c('20170208','Analysis-20170803'),c('20170412','Analysis-20170417--all-good-cells-20170620')
nReplicates=3

setwd('c:/')
globalOutputPath<-gettextf('C:/tempDat/STC1_project_(FV)/drug_siRNA--HelaKyoto/3_replicates_A488_20170910',nReplicates)
if(file.exists(globalOutputPath)){
  unlink(globalOutputPath,recursive = TRUE)
}    
dir.create(globalOutputPath)
setwd(globalOutputPath)


library(plyr)
siRNA.Names<-c('Neg9','UBC9')
## merge data in single table
Combined.Data<-data.frame()
for (rep.Dataset in replicateDatasets){
  dataset.File.Path<-gettextf(inputPathFormat,rep.Dataset[1],rep.Dataset[2])
  cat (dataset.File.Path,'\n')
  cat (file.exists(dataset.File.Path),'\n')
  Rep.Data<-read.csv(dataset.File.Path,as.is = T)
  Combined.Data<-rbind.fill(Combined.Data,read.csv(dataset.File.Path,as.is = T))
}

Combined.Data$Treatment_FACT<-factor(Combined.Data$Treatment_FACT,levels = siRNA.Names)

# plort data
library(ggplot2)
tsveta<-c('white','orange')

dodge <- position_dodge(width = 0.9)
p_list<-list()


p_list[[1]]<-ggplot(Combined.Data, aes(x=Treatment_FACT, y=Normalised.ERES.Count_NUM,fill =Treatment_FACT)) + 
  scale_y_continuous(limits = c(0, max(Combined.Data$Normalised.ERES.Count_NUM)*1.1))+#ylim(0,3000)+#limits = c(0, 500),#expand = c(0.2,0.)
  ylab('SEC24 ERES number per cell (normalised)')+
  theme_bw()+
  scale_x_discrete(expand = c(0.05,0.05))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_jitter(width = 0.3)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='none',legend.title=element_blank())+
  theme(axis.title.y =element_text(vjust = 0.5,size=20),axis.text.y= element_text(vjust = 0.5,size=16))+
  theme(axis.title.x = element_blank(),axis.text.x= element_text(vjust = 0.5,size=18))
p_list[[1]]


p_list[[2]]<-ggplot(Combined.Data, aes(x=Treatment_FACT, y=Eres.Fraction.Thresholded_NUM ,fill =Treatment_FACT)) + 
  scale_y_continuous(limits = c(0, max(Combined.Data$Eres.Fraction.Thresholded_NUM)*1.1),expand = c(0,0))+#ylim(0,3000)+#limits = c(0, 500),
  ylab('SEC24 fraction at ERES')+
  theme_bw()+
  scale_x_discrete(expand = c(0.05,0.05))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_jitter(width = 0.3)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='none',legend.title=element_blank())+
  theme(axis.title.y =element_text(vjust = 0.5,size=20),axis.text.y= element_text(vjust = 0.5,size=16))+
  theme(axis.title.x = element_blank(),axis.text.x= element_text(vjust = 0.5,size=18))
p_list[[2]]

library(gridExtra)
m_plots<-marrangeGrob(p_list,nrow=1,ncol=1)
ggsave(filename = gettextf('plots_Kyoto_siRNA_-%dreplicates.pdf',nReplicates),plot=m_plots,width=150,height=150,units='mm')



### distribution ofcell expression levels
expression.Level.Intervals<-c(0.0,0.8,1.6,1000.0)
expression.Interval.Labels<-c('0-0.8','0.8-1.6','>1.6')
Combined.Data$Interval.Index<-findInterval(Combined.Data$Cell.A488.Integrated.Intensity_NUM,vec=expression.Level.Intervals)

Combined.Data$Interval.Label<-expression.Interval.Labels[Combined.Data$Interval.Index]
Combined.Data$Interval.Label<-factor(Combined.Data$Interval.Label,levels = expression.Interval.Labels)

p_list<-list()

p_list[[1]]<-ggplot(Combined.Data, aes(x=Interval.Label, y=Normalised.ERES.Count_NUM,fill =Treatment_FACT)) + 
  scale_y_continuous(limits = c(0, max(Combined.Data$Normalised.ERES.Count_NUM)*1.1),expand=c(0,0))+#ylim(0,3000)+#limits = c(0, 500),#expand = c(0.2,0.)
  ylab(' SEC24 ERES number per cell (normalised)')+
  xlab('Integrated intensity SEC24')+
  theme_bw()+
  scale_x_discrete(expand = c(0.0,0.0))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.9),size=0.5)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank())+
  theme(axis.title.y =element_text(vjust = 0.5,size=18),axis.text.y= element_text(vjust = 0.5,size=16))+
  theme(axis.title.x = element_text(vjust = 0.5,size=18),axis.text.x= element_text(vjust = 0.5,size=16))
p_list[[1]]

p_list[[2]]<-ggplot(Combined.Data, aes(x=Interval.Label, y=Eres.Fraction.Thresholded_NUM ,fill =Treatment_FACT)) + 
  scale_y_continuous(limits = c(0, max(Combined.Data$Eres.Fraction.Thresholded_NUM)*1.1),expand = c(0,0))+#ylim(0,3000)+#limits = c(0, 500),
  ylab('SEC24 fraction at ERES')+
  xlab('Integrated intensity SEC24')+
  theme_bw()+
  scale_x_discrete(expand = c(0.0,0.0))+
  geom_boxplot(lwd=1,outlier.color = NA,position = dodge, width=0.7)+
  scale_fill_manual(values = tsveta)+#scale_fill_brewer(palette = "Set1")+
  geom_point(position=position_jitterdodge(dodge.width=0.9),size=0.5)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank())+
  theme(axis.title.y =element_text(vjust = 0.5,size=18),axis.text.y= element_text(vjust = 0.5,size=16))+
  theme(axis.title.x = element_text(vjust = 0.5,size=18),axis.text.x= element_text(vjust = 0.5,size=16))
p_list[[2]]


library(gridExtra)
m_plots<-marrangeGrob(p_list,nrow=1,ncol=1)
ggsave(filename = gettextf('plots_Kyoto_siRNA_-%dreplicates--distrib.pdf',nReplicates),plot=m_plots,width=200,height=150,units='mm')



