#
#e-mail: halavatyi@gmail.com.
#Last update: March 15,2017.
rm(list=ls())#clear all variables

rootw<-'root\\'

#
inputPaths<-sprintf('C:/tempDat/STC1_project_(FV)/Sec23-YFP--live imaging--drug/%s--Sec23-YFP--live imaging--drug/Analysis_01/R.Output/Modified_Summary_Table_TimeResolvedMeasurements.txt',c('20170307','20170221'))
outputPath<-'C:/tempDat/STC1_project_(FV)/Sec23-YFP--live imaging--drug/summary2replicates--20170307--20170221'

condition.Levels<-c('1E','1A','1G')
max.Time=1200

#Check for/upload required packages
if(!require(plotrix)) install.packages("plotrix")#biocLite("plotrix",ask=F)
if(!require(BBmisc))  install.packages("BBmics")#biocLite("BBmics",ask=F)
if(!require(ggplot2)) install.packages("ggplot2")#biocLite("BBmics",ask=F)


All.Data<-data.frame(Experiment.Tag=vector())

for(pth in inputPaths){
  replicate.Data<-read.delim(pth,header=T,sep="\t",as.is=T,na.strings='')
  All.Data<-rbind(All.Data,replicate.Data)
}

setwd(outputPath)

write.table(All.Data,file='AllData.txt',quote=F,row.names=F,sep="\t",na='')

All.Data<-All.Data[All.Data$Condition_FACT %in% condition.Levels,]
All.Data$Condition_FACT<-factor(All.Data$Condition_FACT,levels = condition.Levels)


str(All.Data)

All.Data$Tag.Replicate.Condition<-paste(All.Data$Experiment.Tag,All.Data$Date_FACT,All.Data$Condition_FACT,sep='_')
Unique.Tag.Replicate.Condition<-All.Data[,c('Experiment.Tag','Date_FACT','Condition_FACT','Tag.Replicate.Condition')]
Unique.Tag.Replicate.Condition<-Unique.Tag.Replicate.Condition[!duplicated(Unique.Tag.Replicate.Condition), ]
timePoints.Values<-data.frame(Time=0:399*3.034)
allTimePointsDatasets<-merge(Unique.Tag.Replicate.Condition,timePoints.Values,all = TRUE)
All.Data<-merge(All.Data,allTimePointsDatasets,all = TRUE)
All.Data<-All.Data[order(All.Data$Tag.Replicate.Condition,All.Data$Time),]


All.Data[is.na(All.Data$ERES.number.Norm),c('ERES.Average.Intensity.Norm','ERES.number.Norm')]<-0

All.Data<-All.Data[All.Data$Time<max.Time,]

All.Data<-All.Data[order(All.Data$Condition_FACT,All.Data$Date_FACT,All.Data$Experiment.Tag,All.Data$Time),]

mean.Data.Intensity<-aggregate(ERES.Average.Intensity.Norm~Time+Condition_FACT,data=All.Data,FUN='mean')
mean.Data.Integr.Intensity<-aggregate(ERES.Sum.Intensity.Norm~Time+Condition_FACT,data=All.Data,FUN='mean')
mean.Data.Counts<-aggregate(ERES.number.Norm~Time+Condition_FACT,data=All.Data,FUN='mean')

err.Data.Intensity<-aggregate(ERES.Average.Intensity.Norm~Time+Condition_FACT,data=All.Data,FUN='std.error')
names(err.Data.Intensity)[3]<-paste(names(err.Data.Intensity)[3],'.err',sep='')
err.Data.Integr.Intensity<-aggregate(ERES.Sum.Intensity.Norm~Time+Condition_FACT,data=All.Data,FUN='std.error')
names(err.Data.Integr.Intensity)[3]<-paste(names(err.Data.Integr.Intensity)[3],'.err',sep='')
err.Data.Counts<-aggregate(ERES.number.Norm~Time+Condition_FACT,data=All.Data,FUN='std.error')
names(err.Data.Counts)[3]<-paste(names(err.Data.Counts)[3],'.err',sep='')


Aggr.Data<-merge(mean.Data.Intensity,err.Data.Intensity)
Aggr.Data<-merge(Aggr.Data,mean.Data.Integr.Intensity)########
Aggr.Data<-merge(Aggr.Data,err.Data.Integr.Intensity)
Aggr.Data<-merge(Aggr.Data,mean.Data.Counts)
Aggr.Data<-merge(Aggr.Data,err.Data.Counts)
Aggr.Data$Condition_FACT<-factor(Aggr.Data$Condition_FACT,levels = condition.Levels)
Aggr.Data<-Aggr.Data[order(Aggr.Data$Condition_FACT,Aggr.Data$Time),]


intensity.Norm.Max<-max(All.Data$ERES.Average.Intensity.Norm)
integr.Intensity.Norm.Max<-max(All.Data$ERES.Sum.Intensity.Norm)
count.Norm.Max<-max(All.Data$ERES.number.Norm)

tsveta<-c('black','red','blue')

library(ggplot2)
dev.new()
ggplot(Aggr.Data, aes(x=Time, y=ERES.Average.Intensity.Norm, group=Condition_FACT,color=Condition_FACT)) +
  geom_line(size=0.8)+
  geom_ribbon(aes(ymin=ERES.Average.Intensity.Norm-ERES.Average.Intensity.Norm.err, ymax=ERES.Average.Intensity.Norm+ERES.Average.Intensity.Norm.err),alpha=0.5) +
  scale_y_continuous(limits = c(0, intensity.Norm.Max))+
  scale_color_manual(values = tsveta)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
  xlab('Time, s')+
  ylab('Integrated intensity per ERES')

library(ggplot2)
dev.new()
ggplot(Aggr.Data, aes(x=Time, y=ERES.Sum.Intensity.Norm, group=Condition_FACT,color=Condition_FACT)) +
  geom_line(size=0.8)+
  geom_ribbon(aes(ymin=ERES.Sum.Intensity.Norm-ERES.Sum.Intensity.Norm.err, ymax=ERES.Sum.Intensity.Norm+ERES.Sum.Intensity.Norm.err),alpha=0.5) +
  scale_y_continuous(limits = c(0, count.Norm.Max))+
  scale_color_manual(values = tsveta)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
  xlab('Time, s')+
  ylab('Integrated ERES intensity per cell')


library(ggplot2)
dev.new()
ggplot(Aggr.Data, aes(x=Time, y=ERES.number.Norm, group=Condition_FACT,color=Condition_FACT)) +
  geom_line(size=0.8)+
  geom_ribbon(aes(ymin=ERES.number.Norm-ERES.number.Norm.err, ymax=ERES.number.Norm+ERES.number.Norm.err),alpha=0.5) +
  scale_y_continuous(limits = c(0, count.Norm.Max))+
  scale_color_manual(values = tsveta)+
  theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='right',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
  xlab('Time, s')+
  ylab('Number of ERES (normalised)')




##individula  plots

All.Data$Tag.Replicate.Condition<-factor(All.Data$Tag.Replicate.Condition)

nrowPdf=3
ncolPdf=3

p_list<-list()
i=1;
for (drugTreatment in levels(All.Data$Condition_FACT)){
  Data.Treatment<-All.Data[All.Data$Condition_FACT==drugTreatment,]
  for (tagDataset in unique(Data.Treatment$Tag.Replicate.Condition)){
    #cat(drugTreatment,'\t',tagDataset,'\n')
    Data.Set<-Data.Treatment[Data.Treatment$Tag.Replicate.Condition==tagDataset,]

    p_list[[i]]<-ggplot(Data.Set, aes(x=Time, y=ERES.Average.Intensity.Norm, color=Condition_FACT)) +
      geom_line(size=0.8)+
      scale_y_continuous(limits = c(0, intensity.Norm.Max))+
      theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='none',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
      xlab('Time, s')+
      ylab('Integrated intensity per ERES')+
      ggtitle(tagDataset)
    i=i+1;

    p_list[[i]]<-ggplot(Data.Set, aes(x=Time, y=ERES.Sum.Intensity.Norm, color=Condition_FACT)) +
      geom_line(size=0.8)+
      scale_y_continuous(limits = c(0, intensity.Norm.Max))+
      theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='none',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
      xlab('Time, s')+
      ylab('Integrated ERES intensity per cell')+
      ggtitle(tagDataset)
    i=i+1;

    p_list[[i]]<-ggplot(Data.Set, aes(x=Time, y=ERES.number.Norm, color=Condition_FACT)) +
      geom_line(size=0.8)+
      scale_y_continuous(limits = c(0, count.Norm.Max))+
      theme(panel.background= element_rect(color = 'black',size=1.5),legend.position='none',legend.title=element_blank(),axis.text.y= element_text(vjust = 0.5,size=12))+
      xlab('Time, s')+
      ylab('Number of ERES (normalised)')+
      ggtitle(tagDataset)
    i=i+1;
  }
  while (i%%(nrowPdf*ncolPdf)!=1){
    p_list[[i]]<-ggplot()
    i=i+1
  }
  
}


library(gridExtra)
m_plots<-marrangeGrob(p_list,nrow=nrowPdf,ncol=ncolPdf)
ggsave(filename = sprintf('Individual_plots_%d.pdf',max.Time),plot=m_plots,width=400,height=90*3,units='mm')



cat('Script end')
