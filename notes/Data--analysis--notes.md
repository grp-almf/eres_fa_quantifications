# 2017-03-31

## To-do list

### Live-cell imaging data

- Quantifying time-lapse of ERES numbers and intensities after dug treatment
	- Generate plots of individual datasets and compare them with the movies 
- Detecting split/merge events 
	- TrackMate in Fiji
		- Current tracker does not put right pena;ties on intensity splits and merges (intensity of the whole should be sum of intensities of the parts).
		- Can we specify relevant parameters with existing Tracker?
			- Create test datasets 
		- Can we get/develop new Tracker with relevant rules:
			- Write to developers
	- Test Imaris

# 2017-04-19

## Live cell data analysis

### Things to try
- Quantify *pre* and *post* cells
- Classify ERES as *normal* and *small*. Check distributions of ERES intensities over time.
- Check splitting of ERES at times when their number increases. Focus on following datasets:
	- 2017-03-07 **A5**
	- 2017-03-07 **A6**(!)
	- 2017-03-07 **G6**
	- 2017-02-21 **G3**

# 2017-05-11

## Things to do before end of May (sorted by priority; highest to lowest)

1. Quantification of fixed stacks (Hela Kyoto and Sec23-YFP)
	- particular care to the last experiments with RANBP2
2. Count ERES in last live cell Sec23 experiments (big fields of view)
	- Segmentation of cells is an issue because they move andve have neither additional marker nor transmitted light image for segmentation. 
3. Prepare movies of STC1 overexpression time-lapse data
	- For the dataset 2017-05-09 replace the image where the drug is added with "drug added" message.
	- For the dataset 2017-05-05 merge movies (2 or 3 parts) and add "drug added image frame"
	- Align images?
	- Put proper time tags:
		- 0 should correspond to the time when drug is added
		- time tags to be read from metadata.
4. Tracking of ERES split and merge events on Sec23 time-lapse experiments with high time resolution.

# 2017-05-28/31

## Low-zoom time-lapse data with STC1

### Protocol for preparing movies and quantifying  cell intensities
1. merging different pieces of time lapse acquisitions in one movie (when required);
2. image filtration.
3. max projections
4. 2D alignment (max projections) and stacks
5. Extracting time stamps from metadata
	1. printed on the stacks and max projections
	2. combined with intensity quantifications;
6. Job for manual segmentation of cells and background;
7. Quantification of cell total intensities (from filtered stacks);
8. R script to plot individual intensity traces (raw and normalised). Traces from different cells in the same field of view are different lines on the same plot.


### Data processing:
- 2017-05-09 dataset: the whole protocol was run. Time plots of cell intensities were plotted at the end;
- 2017-05-05 dataset: only stacks and projections were prepared, no segmentation and quantification yet


### Things still TODO:
-  introduce time correction for different imaging positions;
-  segment cells and background manually for the dataset 2017-05-05
-  run the whole protocol for the dataset 2017-05-05


## Fixed cells data with COPII stainings
- old quantification protocol seem to be unsatisfactory as it does not reflect visible changes in ERES morphology and localisation:
	- some siRNA treatment lead to break down of ERES into small particles. Current segmentation protocol quantifies particles as single ERES
- Possibilities to improve:
	- set threshold in ERES integrated intensities: not clear how to set global threshold or modify it depending on sample brightness
	- extract "size" ("volume")  feature of ERES and try to filter by size. Seem to be also not very robust, because will be sensitive to segmenting border pixels having large noise;
	- look on distribution profiles of fluorescence and size characteristics of individual ERES
		- check whether thresholds can be extracted from distributions.
		- Whether distributions will be different for control and for treated cells;
	- Look on changes of ERES localisation throughout the cell using different approaches;
-  Test cells selected manually to check different approaches above
	-  dataset 2017-04-12;
	-  examples of control (Neg9) and target siRNA (RANBP2 and 119) cells.

# 2017-06-07/08
## Fixed cells data with COPII stainings
### Analysis of selected cell examples
- Manually modified summary files to list only selected cells.
- New (updated) step of ERES quantification. For each cell Integrated Intensity, Volume and Area are loaded into separate table.
- R script to plot distributions of ERES intensities, areas and volumes.

### Results of manual and automatic tests
- When denoising filtration is applied,a lot of small particles become visible;
- Distributions for Intensities and volumes seem to be different visually, but no bimodal disribution vis visible, not clear how to define threshold to separate ERES from less intense/smaller structures.

### Next things to try
- Log-scale distributions
- Correlative (2D) plots for distributions
- extract local maxima values that correspond to spots. Look of their distributions and correlations with other spot parameters (2D distributions).
- Tryspot quantification without segmenting as in ParticleTracker Fiji plugin.

# 2017-06-19/29
## Fixed cell data with COPII stainings: siRNA experiments with Hela Kyoto

# 2017-07-??
## Working with ERES distributions in fixed cells:
....................
### Things to check
- Dustributions of individual ERES
- ERES outliers in individual cells
- Background subtraction
- Make plots:
	- Fraction of ERES with different sizes per cell.
	- .........


# 2017-08-01/05
## Modified protocols for fixed cell data with COPII stainings
- Extracting max value for each ERES in addition to integrated density, volume and area
- Getting all spots (even dim ones) and filtering afterwards (e.g. by max intensity)
- R calculations (new script) with estimaing ERES-related quantitised by aggregating single ERES data:
	- Get big table containing single ERES numbers in all cells.This table also gas treatment.ID and Cell.ID
	- Filtering ERES by parameters (e.g. max intensity)
	- Aggregation
		- number of filtered ERES in a cell
		- total intensity of filtered ERES
	- Making final calculations
		- ERES fraction in a cell
		- intensity per ERES
	- Plotting

# 2017-09
## Analysis of fixed cells data with COPII stainings
- Filtration of ERES based on max intensity is done in the R postprocessing script, but additional step in Fiji is done to filter 3D ROIs with the same threshold value. Then it is possible to visualise either all segmented ERES ROIs or only those that passed threshold.
- Different thresholds need to be defined for different channels (in case UBC9/2-DO8 experiments):
	- Sec24B-A488: 10000
	- Sec31A-A568: 15000
- In 2-DO8 experiment nuclei segmentation did not work properly with older pipeline, because high cytoplasmic background in DAPI is observed when cells treated with the drug. To deal with it, nucleus segmentation step was slightly modified to allow additional parameters to be specified  in the pipeline (default values of these parameters correspond to earlier version of this step):
	- Nucleus threshold (on filtered images)
	- Otsu radius for automatic local thresholding. Increase of the radius gave better results with less nuclei fragmentation issues.


	