# Colours for Box-plots to be consistent between treatments

### siRNA experiments
- Neg9: white
- STC1: cyan
- RANBP2: pink
- UBC9: orange


### Drug experiments
- 2-DO8: orange