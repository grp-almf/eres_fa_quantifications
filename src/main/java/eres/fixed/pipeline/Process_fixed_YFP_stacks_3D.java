package eres.fixed.pipeline;

import java.util.ArrayList;
import java.util.List;

import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.cell.Step_ExtractSlice;
import automic.postaq.step.cell.Step_GetStackMaxProjection;
import automic.postaq.step.cell.Step_SegmentCellsBrightFieldStack;
//import automic.postaq.step.cell.Step_SegmentDapi;
import automic.postaq.step.cell.Step_SegmentDapi_3D;
import automic.postaq.step.eres.fixed.Step_QuatifyCellAndEres;
import automic.postaq.step.eres.fixed.Step_SegmentEresInCell3D;
import automic.postaq.step.table.Step_ExpandSingleRoiObjects;
import automic.postaq.step.table.Step_MergeMoveCondition;

public class Process_fixed_YFP_stacks_3D implements PredefinedPipelineSteps {
	
	@Override
	public List<RunningStep> createStepsList()throws Exception{
		
		List<RunningStep> stepList=new ArrayList<RunningStep>();
		RunningStep step;
		
		//move and condition table
		step=new RunningStepFactory(Step_MergeMoveCondition.class).create();
        step.setInputSettingValue("Well Position File Tag", "AFocus");
        step.setInputSettingValue("Well Position Regular Expression", "[a-zA-Z0-9-_]+_W(?<Well>\\d+)_P(?<Position>\\d+)_T(?<TimePoint>\\d+).lsm");
        step.setInputSettingValue("Experiment Table SubPath Template", "summary_%s.txt");
        step.setInputSettingValue("Experiment Column Tag", "Experiment.Tag");
        step.setInputSettingValue("Well Column Tag", "Well");
		stepList.add(step);
		
		//project YFP
		RunningStepFactory stackProjectorFactory=new RunningStepFactory(Step_GetStackMaxProjection.class);
        step=stackProjectorFactory.create();
        step.setInputSettingValue("Input Image Tag", "YFP");
        step.setInputSettingValue("Output Image Tag", "YFP.max");
        step.setInputSettingValue("Channel Index", 1);
        step.setInputSettingValue("Time Index", 1);
		stepList.add(step);
		
		//project DAPI
        step=stackProjectorFactory.create();
        step.setInputSettingValue("Input Image Tag", "DAPI");
        step.setInputSettingValue("Output Image Tag", "DAPI.max");
        step.setInputSettingValue("Channel Index", 1);
        step.setInputSettingValue("Time Index", 1);
		stepList.add(step);
		
		//extract slice from brightfield stack
        step=new RunningStepFactory(Step_ExtractSlice.class).create();
        step.setInputSettingValue("Input Image Tag", "DAPI");
        step.setInputSettingValue("Output Image Tag", "BrightField.slice");
        step.setInputSettingValue("Channel Index", 2);
        step.setInputSettingValue("Time Index", 1);
        step.setInputSettingValue("Slice Index", 3);
        stepList.add(step);
        
        //segment nuclei
        step=new RunningStepFactory(Step_SegmentDapi_3D.class).create();
        step.setInputSettingValue("Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("DAPI Image Tag", "DAPI");
        stepList.add(step);
		
        //segment cytoplasm
        step=new RunningStepFactory(Step_SegmentCellsBrightFieldStack.class).create();
        step.setInputSettingValue("Pre-Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("Cells Roi Tag", "Cells.Roi");
        step.setInputSettingValue("Background Roi Tag", "Background.Roi");
        step.setInputSettingValue("Bright Field Image Tag", "DAPI");
        step.setInputSettingValue("Bright Field Channel Index", 2);
        stepList.add(step);
        
        //expandTable to have each cell object as row in the table
        step=new RunningStepFactory(Step_ExpandSingleRoiObjects.class).create();
        step.setInputSettingValue("Grouped Rois column Tag", "Cells.Roi");
        step.setInputSettingValue("Individual Rois column Tag", "SingleCell.Roi");
        step.setInputSettingValue("Process flag column tag", "Success");
        stepList.add(step);
        
        //segment ERES
        step=new RunningStepFactory(Step_SegmentEresInCell3D.class).create();
        step.setInputSettingValue("Eres image Tag", "YFP");
        step.setInputSettingValue("Cell Roi Tag", "SingleCell.Roi");
        step.setInputSettingValue("Segmented ERES 3D ROI Tag", "Eres.3DRoi");
        step.setInputSettingValue("Segmented ERES Number Tag", "Eres.Count");
        step.setInputSettingValue("Process flag column tag", "Success");
        stepList.add(step);
        
        //quantify cells and ERES
        step=new RunningStepFactory(Step_QuatifyCellAndEres.class).create();
        step.setInputSettingValue("Eres image Tag", "YFP");
        step.setInputSettingValue("Cell Roi Tag", "SingleCell.Roi");
        step.setInputSettingValue("ERES 3D ROI Tag", "Eres.3DRoi");
        step.setInputSettingValue("Background Roi Tag", "Background.Roi");
        step.setInputSettingValue("ERES Number Tag", "Eres.Count");
        step.setInputSettingValue("Process flag column tag", "Success");
        
        step.setInputSettingValue("Cell Area Tag", "Cell.Area");
        step.setInputSettingValue("Cell Integrated Intensity Tag", "Cell.Integrated.Intensity");
        step.setInputSettingValue("ERES Fraction Tag", "Eres.Fraction");
        stepList.add(step);

        
        
        return stepList;
	}
}
