package eres.fixed.pipeline;
import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;
import automic.utils.Utils;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.IJ;
import ij.io.*;
import ij.plugin.PlugIn;
import ij.gui.GenericDialog;


public class Process_fixed_YFP_stacks_3D_plugin implements PlugIn {
	PipelineInterface EresFRAPPipeline=null;
	
	static String rootPath;
	static String analysisFolderName="Analysis1";
	static String inputTableFileName;
	
	static int startStep=-1;
	static int endStep=-1;
	static int startDataset=-1;
	static int endDataset=-1;
	
	
	@Override
	public void run(String arg) {
		Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		ApplicationLogger.setLogger(logger);
		
		
		try{
			EresFRAPPipeline=new PipelineFactory(new Process_fixed_YFP_stacks_3D()).getPipeline();
		}catch (Exception ex){
			Utils.generateErrorMessage("Can not initialise pipeline", ex);
		}
		//select input file and folder
		OpenDialog dlo = new OpenDialog ("Choose summary file for analysis", null);
		rootPath=dlo.getDirectory();
		inputTableFileName=dlo.getFileName();
		if (inputTableFileName==null){
			return; //stop if input file was not selected 
		}
			
		//create new generic dialog and fill-in settings
		GenericDialog gd_in=new GenericDialog("Input parameters for quantifying FRAP at ERES");
		
		gd_in.addStringField("Analysis folder name: ", analysisFolderName);
			
		gd_in.addNumericField("Start analysis step (0 based)", 	startStep, 0);
		gd_in.addNumericField("End analysis step (0 based)", 	endStep, 0);
		gd_in.addNumericField("Start dataset (0 based)", 		startDataset, 0);
		gd_in.addNumericField("End dataset (0 based)", 			endDataset, 0);

		gd_in.showDialog();
		if (gd_in.wasCanceled()){
			IJ.log("Plugin is aborted by user.");
			return;
		}
    
		analysisFolderName=gd_in.getNextString();
		startStep=(int)gd_in.getNextNumber();
		endStep=(int)gd_in.getNextNumber();
		startDataset=(int)gd_in.getNextNumber();
		endDataset=(int)gd_in.getNextNumber();
		
		PipelineInput pipelineInput=new PipelineInput();
		pipelineInput.dataPath=rootPath;
		pipelineInput.subPathAnalysis =analysisFolderName;
		pipelineInput.globalExperimentTableName=inputTableFileName;
		pipelineInput.startStep=startStep;
		pipelineInput.endStep=endStep;
		pipelineInput.startDataset=startDataset;
		pipelineInput.endDataset=endDataset;

		try{
			PipelineRunner pipelineRunner=new PipelineRunner(EresFRAPPipeline,pipelineInput);
			pipelineRunner.runPipeline();
		}catch (Exception e){
			IJ.error("Exception raised when processing data.\nPlugin will stop");
			return;
		}
	}
	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
//	public static void main(String[] args) {
//		// set the plugins.dir property to make the plugin appear in the Plugins menu
//		Class<?> clazz = Process_FRAP_ERES_plugin.class;
//		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
//		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
//		System.setProperty("plugins.dir", pluginsDir);
//
//		// start ImageJ
//		new ImageJ();
//		//DEBUG=true;
//		//Debug.run(plugin, parameters);
//		//new WaitForUserDialog("test Maven project").show();
//		
//		// open the Clown sample
//		//ImagePlus image = IJ.openImage("http://imagej.net/images/clown.jpg");
//		//image.show();
//		
//		//out_pth="Z:\\halavaty\\Sec23 with red 09042014\\Analysis\\FRAP Quantification";
//		//this.ProcessData();
//		IJ.runPlugIn(clazz.getName(), "");
//
//	}

}
