package eres.fixed.pipeline;


import java.io.File;

import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineReader;
import automic.postaq.pipeline.PipelineRunner;
import automic.postaq.pipeline.xml.PipelineXmlReader;
import automic.utils.Utils;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.IJ;
import ij.ImageJ;

public abstract class TestStepInXmlPipeline {

	static String pipelinePath="C:\\tempdat\\Ann";
	static String pipelineFileName="Pipeline_FocalAdhesions_modified4.xml";

	
	static final String testDataPath			="C:/tempdat/Ann/TestData_20230222/FA_pipeline_test";//"C:\\tempdat\\Ann";
	static final String testSubPathAnalysis 	="Analysis_2023-3";
	static final String testExperimentTableName	="summary_RAW.txt";
	static final int	testStepIndex			=3;
	static final int	testDatasetStart		=0;
	static final int	testDatasetEnd			=1;
	
	
	public static void printPipelineSteps(PipelineInterface _pipeline){
		Logger logger=ApplicationLogger.getLogger();
		
		PipelineReader pipelineReader=new PipelineReader(_pipeline);
		String[] stepNames=pipelineReader.getStepNames();
		int nSteps=stepNames.length;
		logger.sendMessage("=====================================");
		logger.sendMessage(String.format("%d steps in the tested pipeline", nSteps));
		for (int i=0;i<nSteps;i++)
			logger.sendMessage(String.format("%d: \"%s\"",i,stepNames[i]));
		if((testStepIndex<0)||(testStepIndex>=nSteps))
			throw new ArrayIndexOutOfBoundsException("Wrong test step index");

		logger.sendMessage(String.format("Tested Step: index=%d;name=\"%s\"",testStepIndex,stepNames[testStepIndex]));
		logger.sendMessage("=====================================");
	}
	
	public static PipelineInput getTestInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath=testDataPath;
		protocolInput.subPathAnalysis =testSubPathAnalysis;
		protocolInput.globalExperimentTableName=testExperimentTableName;
		protocolInput.startStep=testStepIndex;
		protocolInput.endStep=testStepIndex;
		protocolInput.startDataset=testDatasetStart;
		protocolInput.endDataset=testDatasetEnd;
		
		return protocolInput;
	}
	
	public static void runTest()throws Exception{
		IJ.run("Synchronize Windows", "");
		
		PipelineXmlReader xmlReader=new PipelineXmlReader(new File(pipelinePath,pipelineFileName).getAbsolutePath());
		PipelineInterface testPipeline=null;
		
		try{
			testPipeline=xmlReader.getPipeline();
		}catch(Exception ex){
			Utils.generateErrorMessage("Pipeline loading crashed", ex);
		}

		printPipelineSteps(testPipeline);
		
		PipelineRunner	pipelineRunner=new PipelineRunner(testPipeline, getTestInput());
		pipelineRunner.runPipeline(true);
}
	
	public static void main(String[] args)throws Exception {
		// start ImageJ
		new ImageJ();

		Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		ApplicationLogger.setLogger(logger);
		
		try{
			runTest();
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Crash During Pipeline Test", ex);
		}
	}
}
