package eres.fixed.pipeline;

import java.util.ArrayList;
import java.util.List;

import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.cell.Step_ExtractSlice;
import automic.postaq.step.cell.Step_GetStackMaxProjection;
import automic.postaq.step.cell.Step_SegmentCellsBrightFieldStack;
import automic.postaq.step.cell.Step_SegmentDapi;
import automic.postaq.step.table.Step_MergeMoveCondition;

public class Process_fixed_YFP_stacks implements PredefinedPipelineSteps {
	
	@Override
	public List<RunningStep> createStepsList()throws Exception{
		
		List<RunningStep> stepList=new ArrayList<RunningStep>();
		RunningStep step;
		
		//move and condition table
		step=new RunningStepFactory(Step_MergeMoveCondition.class).create();
        step.setInputSettingValue("Well Position File Tag", "AFocus");
        step.setInputSettingValue("Well Position Regular Expression", "[a-zA-Z0-9-_]+_W(?<Well>\\d+)_P(?<Position>\\d+)_T(?<TimePoint>\\d+).lsm");
        step.setInputSettingValue("Experiment Table SubPath Template", "summary_%s.txt");
        step.setInputSettingValue("Experiment Column Tag", "Experiment.Tag");
        step.setInputSettingValue("Well Column Tag", "Well");
		stepList.add(step);
		
		//project YFP
		RunningStepFactory stackProjectorFactory=new RunningStepFactory(Step_GetStackMaxProjection.class);
        step=stackProjectorFactory.create();
        step.setInputSettingValue("Input Image Tag", "YFP");
        step.setInputSettingValue("Output Image Tag", "YFP.max");
        step.setInputSettingValue("Channel Index", 1);
        step.setInputSettingValue("Time Index", 1);
		stepList.add(step);
		
		//project DAPI
        step=stackProjectorFactory.create();
        step.setInputSettingValue("Input Image Tag", "DAPI");
        step.setInputSettingValue("Output Image Tag", "DAPI.max");
        step.setInputSettingValue("Channel Index", 1);
        step.setInputSettingValue("Time Index", 1);
		stepList.add(step);
		
		//extract slice from brightfield stack
        step=new RunningStepFactory(Step_ExtractSlice.class).create();
        step.setInputSettingValue("Input Image Tag", "DAPI");
        step.setInputSettingValue("Output Image Tag", "BrightField.slice");
        step.setInputSettingValue("Channel Index", 2);
        step.setInputSettingValue("Time Index", 1);
        step.setInputSettingValue("Slice Index", 3);
        stepList.add(step);
        
        //segment nuclei
        step=new RunningStepFactory(Step_SegmentDapi.class).create();
        step.setInputSettingValue("Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("DAPI Image Tag", "DAPI.max");
        stepList.add(step);
		
        //segment nuclei
        step=new RunningStepFactory(Step_SegmentCellsBrightFieldStack.class).create();
        step.setInputSettingValue("Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("Segmented Cells Roi Tag", "Cells.Roi");
        step.setInputSettingValue("Bright Field Image Tag", "DAPI");
        step.setInputSettingValue("Bright Field Channel Index", 2);
        stepList.add(step);
        
        return stepList;
	}
}
