package eres.fixed.pipeline;

import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;
//import automic.postaq.pipeline.predefined.Process_fixed_YFP_stacks_steps;
import automic.table.ManualControlFrame;
import automic.table.TableModel;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.ImageJ;

public abstract class RunPredefinedPipeline {

	
	public static PipelineInput getDefaultInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath="C:\\tempdat\\Muzamil\\ERES_202210";
		protocolInput.subPathAnalysis ="Analysis_20221104_1";
		protocolInput.globalExperimentTableName="summary-manual.txt";
		protocolInput.startStep=-1;
		protocolInput.endStep=-1;
		protocolInput.startDataset=-1;
		protocolInput.endDataset=-1;
		
		return protocolInput;
	}
	
	public static void main(String[] args) {

		// start ImageJ
		new ImageJ();

		//create logger and set it as application logger
		Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		ApplicationLogger.setLogger(logger);
		
		// create pipeline and pipelineRunner objects
		PipelineInterface	pipelineObject;
		PipelineRunner		pipelineRunner;
		try{
			pipelineObject=new PipelineFactory(new Spot_Analysis_JJ()).getPipeline();//new Process_fixed_YFP_stacks_3D()//new Process_fixed_YFP_stacks();//new Process_FRAP_ERES_Cargo();//
			pipelineRunner=new PipelineRunner(pipelineObject, getDefaultInput());
		}catch (Exception ex){
			logger.sendExceptionMessage("Pipeline initialisation crashed", ex);
			return;
		}
		
		//run pipeline
		try{
			pipelineRunner.runPipeline();
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Pipeline execution crashed", ex);
			return;
		}
		
		//visualise results
		try{
			ManualControlFrame frame=new ManualControlFrame(new TableModel(pipelineRunner.getLastOutputTableFile()),false);
			frame.setVisible(true);
		}catch(Exception ex){
			logger.sendExceptionMessage("Result visualisation crashed", ex);
			return;
		}
	}
}
