package eres.fixed.pipeline;

import java.io.File;

import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.xml.PipelineXMLWriter;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.ImageJ;

public class WritePipelineToTheFile {
	static String filePath="c:\\tempdat\\Juan\\Eres-quantifications-2020";
	static String fileName="Pipeline_JJ_202004last.xml";
	
	public static void main(String[] args) {
		// start ImageJ
		new ImageJ();

		//create logger and set it as application logger
		Logger logger=new TextWindowLogger("Writing pipelineToXML Log");
		ApplicationLogger.setLogger(logger);
		
		// create pipeline object
		PipelineInterface	pipelineObject;
		try{
			pipelineObject=new PipelineFactory(new Spot_Analysis_JJ()).getPipeline();
		}catch (Exception ex){
			logger.sendExceptionMessage("Pipeline initialisation crashed", ex);
			return;
		}
		
		//write pipeline to file
		try{
			new PipelineXMLWriter().writePipelineToFile(pipelineObject, new File(filePath,fileName));
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Pipeline storage to the file crashed", ex);
			return;
		}
	}
}
