package eres.fixed.pipeline;

import java.util.ArrayList;
import java.util.List;

import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.eres.fixed.Step_QuatifyCellAndEres;
//import automic.postaq.step.cell.Step_SegmentCellsBrightFieldStack;
//import automic.postaq.step.eres.fixed.Step_QuatifyCellAndEres;
import automic.postaq.step.jj.Step_SegmentEresInCell3D_JJ;
import automic.postaq.step.jj.Step_ExtractSeries;
import automic.postaq.step.jj.Step_SegmentCellsBrightFieldStack_JJ;
import automic.postaq.step.jj.Step_SegmentDapi_3D_JJ;
import automic.postaq.step.table.Step_ExpandSingleRoiObjects;

public class Spot_Analysis_JJ implements PredefinedPipelineSteps {
	
	@Override
	public List<RunningStep> createStepsList()throws Exception{
		
		List<RunningStep> stepList=new ArrayList<RunningStep>();
		RunningStep step;
		
		//extract series for quantification
		step=new RunningStepFactory(Step_ExtractSeries.class).create();
		stepList.add(step);
		
        //segment nuclei
        step=new RunningStepFactory(Step_SegmentDapi_3D_JJ.class).create();
        step.setInputSettingValue("Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("DAPI Image Tag", "Extracted.Series");
        step.setInputSettingValue("Radius Niblack", 200);
        stepList.add(step);
        
        //segment cytoplasm
        step=new RunningStepFactory(Step_SegmentCellsBrightFieldStack_JJ.class).create();
        step.setInputSettingValue("Pre-Segmented Nuclei Roi Tag", "Nuclei.Roi");
        step.setInputSettingValue("Cells Roi Tag", "Cells.Roi");
        step.setInputSettingValue("Background Roi Tag", "Background.Roi");
        step.setInputSettingValue("Bright Field Image Tag", "Extracted.Series");
        step.setInputSettingValue("Bright Field Channel Index", 2);
        stepList.add(step);
        
        //expandTable to have each cell object as row in the table
        step=new RunningStepFactory(Step_ExpandSingleRoiObjects.class).create();
        step.setInputSettingValue("Grouped Rois column Tag", "Cells.Roi");
        step.setInputSettingValue("Individual Rois column Tag", "SingleCell.Roi");
        step.setInputSettingValue("Process flag column tag", "Success");
        stepList.add(step);
        
        //segment ERES
        step=new RunningStepFactory(Step_SegmentEresInCell3D_JJ.class).create();
        step.setInputSettingValue("Eres image Tag", "Extracted.Series");
        step.setInputSettingValue("Cell Roi Tag", "SingleCell.Roi");
        step.setInputSettingValue("Segmented ERES 3D ROI Tag", "Eres.3DRoi");
        step.setInputSettingValue("Segmented ERES Number Tag", "Eres.Count");
        step.setInputSettingValue("Process flag column tag", "Success");
        step.setInputSettingValue("Eres Channel Index", 3);
        stepList.add(step);
        
        //quantify cells and ERES
        step=new RunningStepFactory(Step_QuatifyCellAndEres.class).create();
        step.setInputSettingValue("Eres image Tag", "Extracted.Series");
        step.setInputSettingValue("Cell Roi Tag", "SingleCell.Roi");
        step.setInputSettingValue("ERES 3D ROI Tag", "Eres.3DRoi");
        step.setInputSettingValue("Background Roi Tag", "Background.Roi");
        step.setInputSettingValue("ERES Number Tag", "Eres.Count");
        step.setInputSettingValue("Process flag column tag", "Success");
        
        step.setInputSettingValue("Cell Area Tag", "Cell.Area");
        step.setInputSettingValue("Cell Integrated Intensity Tag", "Cell.Integrated.Intensity");
        step.setInputSettingValue("ERES Fraction Tag", "Eres.Fraction");
        stepList.add(step);
        
		return stepList;
	}
}

