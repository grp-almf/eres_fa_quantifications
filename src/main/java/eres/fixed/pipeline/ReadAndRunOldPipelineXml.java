package eres.fixed.pipeline;

import java.awt.Color;
import java.io.File;

import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;
import automic.postaq.pipeline.xml.PipelineXmlReader;
import automic.table.ManualControlFrame;
import automic.table.TableModel;
import automic.utils.Utils;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.TextWindowLogger;
import automic.utils.roi.ROIManipulator3D;
import ij.ImageJ;

public class ReadAndRunOldPipelineXml {
	static String pipelinePath="C:\\tempdat\\Ann";
	static String pipelineFileName="Pipeline_FocalAdhesions_modified4.xml";
	
	public static PipelineInput getDefaultInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath="C:\\tempdat\\Ann\\FA_Files_20230328";//
		protocolInput.subPathAnalysis ="Analysis_20230328";
		protocolInput.globalExperimentTableName="summary_RAW.txt";
		protocolInput.startStep=-1;
		protocolInput.endStep=-1;
		protocolInput.startDataset=-1;
		protocolInput.endDataset=-1;
		
		return protocolInput;
	}
	
	public static void runTest(){
		ApplicationLogger.setLogger(new TextWindowLogger("AutoMic analysis log"));
		
		PipelineXmlReader xmlReader=new PipelineXmlReader(new File(pipelinePath,pipelineFileName).getAbsolutePath());
		PipelineInterface pipelineObject=null;
		
		try{
			pipelineObject=xmlReader.getPipeline();
		}catch(Exception ex){
			Utils.generateErrorMessage("Pipeline loading crashed", ex);
		}
		
		PipelineRunner	pipelineRunner=new PipelineRunner(pipelineObject, getDefaultInput());
		try{
			pipelineRunner.runPipeline();
		}
		catch (Exception ex){
			Utils.generateErrorMessage("Pipeline running crashed", ex);
		}
		try{
			//change display color for 3d ROIs
			ROIManipulator3D.NONFILT_COLOR=Color.MAGENTA;
			//
			ManualControlFrame frame=new ManualControlFrame(new TableModel(pipelineRunner.getLastOutputTableFile()),false);
			frame.setVisible(true);
		}catch(Exception ex){
			Utils.generateErrorMessage("Result visualisation crashed", ex);
		}
		
		ApplicationLogger.resetLogger();
	}
	
	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {

		// start ImageJ
		new ImageJ();
		runTest();
		
	}
}
