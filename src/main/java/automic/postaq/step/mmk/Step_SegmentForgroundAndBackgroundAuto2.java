package automic.postaq.step.mmk;

import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.segment.ParticleSegmentor;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.RoiEnlarger;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.AutoThresholder;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

public class Step_SegmentForgroundAndBackgroundAuto2 implements StepFunctions {
	//input parameters
	
	String inputImageTag;
	String foregroundAreaOutputTag;
	String foregroundRoiTag;
	String backgroundRoiTag;
	String autoThresholdingMethod;
	int segmentationChannelIndex;
	
	//?variables referenced by different functions
	Duplicator duplicator;
	ImageOpener imageOpener;
	ZProjector averageProjector;
	ThresholdToSelection thresholdToSelection;
	
	int backgroundDilationPixels=10;
	int grayscaleClosingRadius=5;
	int minimalForeroundSize=10;

	
	@Override
	public String getStepInformation(){
		return "Segment forground and background with auto threshold";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Foreground Roi Tag",	 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Background Roi Tag",	 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Input Image Tag",		 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Foreground Area Roi Tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Grayscale Closing Radius", 	null, grayscaleClosingRadius, 	ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Auto Thresholding Method",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Segmentation Channel Index",	null, null, ParameterType.INT_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("Background Dilation Pixels", 	null, backgroundDilationPixels, ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Smallest Foreground Structure Size", 	null, minimalForeroundSize, 	ParameterType.INT_PARAMETER);
		

		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		foregroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Foreground Roi Tag");
		backgroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Background Roi Tag");
		inputImageTag=			(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		foregroundAreaOutputTag=(String)_stepParameterCollection.getParameterValue("Foreground Area Roi Tag");
		grayscaleClosingRadius= (int)	_stepParameterCollection.getParameterValue("Grayscale Closing Radius");
		autoThresholdingMethod=	(String)_stepParameterCollection.getParameterValue("Auto Thresholding Method");
		segmentationChannelIndex=(int)	_stepParameterCollection.getParameterValue("Segmentation Channel Index");
		
		backgroundDilationPixels=(int)	_stepParameterCollection.getParameterValue("Background Dilation Pixels");
		minimalForeroundSize= (int)	_stepParameterCollection.getParameterValue("Smallest Foreground Structure Size");
		
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(foregroundRoiTag, "ROI");
		workingProcessor.addFileColumns(backgroundRoiTag, "ROI");
		workingProcessor.addValueColumn(foregroundAreaOutputTag, "NUM");
		
		
		duplicator=new Duplicator();
		imageOpener=new ImageOpener();
		averageProjector=new ZProjector();
		averageProjector.setMethod(ZProjector.AVG_METHOD);
		thresholdToSelection=new ThresholdToSelection();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		File inputImageFile=workingTable.getFile(_rowIndex, inputImageTag, "IMG");
		
		String outputFileName=String.format("Dataset_%05d", _rowIndex+1);
		
		if(!workingTable.getBooleanValue(_rowIndex, "Success")) return;
		
		ImagePlus inputImageStack=imageOpener.openImage(inputImageFile);
		IJ.run(inputImageStack, "Set Scale...", "distance=0 known=0 unit=pixel");
		IJ.run(inputImageStack, "32-bit", "");
		
		if(inputImageStack.getNDimensions()>3)
			inputImageStack=duplicator.run(inputImageStack, segmentationChannelIndex, segmentationChannelIndex, 1, inputImageStack.getNSlices(), 1, 1);
			
		IJ.run(inputImageStack, "Grays", "");
		_data.showDebugImage(inputImageStack, "Original image in gray", true);

		averageProjector.setImage(inputImageStack);
		averageProjector.doProjection();

		ImagePlus avgProjectedImage=averageProjector.getProjection();
		_data.showDebugImage(avgProjectedImage, "Average Projected image", true);

		IJ.run(avgProjectedImage, "Maximum...",String.format("radius=%d",grayscaleClosingRadius));
		IJ.run(avgProjectedImage, "Minimum...",String.format("radius=%d",grayscaleClosingRadius));
		_data.showDebugImage(avgProjectedImage, "Average Projected image", true);
	
		
		//IJ.setAutoThreshold(maxProjectedImage, "Triangle dark");
		ImageProcessor avgImageProcessor=avgProjectedImage.getProcessor();
		avgImageProcessor.setAutoThreshold(AutoThresholder.Method.valueOf(autoThresholdingMethod), true);

		
		Roi foregroundRoi=thresholdToSelection.convert(avgImageProcessor);
		_data.showDebugImage(avgProjectedImage, "Foreground Roi", true, foregroundRoi);

		
		avgProjectedImage.setRoi(foregroundRoi);
		IJ.run(avgProjectedImage, "Make Inverse", "");
		
		Roi backgroundRoi=avgProjectedImage.getRoi();
		backgroundRoi=RoiEnlarger.enlarge(backgroundRoi, -backgroundDilationPixels);
		_data.showDebugImage(avgProjectedImage, "Background Roi", true, backgroundRoi);
		
		//ByteProcessor maskProcessor=maxImageProcessor.createMask();
		//_data.showDebugImage(new ImagePlus("", maskProcessor), "Thresholded Mask Image", true);
		
		avgProjectedImage.setRoi((Roi)null);
		ParticleSegmentor segmentor=new ParticleSegmentor(minimalForeroundSize);
		ImagePlus maskImage=segmentor.SegSliceThreshold(avgProjectedImage, ParticleAnalyzer.SHOW_MASKS, false, avgImageProcessor.getMinThreshold());
		IJ.run(maskImage, "Options...", "iterations=1 count=1 black do=Nothing");
		IJ.run(maskImage, "Grays", "");
		IJ.run(maskImage, "Fill Holes", "");
		
		_data.showDebugImage(maskImage, "Thresholded Mask Image", true);
		
		
		IJ.setRawThreshold(maskImage, 128, 255, null);
		foregroundRoi=thresholdToSelection.convert(maskImage.getProcessor());
		
		double foregroundArea;
		if (ROIManipulator2D.isEmptyRoi(foregroundRoi)){
			foregroundArea=0;
		}else {
			maskImage.setRoi(foregroundRoi);
			foregroundArea=maskImage.getStatistics(ImageStatistics.AREA).area;
			
		}
		
		
		
		
		
		if (!ROIManipulator2D.isEmptyRoi(foregroundRoi)){
			workingProcessor.saveRoiToTable(_rowIndex, foregroundRoi, foregroundRoiTag, outputFileName);
		}

		if (!ROIManipulator2D.isEmptyRoi(backgroundRoi)){
			workingProcessor.saveRoiToTable(_rowIndex, backgroundRoi, backgroundRoiTag, outputFileName);
		}
		
		workingProcessor.setValue(foregroundArea, _rowIndex, foregroundAreaOutputTag, "NUM");
		
		
		return;
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-1,10,10,2);
	}
}
