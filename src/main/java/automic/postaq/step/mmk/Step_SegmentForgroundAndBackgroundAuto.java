package automic.postaq.step.mmk;

import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.RoiEnlarger;
import ij.plugin.ZProjector;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.AutoThresholder;
import ij.process.ImageProcessor;

public class Step_SegmentForgroundAndBackgroundAuto implements StepFunctions {
	//input parameters
	
	String inputImageTag;
	String foregroundRoiTag;
	String backgroundRoiTag;
	int segmentationChannelIndex;
	
	//?variables referenced by different functions
	Duplicator duplicator;
	ImageOpener imageOpener;
	ZProjector maxProjector;
	ThresholdToSelection thresholdToSelection;
	
	int backgroundDilationPixels=10;
	
	@Override
	public String getStepInformation(){
		return "Segment forground and background with auto threshold";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Foreground Roi Tag",	 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Background Roi Tag",	 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Input Image Tag",		 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Segmentation Channel Index",	null, null, ParameterType.INT_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("Background Dilation Pixels", 	null, backgroundDilationPixels, 	ParameterType.INT_PARAMETER);

		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		foregroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Foreground Roi Tag");
		backgroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Background Roi Tag");
		inputImageTag=			(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		segmentationChannelIndex=(int)	_stepParameterCollection.getParameterValue("Segmentation Channel Index");
		
		backgroundDilationPixels=(int)	_stepParameterCollection.getParameterValue("Background Dilation Pixels");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(foregroundRoiTag, "ROI");
		workingProcessor.addFileColumns(backgroundRoiTag, "ROI");
		
		duplicator=new Duplicator();
		imageOpener=new ImageOpener();
		maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		thresholdToSelection=new ThresholdToSelection();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		File inputImageFile=workingTable.getFile(_rowIndex, inputImageTag, "IMG");
		
		String outputFileName=String.format("Dataset_%05d", _rowIndex+1);
		
		if(!workingTable.getBooleanValue(_rowIndex, "Success")) return;
		
		ImagePlus inputImageStack=imageOpener.openImage(inputImageFile);
		
		if(inputImageStack.getNDimensions()>3)
			inputImageStack=duplicator.run(inputImageStack, segmentationChannelIndex, segmentationChannelIndex, 1, inputImageStack.getNSlices(), 1, 1);
			
		IJ.run(inputImageStack, "Grays", "");
		_data.showDebugImage(inputImageStack, "Original image in gray", true);

		maxProjector.setImage(inputImageStack);
		maxProjector.doProjection();

		ImagePlus maxProjectedImage=maxProjector.getProjection();
		_data.showDebugImage(maxProjectedImage, "Max Projected image", true);

		//IJ.setAutoThreshold(maxProjectedImage, "Triangle dark");
		ImageProcessor maxImageProcessor=maxProjectedImage.getProcessor();
		maxImageProcessor.setAutoThreshold(AutoThresholder.Method.Triangle, true);
		Roi foregroundRoi=thresholdToSelection.convert(maxImageProcessor);
		_data.showDebugImage(maxProjectedImage, "Foreground Roi", true, foregroundRoi);

		
		maxProjectedImage.setRoi(foregroundRoi);
		IJ.run(maxProjectedImage, "Make Inverse", "");
		
		Roi backgroundRoi=maxProjectedImage.getRoi();
		backgroundRoi=RoiEnlarger.enlarge(backgroundRoi, -backgroundDilationPixels);
		_data.showDebugImage(maxProjectedImage, "Background Roi", true, backgroundRoi);

		workingProcessor.saveRoiToTable(_rowIndex, foregroundRoi, foregroundRoiTag, outputFileName);
		workingProcessor.saveRoiToTable(_rowIndex, backgroundRoi, backgroundRoiTag, outputFileName);
		
		return;
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-3,10,10,2);
	}
}
