package automic.postaq.step.mmk;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ImageCalculator;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.utils.ThreadUtil;

public class Step_Remove_Local_Background_FAs_Roi  implements StepFunctions{
	
	
	//input parameters
	String inputImageTag;
	String inputForegroundRoiTag;
	String outputImageTag;
	double smoothingFilterRadius;
	double backgroundFilterRadius;
	
	Duplicator duplicator=null;
	
	@Override
	public String getStepInformation(){
		return "Subtract local background estimated with smoothing filter of a larger radius ";
	}
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		newParameterCollection.addParameter("Input Image Tag", 	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Input Foreground Roi Tag", 	null, null, ParameterType.STRING_PARAMETER);

		newParameterCollection.addParameter("Output Image Tag", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Smoothing filter radius", null, null, ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Background filter radius", null, null, ParameterType.DOUBLE_PARAMETER);

		
		return newParameterCollection;
	}

	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=	(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		inputForegroundRoiTag=	(String)_stepParameterCollection.getParameterValue("Input Foreground Roi Tag");
		outputImageTag=	(String)_stepParameterCollection.getParameterValue("Output Image Tag");
		smoothingFilterRadius=	(double)_stepParameterCollection.getParameterValue("Smoothing filter radius");
		backgroundFilterRadius=	(double)_stepParameterCollection.getParameterValue("Background filter radius");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		duplicator=new Duplicator();

		TableProcessor tableProcessor=_data.getWorkingProcessor();
		tableProcessor.addFileColumns(outputImageTag, "IMG");

	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		ImagePlus inputImage,outputImage;
		
		final String fileName=String.format("Dataset_%05d", _rowIndex+1);
		
		inputImage=ImageOpenerWithBioformats.openImage(workingProcessor.getFile(_rowIndex, inputImageTag, "IMG"));
		Roi foregroundRoi=ROIManipulator2D.flsToRois(workingProcessor.getFile(_rowIndex, inputForegroundRoiTag, "ROI"))[0];
		
		
		IJ.run(inputImage, "32-bit", "");
		IJ.run(inputImage, "Set Scale...", "distance=0 known=0 unit=pixel");
		
		_data.showDebugImage(inputImage, "Original image in gray", true);

		
		//image Gating
		ImageProcessor maskProcessor=new ByteProcessor(inputImage.getWidth(), inputImage.getHeight());
		maskProcessor.setColor(1);
		maskProcessor.fill(foregroundRoi);
		ImagePlus maskImage= new ImagePlus("",maskProcessor);
		_data.showDebugImage(maskImage, "Mask Image", true);
		
		ImagePlus gatedImage = new ImageCalculator().run("Multiply create 32-bit", inputImage, maskImage);
		_data.showDebugImage(gatedImage, "Gated Image", true);

		ImagePlus backgroundMaskImage=maskImage.duplicate();
		ImagePlus backgroundGatedImage=gatedImage.duplicate();
		
		
		int ncpu=ThreadUtil.getNbCpus();
		ImageFloat originalHandler=new ImageFloat(inputImage);
		ImageFloat smoothHandler=FastFilters3D.filterFloatImage(originalHandler, FastFilters3D.MEDIAN, (float)smoothingFilterRadius,(float)smoothingFilterRadius, 0, ncpu,false);
		_data.showDebugImage(smoothHandler.getImagePlus(), "Smooth image", true);
		ImageFloat backgroundHandler=FastFilters3D.filterFloatImage(originalHandler, FastFilters3D.MEDIAN, (float)backgroundFilterRadius,(float)backgroundFilterRadius, 0, ncpu,false);
		_data.showDebugImage(smoothHandler.getImagePlus(), "Background image", true);
		ImageFloat cleanHandler=(ImageFloat)smoothHandler.addImage(backgroundHandler, 1, -1);
		_data.showDebugImage(cleanHandler.getImagePlus(), "background subtracted stack", true);

		outputImage=cleanHandler.getImagePlus();
		
		
		workingProcessor.saveImageToTable(_rowIndex, outputImage, outputImageTag, fileName);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-1,10,10,2);
	}
}
