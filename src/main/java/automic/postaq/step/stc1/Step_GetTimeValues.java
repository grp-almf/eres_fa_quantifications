package automic.postaq.step.stc1;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import automic.parameters.ParameterCollection;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;

import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import ij.ImagePlus;
import ij.plugin.ImageInfo;
import loci.common.services.ServiceFactory;
import loci.formats.IFormatReader;
import loci.formats.in.ZeissCZIReader;
import loci.formats.meta.IMetadata;
import loci.formats.services.OMEXMLService;


public class Step_GetTimeValues implements StepFunctions {
	
	//input parameters
	
	//parameters currently not recordered
	String[] rawPartTags={"Part1","Part2","Part3"};
	String skipFramesTag="Skip.Frame.Index";
	String drugTimeTag="Date.Time.Durg";
	int nPossiblleParts=rawPartTags.length;
	
	//output parameters
	String outputTableTag="Time.Values";
	
	ImageOpenerInterface imageOpener;
	
	
	@Override
	public String getStepInformation(){
		return "Get time values";
	}
	
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();

		//parameters without default values
		
		//parameters with default values
		
		
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor tableProcessor=_data.getWorkingProcessor();
		
		tableProcessor.addFileColumns(outputTableTag, "TXT");
		
		imageOpener=new ImageOpener();
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		TableModel workingTable=_data.getWorkingTable();
		
		//concatenate parts and get time tags
		String drugDateTime=workingTable.getStringValue(_rowIndex, drugTimeTag);
		List<Double> timeValues=new ArrayList<Double>();
		for (int partIndex=0;partIndex<nPossiblleParts;partIndex++)
			addTimeValuesToList(timeValues, workingTable,_rowIndex, rawPartTags[partIndex],drugDateTime);
		
		
		
		//remove unwanted slice
		Double skipIndex=workingTable.getNumericValue(_rowIndex, skipFramesTag);
		
		if (!skipIndex.isNaN()){
			int skipIndexInt=skipIndex.intValue();
			timeValues.remove(skipIndexInt-1);
		}

		//create table with time tags
		TableModel outputTable=new TableModel(workingTable.getRootPath());
		outputTable.addValueColumn("Frame.Index", "NUM");
		outputTable.addValueColumn("Time.Seconds", "NUM");
		
		int nTimePoints=timeValues.size();
		
		for (int timeIndex=0;timeIndex<nTimePoints;timeIndex++){
			outputTable.addRow();
			outputTable.setNumericValue(timeIndex+1, timeIndex, "Frame.Index");
			outputTable.setNumericValue(timeValues.get(timeIndex), timeIndex, "Time.Seconds");
		}
		
		//save output table
		workingProcessor.saveTableToTable(_rowIndex, outputTable, outputTableTag, String.format("Dataset--%05d", _rowIndex+1));
		
		
		return;
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}

	private void addTimeValuesToList(List<Double> _timeList,TableModel _dataTable, int _rowIndex, String _imageTag, String _drugDateTimeString)throws Exception{
		
		File imageFile=_dataTable.getFile(_rowIndex, _imageTag, "IMGS");
		if (imageFile==null)
			return;
		
		ImagePlus originalImage=imageOpener.openImage(_dataTable, _rowIndex, _imageTag, "IMGS");
		
		File folderFile=imageFile.getParentFile();
		String[] nameElements=imageFile.getName().split(" #");
		String fullPath= new File(folderFile,nameElements[0]).getAbsolutePath();
		int seriesIndex=Integer.parseInt(nameElements[1])-1;
		
		
		//ImageReader reader = new ImageReader();
		IFormatReader reader=new ZeissCZIReader();
		//IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		ServiceFactory factory = new ServiceFactory();
		OMEXMLService service = factory.getInstance(OMEXMLService.class);
	    IMetadata omeMeta = service.createOMEXMLMetadata();
		
	    reader.setMetadataStore(omeMeta);
		reader.setId(fullPath);
		
		
		//calculate time offset
		
		
		DateFormat DrugDateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("us"));
		Date drugDate=DrugDateFormat.parse(_drugDateTimeString);
		long drugMillis=drugDate.getTime();
		
		String[] infoInputs=new ImageInfo().getImageInfo(originalImage).split("\n");
		String imageDateString=null;
		String settingName="Information|Document|CreationDate #1 = ";
		for (String infoInput:infoInputs){
			if (infoInput.startsWith(settingName)){
				imageDateString=infoInput.substring(settingName.length(), infoInput.length());
				break;
			}
		}
		imageDateString=imageDateString.replace('T', ' ');
		System.out.println(imageDateString);
		DateFormat ImageDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("us"));
		Date imageDate=ImageDateFormat.parse(imageDateString);
		long imageMillis=imageDate.getTime();
		
		//
		
		int n_tpoints=omeMeta.getPixelsSizeT(seriesIndex).getValue();
		

		double offset=omeMeta.getPlaneDeltaT(seriesIndex,reader.getIndex(0,0,0)).value().doubleValue();
		double offsetDrug=(drugMillis-imageMillis)/1000.0;
		for (int tIndex=0; tIndex<n_tpoints; tIndex++){
			_timeList.add(omeMeta.getPlaneDeltaT(seriesIndex,reader.getIndex(0,0,tIndex)).value().doubleValue()-offset-offsetDrug);
		}
		reader.close();
	}


}
