package automic.postaq.step.stc1;


import java.awt.Color;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;

import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import automic.utils.roi.ROIManipulator2D;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.gui.WaitForUserDialog;
import ij.plugin.frame.RoiManager;


public class Step_DefineManualROIs implements StepFunctions {
	
	//input parameters
	String inputImageTag;
	String dialogMessage="Define ROIs";
	boolean rememberStackPosition=false;
	
	double roiStrokeWidth=1;
	String roiStrokeColor="#FFC0CB";
	//parameters currently not recordered
	
	
	//output parameters
	String outputRoisTag;
	
	
	ImageOpenerInterface imageOpener;

	RoiManager roiManager;
	
	boolean visibleRoiManager;
	Color roiColor;
	
	
	@Override
	public String getStepInformation(){
		return "Define ROIs Manually";
	}
	
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();

		//parameters without default values
		newParameterCollection.addParameter("Input Image Tag", 			null, null, 				ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Output Rois Tag", 			null, null, 				ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("Dialog Message",			null,dialogMessage,			ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Remember Stack Position",	null,rememberStackPosition,	ParameterType.BOOL_PARAMETER);
		newParameterCollection.addParameter("Roi Stroke Width",			null,roiStrokeWidth,		ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Roi Stroke Color",			null,roiStrokeColor,		ParameterType.STRING_PARAMETER);

		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=			(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		outputRoisTag=			(String)_stepParameterCollection.getParameterValue("Output Rois Tag");
		
		dialogMessage=			(String)_stepParameterCollection.getParameterValue("Dialog Message");
		rememberStackPosition=	(boolean)_stepParameterCollection.getParameterValue("Remember Stack Position");		

		roiStrokeWidth=			(double)_stepParameterCollection.getParameterValue("Roi Stroke Width");
		roiStrokeColor=			(String)_stepParameterCollection.getParameterValue("Roi Stroke Color");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor tableProcessor=_data.getWorkingProcessor();
		
		tableProcessor.addFileColumns(outputRoisTag, "ROI");
		
		imageOpener=new ImageOpener();
		roiManager=ROIManipulator2D.getEmptyRm();
		
		visibleRoiManager=roiManager.isVisible();
		roiManager.setVisible(true);
		
		roiColor=Color.decode(roiStrokeColor);
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		TableModel workingTable=_data.getWorkingTable();
		
		ImagePlus inputImage=imageOpener.openImage(workingTable, _rowIndex, inputImageTag, "IMG");
		inputImage.show();
		roiManager.runCommand("Reset");
		new WaitForUserDialog(dialogMessage, String.format("Dataset %d out of %d", _rowIndex+1,workingTable.getRowCount())).show();
		
		Roi[] targetRois=roiManager.getRoisAsArray();
		if(!ROIManipulator2D.isEmptyRoiArr(targetRois)){
			int iRoi=0;
			for (Roi roi:targetRois){
				iRoi++;
				roi.setStrokeColor(roiColor);
				roi.setStrokeWidth(roiStrokeWidth);
				roi.setName(String.format("r_%04d_%04d", _rowIndex,iRoi));
				if (! rememberStackPosition){
					roi.setPosition(0);
					roi.setPosition(0, 0, 0);
				}
			}
			workingProcessor.saveRoisToTable(_rowIndex, targetRois, outputRoisTag, String.format("Dataset--%05d", _rowIndex+1));
		}
			
		roiManager.runCommand("Reset");
		
		inputImage.changes=false;
		inputImage.close();
		
		return;
	}
	
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{
		roiManager.setVisible(visibleRoiManager);
	}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
}
