package automic.postaq.step.stc1;


import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;

import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.plugin.GroupedZProjector;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;


public class Step_QuantifyCellIntensities implements StepFunctions {
	
	//input parameters
	String inputImageTag;
	String cellRoiTag;
	String backgroundRoiTag;
	boolean rememberStackPosition=false;
	
	
	
	//output parameters
	String outputTableTag;
	
	
	ImageOpenerInterface imageOpener;
	GroupedZProjector sumProjector;
	
	
	@Override
	public String getStepInformation(){
		return "Quantify Cell Intensities";
	}
	
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();

		//parameters without default values
		newParameterCollection.addParameter("Input Image Tag", 			null, null, 				ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Cell Roi Tag",		 		null, null,		 			ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Background Roi Tag", 		null, null,		 			ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Output Table Tag", 		null, null,		 			ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=			(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		cellRoiTag=				(String)_stepParameterCollection.getParameterValue("Cell Roi Tag");
		backgroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Background Roi Tag");
		outputTableTag=			(String)_stepParameterCollection.getParameterValue("Output Table Tag");
		
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor tableProcessor=_data.getWorkingProcessor();
		
		tableProcessor.addFileColumns(outputTableTag, "TXT");
		
		imageOpener=new ImageOpener();
		sumProjector=new GroupedZProjector();
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		TableModel workingTable=_data.getWorkingTable();
		
		ImagePlus inputImage=imageOpener.openImage(workingTable, _rowIndex, inputImageTag, "IMG");
		IJ.run(inputImage, "Set Scale...", "distance=0");
		IJ.run(inputImage, "32-bit", "");
		ImagePlus sumImage=sumProjector.groupZProject(inputImage, ZProjector.SUM_METHOD, inputImage.getNSlices());
		_data.showDebugImage(sumImage, "Projected image", false);
		
		
		
		
		Roi cellRoi=ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, cellRoiTag, "ROI"))[0];
		Roi backgroundRoi=ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, backgroundRoiTag, "ROI"))[0];


		ImageStack sumStack=sumImage.getStack();
		int nTimePoints=sumStack.size();
		
		//create table with time tags
		TableModel outputTable=new TableModel(workingTable.getRootPath());
		outputTable.addValueColumn("Frame.Index", "NUM");
		outputTable.addValueColumn("Cell.Integrated.Intensity", "NUM");
		
		double cellMeanIntensity,backgroundMeanIntensity,cellArea;
		ImageProcessor tProcessor;
		ImageStatistics cellStatistics;
		for (int timeIndex=0;timeIndex<nTimePoints;timeIndex++){
			tProcessor=sumStack.getProcessor(timeIndex+1);
			tProcessor.setRoi(cellRoi);
			cellStatistics=tProcessor.getStatistics();
			cellMeanIntensity=cellStatistics.mean;
			cellArea=cellStatistics.area;
			
			tProcessor.setRoi(backgroundRoi);
			backgroundMeanIntensity=tProcessor.getStatistics().mean;
			tProcessor.resetRoi();
			
			outputTable.addRow();
			outputTable.setNumericValue(timeIndex+1, timeIndex, "Frame.Index");
			outputTable.setNumericValue((cellMeanIntensity-backgroundMeanIntensity)*cellArea, timeIndex, "Cell.Integrated.Intensity");
		}
		
		//save output table
		workingProcessor.saveTableToTable(_rowIndex, outputTable, outputTableTag, String.format("Dataset--%05d", _rowIndex+1));
	
		
		return;
	}
	
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
}
