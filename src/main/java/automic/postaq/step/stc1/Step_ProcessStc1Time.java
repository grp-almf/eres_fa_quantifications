package automic.postaq.step.stc1;


import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Point2D;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import automic.parameters.ParameterCollection;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;

import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.TextRoi;
import ij.plugin.Concatenator;
import ij.plugin.GroupedZProjector;
import ij.plugin.HyperStackConverter;
import ij.plugin.ImageInfo;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import loci.common.services.ServiceFactory;
import loci.formats.IFormatReader;
import loci.formats.in.ZeissCZIReader;
import loci.formats.meta.IMetadata;
import loci.formats.services.OMEXMLService;
import mpicbg.imglib.algorithm.fft.PhaseCorrelation;
import mpicbg.imglib.algorithm.fft.PhaseCorrelationPeak;
import mpicbg.imglib.image.ImagePlusAdapter;


public class Step_ProcessStc1Time implements StepFunctions {
	
	//input parameters
	
	//parameters currently not recordered
	String[] rawPartTags={"Part1","Part2","Part3"};
	String skipFramesTag="Skip.Frame.Index";
	String drugTimeTag="Date.Time.Durg";
	int nPossiblleParts=3;
	double filterRadius=1;
	double backgroundSubtraction=10000;
	
	//output parameters
	String outputStackTag="Stacks.Processed";
	String outputProjectionsTag="Projected";
	
	
	ImageOpenerInterface imageOpener;
	Concatenator concatenator;
	GroupedZProjector maxProjector;
	
	
	
	@Override
	public String getStepInformation(){
		return "Process STC1 time stacks";
	}
	
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();

		//parameters without default values
		
		//parameters with default values
		
		
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor tableProcessor=_data.getWorkingProcessor();
		
		tableProcessor.addFileColumns(outputStackTag, "IMG");
		tableProcessor.addFileColumns(outputProjectionsTag, "IMG");
		
		imageOpener=new ImageOpener();
		concatenator=new Concatenator();
		maxProjector=new GroupedZProjector();
		//maxProjector.setMethod(ZProjector.MAX_METHOD);

		
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		TableModel workingTable=_data.getWorkingTable();
		
		//open parts
		ImagePlus[] partImages=new ImagePlus[nPossiblleParts];
		for (int partIndex=0;partIndex<nPossiblleParts;partIndex++){
			partImages[partIndex]=imageOpener.openImage(workingTable, _rowIndex, rawPartTags[partIndex], "IMGS");
			
			if(partImages[partIndex]==null){
				_data.showDebugMessage(String.format("%s image is null", rawPartTags[partIndex]));
			}else{
				_data.showDebugMessage(String.format("%s image is not null", rawPartTags[partIndex]));
				_data.showDebugImage(partImages[partIndex], String.format("%s original", rawPartTags[partIndex]), false);
			}
				
			
		}
		
		//concatenate parts and get time tags
		String drugDateTime=workingTable.getStringValue(_rowIndex, drugTimeTag);
		List<String> timeTags=new ArrayList<String>();
		ImagePlus concatenatedImage=null;
		addTimeTagsToList(timeTags, workingTable,partImages[0], _rowIndex, rawPartTags[0],drugDateTime);
		if(partImages[1]!=null){
			addTimeTagsToList(timeTags, workingTable,partImages[1], _rowIndex, rawPartTags[1],drugDateTime);
			concatenatedImage=concatenator.concatenateHyperstacks(new ImagePlus[]{partImages[0],partImages[1]}, "Concatenated", false);
			if(partImages[2]!=null){
				addTimeTagsToList(timeTags, workingTable,partImages[2], _rowIndex, rawPartTags[2],drugDateTime);
				concatenatedImage=concatenator.concatenateHyperstacks(new ImagePlus[]{concatenatedImage,partImages[2]}, "Concatenated", false);
			}
			concatenatedImage = HyperStackConverter.toHyperStack(concatenatedImage, 1, partImages[0].getNSlices(), concatenatedImage.getStackSize()/partImages[0].getNSlices(), "default", "Color");
		}else{
			concatenatedImage=partImages[0];
		}
		int nTimePoints=concatenatedImage.getNFrames();
		
		
		//process concatenated data
		//convert to 32-bit
		//IJ.run(concatenatedImage, "32-bit", "");
		//Subtract background
		IJ.run(concatenatedImage, "Subtract...", String.format("value=%f stack",backgroundSubtraction));
		//filter
		IJ.run(concatenatedImage, "Median...", String.format("radius=%f stack", filterRadius));
		//convert back to 16-bit
		//IJ.run(concatenatedImage, "16-bit", "");
		//max project
		//maxProjector.setImage(concatenatedImage);
		//maxProjector.doHyperStackProjection(false);
		//ImagePlus maxImage=maxProjector.getProjection();
		ImagePlus maxImage=maxProjector.groupZProject(concatenatedImage, ZProjector.MAX_METHOD, concatenatedImage.getNSlices());
		_data.showDebugImage(maxImage, "Projected image", false);
		
		
		//remove unwanted slice
		ImageStack maxStack=maxImage.getStack();
		Double skipIndex=workingTable.getNumericValue(_rowIndex, skipFramesTag);
		maxStack=maxImage.getStack();
		
		if (!skipIndex.isNaN()){
			int skipIndexInt=skipIndex.intValue();
			maxStack.deleteSlice(skipIndexInt);
			concatenatedImage.setT(skipIndexInt);
			IJ.run(concatenatedImage, "Delete Slice", "delete=frame");
			timeTags.remove(skipIndexInt-1);
			nTimePoints--;
		}
		
		//align max projection
		
		Point2D[] shifts=new Point2D[nTimePoints];
		ImageStack concatenatedStack=concatenatedImage.getImageStack();
		for (int timepoint=1;timepoint<nTimePoints;timepoint++){
			ImageProcessor nextProcessor=maxStack.getProcessor(timepoint+1);
			ImageProcessor previousProcessor=maxStack.getProcessor(timepoint);
			
			Point2D shift=computeShiftUsingPhaseCorrelation(new ImagePlus("Image to align",nextProcessor), new ImagePlus("Reference image",previousProcessor), _data);
			double xShift=-shift.getX();
			double yShift=-shift.getY();
			shifts[timepoint]=shift;
			
			nextProcessor.translate(xShift, yShift);
			
			for (int sliceIndex=1; sliceIndex<=concatenatedImage.getNSlices();sliceIndex++){
				concatenatedStack.getProcessor(concatenatedImage.getStackIndex(1, sliceIndex, timepoint+1)).translate(xShift, yShift);
			}
		}
		
		//apply time stamps to max projections
		for (int timepoint=0;timepoint<nTimePoints;timepoint++){
			TextRoi tagRoi=new TextRoi(30, 30, timeTags.get(timepoint), new Font("Arial", Font.BOLD, 75));
			ImageProcessor targetProcessor=maxStack.getProcessor(timepoint+1);
			targetProcessor.setColor(Color.white);
			targetProcessor.draw(tagRoi);
			
			for (int sliceIndex=1; sliceIndex<=concatenatedImage.getNSlices();sliceIndex++){
				targetProcessor=concatenatedStack.getProcessor(concatenatedImage.getStackIndex(1, sliceIndex, timepoint+1));
				targetProcessor.setColor(Color.white);
				targetProcessor.draw(tagRoi);
			}
			
		}
		
		//Put new "drug added" slice
		
		
		//save max projection and stack
		_data.showDebugImage(maxImage,"Final Max image", true);
		workingProcessor.saveImageToTable(_rowIndex, maxImage, outputProjectionsTag, String.format("Dataset--%05d", _rowIndex+1));
		workingProcessor.saveImageToTable(_rowIndex, concatenatedImage, outputStackTag, String.format("Dataset--%05d", _rowIndex+1));
		
		
		return;
		//String eresTimeImageFileApth=workingProcessor.getFileAbsolutePath(_rowIndex, rawPart1Tag, "IMGS");
		
		//workingProcessor.setValue(getSuccessWord(outputCode), _rowIndex, "Analysis.Result", null);
	}
	
	
	/**
	 * computes shift between two images
	 * copied and modified from code of Tischi (bigDataTracker/ObjectTracker)
	 * here we assume no shift in z as we deal with 2D data
	 * @param imp1
	 * @param imp0
	 * @return Point2D object containing coordinates of shift vector 
	 */
	
	private Point2D computeShiftUsingPhaseCorrelation(ImagePlus imp1, ImagePlus imp0, StepData _data) {
        _data.showDebugMessage("PhaseCorrelation phc = new PhaseCorrelation(...)");
		//if( _data.logger.isShowDebug() )   logger.info();
        PhaseCorrelation phc = new PhaseCorrelation(ImagePlusAdapter.wrap(imp1), ImagePlusAdapter.wrap(imp0), 5, true);
        _data.showDebugMessage("phc.process()... ");
        //if( logger.isShowDebug() )   logger.info("phc.process()... ");
        phc.process();
        // get the first peak that is not a clean 1.0,
        // because 1.0 cross-correlation typically is an artifact of too much shift into black areas of both images
        ArrayList<PhaseCorrelationPeak> pcp = phc.getAllShifts();
        float ccPeak = 0;
        int iPeak = 0;
        for(iPeak = pcp.size()-1; iPeak>=0; iPeak--) {
            ccPeak = pcp.get(iPeak).getCrossCorrelationPeak();
            if (ccPeak < 0.999) break;
        }
        //info(""+ccPeak);
        int[] shift = pcp.get(iPeak).getPosition();
        return new Point2D.Double(shift[0],shift[1]);
    }
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}

	private void addTimeTagsToList(List<String> _tagList,TableModel _dataTable,ImagePlus _originalImage, int _rowIndex, String _imageTag, String _drugDateTimeString)throws Exception{
		
		File imageFile=_dataTable.getFile(_rowIndex, _imageTag, "IMGS");
		File folderFile=imageFile.getParentFile();
		String[] nameElements=imageFile.getName().split(" #");
		String fullPath= new File(folderFile,nameElements[0]).getAbsolutePath();
		int seriesIndex=Integer.parseInt(nameElements[1])-1;
		
		
		//ImageReader reader = new ImageReader();
		IFormatReader reader=new ZeissCZIReader();
		//IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		ServiceFactory factory = new ServiceFactory();
		OMEXMLService service = factory.getInstance(OMEXMLService.class);
	    IMetadata omeMeta = service.createOMEXMLMetadata();
		
	    reader.setMetadataStore(omeMeta);
		reader.setId(fullPath);
		
		
		//calculate time offset
		
		
		DateFormat DrugDateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("us"));
		Date drugDate=DrugDateFormat.parse(_drugDateTimeString);
		long drugMillis=drugDate.getTime();
		
		String[] infoInputs=new ImageInfo().getImageInfo(_originalImage).split("\n");
		String imageDateString=null;
		String settingName="Information|Document|CreationDate #1 = ";
		for (String infoInput:infoInputs){
			if (infoInput.startsWith(settingName)){
				imageDateString=infoInput.substring(settingName.length(), infoInput.length());
				break;
			}
		}
		imageDateString=imageDateString.replace('T', ' ');
		System.out.println(imageDateString);
		DateFormat ImageDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("us"));
		Date imageDate=ImageDateFormat.parse(imageDateString);
		long imageMillis=imageDate.getTime();
		
		//
		
		int n_tpoints=omeMeta.getPixelsSizeT(seriesIndex).getValue();
		
		double[] deltaTValues=new double[n_tpoints];

		double offset=omeMeta.getPlaneDeltaT(seriesIndex,reader.getIndex(0,0,0)).value().doubleValue();
		double offsetDrug=(drugMillis-imageMillis)/1000.0;
		for (int tIndex=0; tIndex<n_tpoints; tIndex++){
			deltaTValues[tIndex]=omeMeta.getPlaneDeltaT(seriesIndex,reader.getIndex(0,0,tIndex)).value().doubleValue()-offset-offsetDrug;//-omeMeta.getPlaneDeltaT(0,0).value().doubleValue();//reader.getIndex(0,0,m)
			_tagList.add(String.format("%02d:%02d",(int)(deltaTValues[tIndex]/60),Math.abs((int)(deltaTValues[tIndex]%60))));
		}
		reader.close();
		
		
		
	}


}
