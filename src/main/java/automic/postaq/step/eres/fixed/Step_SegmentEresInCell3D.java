package automic.postaq.step.eres.fixed;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
//import ij.ImageStack;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import mcib3d.geom.Object3D;
//import ij.process.ImageProcessor;
import mcib3d.geom.Objects3DPopulation;
//import mcib3d.image3d.ImageFloat;
//import mcib3d.image3d.ImageHandler;
//import mcib3d.image3d.ImageShort;
//import mcib3d.image3d.processing.FastFilters3D;
//import mcib3d.utils.ThreadUtil;
import process.spotseg.ERESSegm_3D;

public class Step_SegmentEresInCell3D implements StepFunctions {
	//input parameters
	
	String eresImageTag;
	String cellRoiTag;
	String segmentedEres3DRoiTag;
	String segmentedEresNumberTag;
	String processBooleanTag;
	
	//non-passed parameters
	
	//stack preprocessing and seeg finding
	double smoothXY=1;
	double smoothZ=1;
	double smoothBackXY=10;
	double smoothBackZ=3;
	double findMaxXY=2.5;
	double findMaxZ=1.4;
	
	//stack ERES segmentation
	private static int localThreshold=0;
	private static int seedThreshold=6000;
	private static int volumeMin=10;
	private static int volumeMax=1000;
	
	//special for Local Gauss segmentation
	private static int maximumRadius=10;
	private static double sd=2;//1.285;

	
	
	private int eresChannelIndex=1;
	
//	double nucMinSize=3000;
//	double nucFilterSize=10;
//	double nucThresh=10000;
	
	
	//?variables referenced by different functions
	//ImagePlus dapiImage=null;
	//Roi [] nucRois=null;
	
	ImageOpener imageOpener;
	Duplicator duplicator;
	
	@Override
	public String getStepInformation(){
		return "Performs 3D segmentation of ERES from 3D stack.\n"
				+ "Limits segmentation to the cell roi.";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Eres image Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Cell Roi Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Segmented ERES 3D ROI Tag", 	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Segmented ERES Number Tag", 	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Process flag column tag",		null, null, ParameterType.STRING_PARAMETER);

		
		//parameters with default values
		newParameterCollection.addParameter("Eres Channel Index",			null, eresChannelIndex, ParameterType.INT_PARAMETER);

		
//		newParameterCollection.addParameter("Minimal nucleus size", 	null, nucMinSize, 	ParameterType.DOUBLE_PARAMETER);
//		newParameterCollection.addParameter("Filter Size Nucleus",		null, nucFilterSize,ParameterType.DOUBLE_PARAMETER);
//		newParameterCollection.addParameter("Nucleus Threshold",		null, nucThresh,	ParameterType.DOUBLE_PARAMETER);
		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		eresImageTag=			(String)_stepParameterCollection.getParameterValue("Eres image Tag");
		cellRoiTag=				(String)_stepParameterCollection.getParameterValue("Cell Roi Tag");
		segmentedEres3DRoiTag=	(String)_stepParameterCollection.getParameterValue("Segmented ERES 3D ROI Tag");
		segmentedEresNumberTag=	(String)_stepParameterCollection.getParameterValue("Segmented ERES Number Tag");
		processBooleanTag=		(String)_stepParameterCollection.getParameterValue("Process flag column tag");

		eresChannelIndex=		(Integer)_stepParameterCollection.getParameterValue("Eres Channel Index");
		
//		nucMinSize=			(Double)_stepParameterCollection.getParameterValue("Minimal nucleus size");
//		nucFilterSize=		(Double)_stepParameterCollection.getParameterValue("Filter Size Nucleus");
//		nucThresh=			(Double)_stepParameterCollection.getParameterValue("Nucleus Threshold");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(segmentedEres3DRoiTag, "ROI");
		workingProcessor.addValueColumn(segmentedEresNumberTag, "NUM");
		
		imageOpener=new ImageOpener();
		duplicator=new Duplicator();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		if (!workingTable.getBooleanValue(_rowIndex, processBooleanTag))//do not process unnecessary datasets
			return;
		
		ImagePlus	originalStackImage=imageOpener.openImage(workingTable.getFile(_rowIndex, eresImageTag, "IMG"));
		if (originalStackImage.getNChannels()>1)
			originalStackImage=duplicator.run(originalStackImage, eresChannelIndex, eresChannelIndex, 1, originalStackImage.getNSlices(), 1, originalStackImage.getNFrames());
		
		IJ.run(originalStackImage, "32-bit", "");
		
		File 		cellRoiFile=workingTable.getFile(_rowIndex, cellRoiTag, "ROI");
		Roi 		cellRoi= ROIManipulator2D.flsToRois(cellRoiFile)[0];
		_data.showDebugMessage("Cell Roi bounds: "+cellRoi.getBounds());
		
		Objects3DPopulation eres3DRois=segmentEres(originalStackImage,cellRoi,_data);
		
		int eresNumber=eres3DRois.getNbObjects();
		workingProcessor.setValue((double)eresNumber, _rowIndex, segmentedEresNumberTag, "NUM");
		
		if (eresNumber>0){
			workingProcessor.save3DRoisToTable(_rowIndex, eres3DRois, segmentedEres3DRoiTag, FileUtils.cutExtension(cellRoiFile.getName()));
		}
		return;
	}
	
	
	private Objects3DPopulation segmentEres(ImagePlus _stackImage,Roi _cellRoi,StepData _data)throws Exception{
		_data.showDebugImage(_stackImage, "32-bit raw data with cell Roi", true,_cellRoi);
		
		Rectangle cellRoiRectangle=_cellRoi.getBounds();
		
		this.fillBlackOutOfCell(_stackImage, _cellRoi);

		_data.showDebugImage(_stackImage, "raw data with black out of cell", true);
		
		_stackImage.setRoi(_cellRoi);
		
		ImagePlus cropImage=_stackImage.duplicate();
		cropImage.setRoi((Roi)null);
		_stackImage.setRoi((Roi)null);
		_data.showDebugImage(cropImage, "Crop Image", true);
		
//		int ncpu=ThreadUtil.getNbCpus();
//		ImageFloat originalHandler=new ImageFloat(_stackImage);
//		ImageFloat smoothHandler=FastFilters3D.filterFloatImage(originalHandler, FastFilters3D.MEDIAN, (float)smoothXY,(float)smoothXY, (float)smoothZ, ncpu,false);
//		_data.showDebugImage(smoothHandler.getImagePlus(), "Smooth stack", true, null);
//		ImageFloat backgroundHandler=FastFilters3D.filterFloatImage(originalHandler, FastFilters3D.MEDIAN, (float)smoothBackXY,(float)smoothBackXY, (float)smoothBackZ, ncpu,/*false*/true);
//		_data.showDebugImage(backgroundHandler.getImagePlus(), "background stack", true, null);
//		ImageFloat cleanHandler=(ImageFloat)smoothHandler.addImage(backgroundHandler, 1, -1);
//		_data.showDebugImage(cleanHandler.getImagePlus(), "background suptracted stack", true, null);
//		
//		ImageFloat seedHandler= FastFilters3D.filterFloatImage(cleanHandler,FastFilters3D.MAXLOCAL,(float)findMaxXY,(float)findMaxXY,(float)findMaxZ,ncpu,false);
//		_data.showDebugImage(seedHandler.getImagePlus(), "ERES seeds", true, null);


		
		ERESSegm_3D eresSegmentor=new ERESSegm_3D();
		eresSegmentor.setFiltParams(smoothXY, smoothZ, smoothBackXY, smoothBackZ, findMaxXY, findMaxZ);
		eresSegmentor.setSegLocalGauss(maximumRadius, sd, volumeMin, volumeMax, seedThreshold, localThreshold);
		
		
		Objects3DPopulation segmentedEresPopulation=eresSegmentor.segment3D(cropImage);
		ImagePlus cleanedImage=eresSegmentor.getCleanedImagePlus();
		_data.showDebugImage(cleanedImage, "Cleaned image", true);
		_data.showDebugImage(cleanedImage, "Cleaned image with segmented ERES overlay", true, segmentedEresPopulation,Color.ORANGE);
		_data.showDebugImage(eresSegmentor.getMaskImage(), "ERES Mask image", true);

		for (Object3D eresObject:segmentedEresPopulation.getObjectsList())
			eresObject.translate(cellRoiRectangle.x, cellRoiRectangle.y, 0);
		
		_data.showDebugImage(_stackImage, "Original image with segmented ERES overlay", true, segmentedEresPopulation,Color.ORANGE);
		
		return  segmentedEresPopulation;//new Objects3DPopulation();//
	}
	
	private void fillBlackOutOfCell(ImagePlus _stackImage,Roi _cellRoi){
		IJ.setForegroundColor(0, 0, 0);
		_stackImage.setRoi((Roi)_cellRoi.clone());//why I need it here? inversion on the image seems to affect ROI
		IJ.run(_stackImage, "Make Inverse", "");
		IJ.run(_stackImage, "Fill", "stack");
		IJ.run(_stackImage, "Select None", "");
		
//		ImageStack stack=_stackImage.getStack();
//		int numbelOfSlices=stack.getSize();
//		
//		ImageProcessor processor;
//		for (int sliceIndex=1;sliceIndex<=numbelOfSlices;sliceIndex++){
//			processor=stack.getProcessor(sliceIndex);
//			processor.setColor(0);
//			processor.setRoi(_cellRoi);
//			processor.in
//			processor.fillOutside(_cellRoi);
//		}
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
}
