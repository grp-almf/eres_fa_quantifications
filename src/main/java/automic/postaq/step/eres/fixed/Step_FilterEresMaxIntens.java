package automic.postaq.step.eres.fixed;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ROIManipulator3D;
import automic.utils.roi.RoiFilter3D;
import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom.Objects3DPopulation;

public class Step_FilterEresMaxIntens implements StepFunctions {
	//input parameters
	
	String isolatedCellImageTag;
	String eres3DRoiTag;
	String processBooleanTag;
	double maxIntensityThreshold;
	
	String outputFilteredEresRoiTag;

	
	//non-passed parameters
	
	ImageOpener imageOpener;
	
	@Override
	public String getStepInformation(){
		return "Filter ERES based on Max intensity";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Isolated Cell Image Tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES 3D ROI Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Process flag column tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Max Intensity Threshold",		null, null, ParameterType.DOUBLE_PARAMETER);

		newParameterCollection.addParameter("Filtered ERES 3D ROI tag",		null, null, ParameterType.STRING_PARAMETER);
		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		isolatedCellImageTag=		(String)_stepParameterCollection.getParameterValue("Isolated Cell Image Tag");
		eres3DRoiTag=				(String)_stepParameterCollection.getParameterValue("ERES 3D ROI Tag");
		processBooleanTag=			(String)_stepParameterCollection.getParameterValue("Process flag column tag");
		maxIntensityThreshold=		(double)_stepParameterCollection.getParameterValue("Max Intensity Threshold");
		
		outputFilteredEresRoiTag=	(String)_stepParameterCollection.getParameterValue("Filtered ERES 3D ROI tag");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(outputFilteredEresRoiTag, "ROI");
		
		imageOpener=new ImageOpener();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		if (!workingTable.getBooleanValue(_rowIndex, processBooleanTag))//do not process unnecessary datasets
			return;
		
		ImagePlus	isolatedStackImage=imageOpener.openImage(workingTable.getFile(_rowIndex, isolatedCellImageTag, "IMG"));
		IJ.run(isolatedStackImage, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
		
		String roiFileName=workingTable.getFileName(_rowIndex, eres3DRoiTag, "ROI");
		if (roiFileName==null)
			return;
		
		Objects3DPopulation eresPopulation=ROIManipulator3D.fileToPopulation(workingTable.getFile(_rowIndex, eres3DRoiTag, "ROI"));
		RoiFilter3D roi3dFilter=new RoiFilter3D(eresPopulation);
		roi3dFilter.setImagePlus(isolatedStackImage);
		roi3dFilter.filterThr(RoiFilter3D.MAX, maxIntensityThreshold, Double.MAX_VALUE, true);
		Objects3DPopulation filteredPopulation=roi3dFilter.getPassedPopulation();
		if (filteredPopulation.getNbObjects()>0)
			workingProcessor.save3DRoisToTable(_rowIndex, filteredPopulation, outputFilteredEresRoiTag, FileUtils.cutExtension(roiFileName));
		
		return;
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,100,2);
	}
}
