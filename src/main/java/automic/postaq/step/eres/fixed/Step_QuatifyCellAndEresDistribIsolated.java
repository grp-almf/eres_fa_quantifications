package automic.postaq.step.eres.fixed;


import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.roi.ROIManipulator3D;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;

public class Step_QuatifyCellAndEresDistribIsolated implements StepFunctions {
	//input parameters
	
	String eresImageTag;
	String isolatedCellImageTag;
	String cellRoiTag;
	String eres3DRoiTag;
	String backgroundRoiTag;
	String eresNumberTag;
	String processBooleanTag;

	String outputCellAreaTag;
	String outputTotalIntensityTag;
	String outputEresRatioTag;
	String outputEresTableTag;

	
	private int eresChannelIndex=1;

	
	//non-passed parameters
	
	ImageOpener imageOpener;
	ZProjector sumProjector;
	Duplicator duplicator;
	
	
	Double[] backgroundIntensitiesArray;
	
	@Override
	public String getStepInformation(){
		return "Quantifies cell area, integrated intensity of the cell "
				+ "and intensities of individual ERES";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Eres image Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Isolated Cell Image Tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Cell Roi Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES 3D ROI Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Background Roi Tag",			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Process flag column tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES Number Tag",				null, null, ParameterType.STRING_PARAMETER);

		newParameterCollection.addParameter("Cell Area Tag", 				null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Cell Integrated Intensity Tag",null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES Fraction Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES Table Tag",	 			null, null, ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("Eres Channel Index",			null, eresChannelIndex, ParameterType.INT_PARAMETER);

		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		eresImageTag=			(String)_stepParameterCollection.getParameterValue("Eres image Tag");
		isolatedCellImageTag=	(String)_stepParameterCollection.getParameterValue("Isolated Cell Image Tag");
		cellRoiTag=				(String)_stepParameterCollection.getParameterValue("Cell Roi Tag");
		eres3DRoiTag=			(String)_stepParameterCollection.getParameterValue("ERES 3D ROI Tag");
		backgroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Background Roi Tag");
		processBooleanTag=		(String)_stepParameterCollection.getParameterValue("Process flag column tag");
		eresNumberTag=			(String)_stepParameterCollection.getParameterValue("ERES Number Tag");
		
		outputCellAreaTag=		(String)_stepParameterCollection.getParameterValue("Cell Area Tag");
		outputTotalIntensityTag=(String)_stepParameterCollection.getParameterValue("Cell Integrated Intensity Tag");
		outputEresRatioTag=		(String)_stepParameterCollection.getParameterValue("ERES Fraction Tag");
		outputEresTableTag=		(String)_stepParameterCollection.getParameterValue("ERES Table Tag");
		
		eresChannelIndex=		(Integer)_stepParameterCollection.getParameterValue("Eres Channel Index");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addValueColumn(outputCellAreaTag, "NUM");
		workingProcessor.addValueColumn(outputTotalIntensityTag, "NUM");
		workingProcessor.addValueColumn(outputEresRatioTag, "NUM");
		workingProcessor.addFileColumns(outputEresTableTag, "TXT");
		
		imageOpener=new ImageOpener();
		sumProjector=new ZProjector();
		sumProjector.setMethod(ZProjector.SUM_METHOD);
		duplicator=new Duplicator();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		if (!workingTable.getBooleanValue(_rowIndex, processBooleanTag))//do not process unnecessary datasets
			return;
		
		ImagePlus	originalStackImage=imageOpener.openImage(workingTable.getFile(_rowIndex, eresImageTag, "IMG"));
		ImagePlus	isolatedStackImage=imageOpener.openImage(workingTable.getFile(_rowIndex, isolatedCellImageTag, "IMG"));
		if (originalStackImage.getNChannels()>1)
			originalStackImage=duplicator.run(originalStackImage, eresChannelIndex, eresChannelIndex, 1, originalStackImage.getNSlices(), 1, originalStackImage.getNFrames());
		if (originalStackImage.getNDimensions()!=3)
			throw new Exception("Wrong dimensions of input image");
		IJ.run(originalStackImage, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
		IJ.run(isolatedStackImage, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
		
		Roi 		cellRoi= ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, cellRoiTag, "ROI"))[0];

		File 		backgroundRoiFile=workingTable.getFile(_rowIndex, backgroundRoiTag, "ROI");
		Roi 		backgroundRoi;
		if (backgroundRoiFile!=null)
			backgroundRoi=ROIManipulator2D.flsToRois(backgroundRoiFile)[0];
		else
			backgroundRoi=null;
		
		_data.showDebugImage(originalStackImage, "Original stack with background Roi", true,backgroundRoi);
		
		IJ.run(originalStackImage, "32-bit", "");
		this.subtractBackground(originalStackImage, backgroundRoi,_data);
		_data.showDebugImage(originalStackImage, "32-bit background subtracted stack", true,backgroundRoi);
		
		sumProjector.setImage(originalStackImage);
		sumProjector.doProjection();
		ImagePlus sumProjectedImage=sumProjector.getProjection();
		_data.showDebugImage(sumProjectedImage, "Sum Projected Image", true);
		_data.showDebugImage(sumProjectedImage, "Sum Projected Image with cell Roi", true,cellRoi);
		
		sumProjectedImage.setRoi(cellRoi);
		ImageStatistics cellStatistics=sumProjectedImage.getStatistics(ImageStatistics.AREA|ImageStatistics.MEAN);
		double cellArea=cellStatistics.area;
		double cellIntegratedIntensity=cellStatistics.mean*cellArea;
		sumProjectedImage.setRoi((Roi)null);
		
		_data.showDebugMessage(String.format("Quantified cell area: %f", cellArea));
		_data.showDebugMessage(String.format("Quantified cell intensity: %f", cellIntegratedIntensity));
		workingProcessor.setValue(cellArea, _rowIndex, outputCellAreaTag, "NUM");
		workingProcessor.setValue(cellIntegratedIntensity, _rowIndex, outputTotalIntensityTag, "NUM");
		

		
		int eresNumber=(int)workingTable.getNumericValue(_rowIndex, eresNumberTag);
		_data.showDebugMessage(String.format("ERES number: %d", eresNumber));
		
		double eresFraction=Double.NaN;

		if (eresNumber>0){
			File roi3dFile=workingTable.getFile(_rowIndex, eres3DRoiTag, "ROI");
			Objects3DPopulation eresPopulation=ROIManipulator3D.fileToPopulation(roi3dFile);
			if(eresPopulation.getNbObjects()!=eresNumber)
				throw new Exception("Problem with import of 3D ERES objects");
			double eresTotalIntensity=this.getEresTotalIntensity(isolatedStackImage, eresPopulation);
			_data.showDebugMessage(String.format("ERES total intensity: %f", eresTotalIntensity));
			eresFraction=eresTotalIntensity/cellIntegratedIntensity;
			workingProcessor.saveTableToTable(_rowIndex, this.getEresDistribution(isolatedStackImage, eresPopulation, _data), outputEresTableTag, FileUtils.cutExtension(roi3dFile.getName()));
		}else{
			eresFraction=0;
		}
		
		_data.showDebugMessage(String.format("ERES fluorescence fraction: %f", eresFraction));
		workingProcessor.setValue(eresFraction, _rowIndex, outputEresRatioTag, "NUM");
		return;
	}
	
	private void subtractBackground(ImagePlus _stackImage,Roi _backgroundRoi,StepData _data){
		ImageStack stack=_stackImage.getImageStack();
		int numberOfSlices=stack.getSize();
		
		ImageProcessor processor;
		double backgroundValue;
		if (_backgroundRoi!=null){
			backgroundIntensitiesArray=new Double[numberOfSlices];
			for (int sliceIndex=1;sliceIndex<=numberOfSlices;sliceIndex++){
				processor=stack.getProcessor(sliceIndex);
				processor.setRoi(_backgroundRoi);
				backgroundValue=processor.getStatistics().mean;
				_data.showDebugMessage(String.format("Slice: %d; Background Value: %f", sliceIndex,backgroundValue));
				processor.resetRoi();
				processor.subtract(backgroundValue);
				backgroundIntensitiesArray[sliceIndex-1]=backgroundValue;
			}
		}else{
			for (int sliceIndex=1;sliceIndex<=numberOfSlices;sliceIndex++){
				processor=stack.getProcessor(sliceIndex);
				processor.resetRoi();
				backgroundValue=backgroundIntensitiesArray[sliceIndex-1];
				processor.subtract(backgroundValue);
			}
		}
	}
	
	private double getEresTotalIntensity(ImagePlus _stackFloatImage, Objects3DPopulation _eresPopulation){
		double result=0;
		
		ImageHandler imageHandler=new ImageFloat(_stackFloatImage);
		//int eresNumber=_eresPopulation.getNbObjects();
		
		for (Object3D eres3DRoi:_eresPopulation.getObjectsList()){
			//eresRoi3D=_eresPopulation.g
			result+=eres3DRoi.getIntegratedDensity(imageHandler);
		}
		
		return result;
	}
	
	private TableModel getEresDistribution(ImagePlus _stackFloatImage, Objects3DPopulation _eresPopulation, StepData _data)throws Exception{
		TableModel eresDistributionTable=new TableModel(_data.getWorkingTable().getRootPath());
		eresDistributionTable.addColumn("Eres.ID");
		eresDistributionTable.addValueColumn("Integrated.Intensity", "NUM");
		eresDistributionTable.addValueColumn("Max.Intensity", "NUM");
		eresDistributionTable.addValueColumn("Volume", "NUM");
		eresDistributionTable.addValueColumn("Area", "NUM");

		ImageHandler imageHandler=new ImageFloat(_stackFloatImage);
		int eresIndex=0;
		for (Object3D eres3DRoi:_eresPopulation.getObjectsList()){
			eresDistributionTable.addRow();

			eresDistributionTable.setStringValue(eres3DRoi.getName(), eresIndex, "Eres.ID");
			eresDistributionTable.setNumericValue(eres3DRoi.getIntegratedDensity(imageHandler), eresIndex, "Integrated.Intensity");
			eresDistributionTable.setNumericValue(eres3DRoi.getPixMaxValue(imageHandler), eresIndex, "Max.Intensity");
			eresDistributionTable.setNumericValue(eres3DRoi.getVolumePixels(), eresIndex, "Volume");
			eresDistributionTable.setNumericValue(eres3DRoi.getAreaPixels(), eresIndex, "Area");
			
			eresIndex++;
		}

		
		return eresDistributionTable;
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,100,2);
	}
}
