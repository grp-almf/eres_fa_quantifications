package automic.postaq.step.eres.time;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;

import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import fiji.plugin.trackmate.Logger;
import fiji.plugin.trackmate.Model;
import fiji.plugin.trackmate.Settings;
import fiji.plugin.trackmate.Spot;
import fiji.plugin.trackmate.SpotCollection;
import fiji.plugin.trackmate.TrackMate;
import fiji.plugin.trackmate.detection.DogDetectorFactory;
import fiji.plugin.trackmate.features.FeatureFilter;
//import fiji.plugin.trackmate.features.spot.SpotAnalyzer;
import fiji.plugin.trackmate.features.spot.SpotIntensityAnalyzerFactory;
import fiji.plugin.trackmate.tracking.LAPUtils;
import fiji.plugin.trackmate.tracking.sparselap.SparseLAPTrackerFactory;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.frame.RoiManager;


public class Step_SegmentTrackQuantifyEres implements StepFunctions {
	
	//input parameters
	String inputImageTag;
	
	//parameters currently not recordered
	double trackParticleRadius=6;
	double trackparticleQuality=0;
	double trackParticleThreshold=30;
	public int maxFrameGap=1;
	double maxEresJump=10;
	
	double qualityPenalty=1;
	
	//output parameters
	String eresSpotsRoiTag="ERES.Spots";
	String quantSpotsTableTag="Spots.Statictics";
	
	private static  RoiManager rMan;
	
	private File tableFolder;
	
	
	
	@Override
	public String getStepInformation(){
		return "Segment track quantify ERES";
	}
	
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();

		//parameters without default values
		newParameterCollection.addParameter("Input Image Tag", 			null, null, 			ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("ERES Spots Roi Tag",		null,eresSpotsRoiTag,	ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("ERES Spots Table Tag",		null,quantSpotsTableTag,ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Maximum ERES Jump",		null,maxEresJump, 		ParameterType.DOUBLE_PARAMETER);
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=		(String)_stepParameterCollection.getParameterValue("Input Image Tag");
			
		eresSpotsRoiTag=	(String)_stepParameterCollection.getParameterValue("ERES Spots Roi Tag");
		quantSpotsTableTag=	(String)_stepParameterCollection.getParameterValue("ERES Spots Table Tag");
		maxEresJump=		(double)	_stepParameterCollection.getParameterValue("Maximum ERES Jump");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor tableProcessor=_data.getWorkingProcessor();
		
		tableProcessor.addFileColumns(eresSpotsRoiTag, "ROI");
		tableProcessor.addFileColumns(quantSpotsTableTag, "TXT");

		rMan=ROIManipulator2D.getEmptyRm();
		tableFolder=new File(_data.getWorkingTable().getRootPath(),quantSpotsTableTag);
		
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		String eresTimeImageFileApth;
		
		eresTimeImageFileApth=workingProcessor.getFileAbsolutePath(_rowIndex, inputImageTag, "IMG");
		ProcessTimeLapse(eresTimeImageFileApth,workingProcessor, _rowIndex, _data);
		
		//workingProcessor.setValue(getSuccessWord(outputCode), _rowIndex, "Analysis.Result", null);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(1,10,10,2);
	}

	
	private void ProcessTimeLapse(String _in_file, TableProcessor _tableProcessor, int _rowIndex, StepData _data) throws Exception{//currently assume always one channel for simplicity
		
		rMan.reset();
		ImagePlus inputImage=null;
		try{
			inputImage=ImageOpenerWithBioformats.openImage(_in_file);
		}catch (Exception e){
			throw new Exception("No Input Image File");
		}
		
		String outputName=FileUtils.cutExtension(new File(_in_file).getName());

		if (inputImage==null)
			throw new Exception("No Input Image");

		
		IJ.run(inputImage, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
		
		
		//get the first channel if more than one
		if (inputImage.getNChannels()>1)
			throw new Exception("1 channel image is expected");
		
		IJ.run(inputImage, "Grays", "");// convert LUT to gray scale

//		int nTimePoints=inputImage.getNFrames();
		
		//quantify FRAP region
//		double [] timeValues=new double[nTimePoints];
				
		//get time tags
//		try{
//			ImageOpenerWithBioformats.getTimeTags(_in_file, timeValues);
//		}catch (Exception e)
//			throw new Exception("Unable to get time tags");
		
		//find upper left corner of the region to cut and position of the bleached ROI inside this region
		
		
		TrackMate_Eres_Tracker mateTracker=new TrackMate_Eres_Tracker();
		try{
			mateTracker.trackAndQuantEres(inputImage,_data);
		}catch (Exception ex){
			throw new Exception("Tracking Exception");
		}
		
		//_tableProcessor.saveRoisToTable(_rowIndex, mateTracker.getSpotRois(), eresSpotsRoiTag, outputName);
		
		_tableProcessor.saveTableToTable(_rowIndex, mateTracker.getSpotResultsTable(), quantSpotsTableTag, outputName);


		rMan.runCommand("Reset");		
		
		inputImage.changes=false;
		inputImage.close();

	}
	

	
	class TrackMate_Eres_Tracker{
		TableModel spotsTable;
		Model trackMateModel;
		Settings trackMateSettings;

		
		public void trackAndQuantEres(ImagePlus _originalImage,StepData _data)throws Exception{
			
			IJ.run(_originalImage, "Set Scale...", "distance=0");
			
			IJ.run(_originalImage, "32-bit", "");
			
			IJ.run(_originalImage, "Subtract...", "value=10000 stack");
			
			_data.showDebugImage(_originalImage, "Original Image", true);
			
			
			//setup model
			trackMateModel= new Model();
			trackMateModel.setLogger((_data.getDebugFlag())?Logger.IJ_LOGGER:Logger.VOID_LOGGER);//set logger
			
			//setup settings
			trackMateSettings=new Settings();
			trackMateSettings.setFrom(_originalImage);
			trackMateSettings.detectorFactory=new DogDetectorFactory<>();
			//put settings
			trackMateSettings.detectorSettings.put("DO_SUBPIXEL_LOCALIZATION", true);
			trackMateSettings.detectorSettings.put("RADIUS", trackParticleRadius);
			trackMateSettings.detectorSettings.put("TARGET_CHANNEL", 1);
			trackMateSettings.detectorSettings.put("THRESHOLD", trackParticleThreshold);
			trackMateSettings.detectorSettings.put("DO_MEDIAN_FILTERING", false);
			
			trackMateSettings.initialSpotFilterValue=trackparticleQuality;
			
			//filter on quality - probably redundant
			FeatureFilter spotFeatureFilter1 = new FeatureFilter("QUALITY", trackparticleQuality, true);
			trackMateSettings.addSpotFilter(spotFeatureFilter1);
			
			//configure tracker
			trackMateSettings.trackerFactory=new SparseLAPTrackerFactory();//new LAPTrackerFactory();
			trackMateSettings.trackerSettings=LAPUtils.getDefaultLAPSettingsMap();
			trackMateSettings.trackerSettings.put("MAX_FRAME_GAP", maxFrameGap);
			
			Map<String, Double> trackPenalties=new HashMap<String,Double>();
			trackPenalties.put("QUALITY", qualityPenalty);
			trackMateSettings.trackerSettings.put("LINKING_FEATURE_PENALTIES", trackPenalties);
			trackMateSettings.trackerSettings.put("GAP_CLOSING_FEATURE_PENALTIES", trackPenalties);
			trackMateSettings.trackerSettings.put("ALLOW_TRACK_SPLITTING",true);
			trackMateSettings.trackerSettings.put("ALLOW_TRACK_MERGING",true);
			
			if(_data.getDebugFlag())
				System.out.println(trackMateSettings);
			
			trackMateSettings.addSpotAnalyzerFactory(new SpotIntensityAnalyzerFactory<>());
			
			//analyse spots
			//SpotIntensityAnalyzer<RealType<T>>
			
//			//analyse tracks
//			TrackDurationAnalyzer trackDurationAnalyzer=new TrackDurationAnalyzer();
////			if(Debug){
////				System.out.println(TrackDurationAnalyzer.FEATURE_NAMES);
////				System.out.println(TrackDurationAnalyzer.FEATURES);
////			}
//			trackMateSettings.addTrackAnalyzer(trackDurationAnalyzer);
//			trackMateSettings.addTrackAnalyzer(new TrackLocationAnalyzer());
//			trackMateSettings.addTrackAnalyzer(new TrackBranchingAnalyzer());
//			trackMateSettings.addTrackAnalyzer(new TrackSpotQualityFeatureAnalyzer());
//			
//			
//			FeatureFilter trackSpotNumberFilter=new FeatureFilter("NUMBER_SPOTS", minimalTrackDuration, true);
//			trackMateSettings.addTrackFilter(trackSpotNumberFilter);
//			
//			
			//initialise trackMate and process
			TrackMate trackMate=new TrackMate(trackMateModel,trackMateSettings);
			if(!trackMate.checkInput())
				throw new Exception("TrackMate Input is incompatible");
			if(!trackMate.process())
				throw new Exception("TrackMate processing error");
			
			
			generateSpotsTable(trackMateModel);
			
//			debugTrackOutput(trackMateModel,_data);

//			finalDebugViews(_data);
			
		}
		
		public TableModel getSpotResultsTable(){
			return spotsTable;
		}
		
		public Roi[] getSpotRois(){
			return null;
		}
		
		private void generateSpotsTable(Model _trackMateModel)throws Exception{
			TableModel resultsTable=new TableModel(tableFolder.getAbsolutePath());
			resultsTable.addValueColumn("Time", "NUM");
			resultsTable.addValueColumn("Frame", "NUM");
			resultsTable.addValueColumn("Total.Intensity", "NUM");
			
			// Get spot collection (all spots)
			SpotCollection spotCollection = _trackMateModel.getSpots();
			for (Spot spot : spotCollection.iterable(false)) {
				int tableRowIndex=resultsTable.getRowCount();
				resultsTable.addRow();
				resultsTable.setNumericValue(spot.getFeature(SpotIntensityAnalyzerFactory.TOTAL_INTENSITY), tableRowIndex, "Total.Intensity");
				resultsTable.setNumericValue(spot.getFeature(Spot.FRAME), tableRowIndex, "Frame");
//				System.out.println(spot.getFeatures());
//				spotIDlist.add(spot.ID());
//				xList.add(spot.getDoublePosition(0));
//				yList.add(spot.getDoublePosition(1));
//				zList.add(spot.getDoublePosition(2));
//				qList.add(spot.getFeature(Spot.QUALITY));
			}
			spotsTable=resultsTable;
		}
	}

}
