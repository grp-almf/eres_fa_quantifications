package automic.postaq.step.jj;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.TIFProcessorImageJ;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.segment.ParticleSegmentor;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ImageStatistics;

public class Step_Segment_Count_Quantify_FAs implements StepFunctions {
	//input parameters
	
	String segmentedStructuresTag;
	String inputImageTag;
	
	String countTag;
	String totalAreaTag;
	String totalIntensityTag;
	
	//non-passed parameters
	double structureMinSize=10;
	double structureMinPeakIntensity=700;
	double structureMinIntegratedIntensity=5000;
	double filterSize=0;
	double threshold=30;
	
	
	@Override
	public String getStepInformation(){
		return "Segment nuclei";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Segmented Structures Roi Tag", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Input Image Tag", 			null, null, ParameterType.STRING_PARAMETER);

		newParameterCollection.addParameter("Structure Count Tag", 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Structure Total Area Tag",	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Structure Total Intensity Tag",	null, null, ParameterType.STRING_PARAMETER);

		
		//parameters with default values
		
		newParameterCollection.addParameter("Minimal Structure Size", 	null, structureMinSize, 	ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Minimal Structure Peak Intensity", 	null, structureMinPeakIntensity, 	ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Minimal Structure Integrated Intensity", 	null, structureMinIntegratedIntensity, 	ParameterType.DOUBLE_PARAMETER);

		
		newParameterCollection.addParameter("Filter Size",				null, filterSize,ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Threshold",				null, threshold,	ParameterType.DOUBLE_PARAMETER);
		
		
		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		segmentedStructuresTag=	(String)_stepParameterCollection.getParameterValue("Segmented Structures Roi Tag");
		inputImageTag=			(String)_stepParameterCollection.getParameterValue("Input Image Tag");

		countTag=				(String)_stepParameterCollection.getParameterValue("Structure Count Tag");
		totalAreaTag=			(String)_stepParameterCollection.getParameterValue("Structure Total Area Tag");
		totalIntensityTag=		(String)_stepParameterCollection.getParameterValue("Structure Total Intensity Tag");
		
		structureMinSize=		(Double)_stepParameterCollection.getParameterValue("Minimal Structure Size");
		structureMinPeakIntensity=		(Double)_stepParameterCollection.getParameterValue("Minimal Structure Peak Intensity");
		structureMinIntegratedIntensity=(Double)_stepParameterCollection.getParameterValue("Minimal Structure Integrated Intensity");

		filterSize=				(Double)_stepParameterCollection.getParameterValue("Filter Size");
		threshold=				(Double)_stepParameterCollection.getParameterValue("Threshold");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(segmentedStructuresTag, "ROI");
		workingProcessor.addValueColumn(countTag, "NUM");
		workingProcessor.addValueColumn(totalAreaTag, "NUM");
		workingProcessor.addValueColumn(totalIntensityTag, "NUM");
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		ImagePlus inputImage=TIFProcessorImageJ.openTIFImage(workingTable.getFile(_rowIndex, inputImageTag, "IMG"));
		//dapiImage.show();
		
		Roi[] structureRois=segmentStructures(inputImage,_data);
		
		if (ROIManipulator2D.isEmptyRoiArr(structureRois)){
			workingTable.setNumericValue(0, _rowIndex, countTag);
			workingTable.setNumericValue(0, _rowIndex, totalAreaTag);
			workingTable.setNumericValue(0, _rowIndex, totalIntensityTag);
		}else{
			Roi combinedRoi=ROIManipulator2D.RoiUnion(structureRois);
			inputImage.setRoi(combinedRoi);
			ImageStatistics structureStat=inputImage.getStatistics(ImageStatistics.AREA|ImageStatistics.MEAN);
			workingTable.setNumericValue(structureRois.length, _rowIndex, countTag);
			workingTable.setNumericValue(structureStat.area, _rowIndex, totalAreaTag);
			workingTable.setNumericValue(structureStat.mean*structureStat.area, _rowIndex, totalIntensityTag);

			workingProcessor.saveRoisToTable(_rowIndex, structureRois, segmentedStructuresTag, String.format("Dataset_%05d", _rowIndex+1));
		}
		return;
	}
	
	private Roi[] segmentStructures(ImagePlus _rawImage,StepData _data)throws Exception{
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		_data.showDebugMessage("filterSize="+filterSize);
		_data.showDebugMessage("minSize="+structureMinSize);
		_data.showDebugMessage("threshold="+threshold);
		
		_data.showDebugImage(_rawImage, "Original image", true);
		//IJ.run(_rawDapiMaxImage, "Subtract...", "value="+back_DAPIMax);
		IJ.run(_rawImage, "Median...", "radius="+filterSize);
		_data.showDebugImage(_rawImage, "Filtered image", true);
		
		ParticleSegmentor segmentor=new ParticleSegmentor((int)structureMinSize);
		/*ImagePlus nucMaskImg=*/segmentor.SegSliceThreshold(_rawImage, ParticleAnalyzer.SHOW_MASKS, true, threshold);
		
		
		
		Roi[] structRois=rMan.getRoisAsArray();
		
		if (ROIManipulator2D.isEmptyRoiArr(structRois))
			return structRois;

		_data.showDebugMessage("N ROIs before filtering="+structRois.length);

		
		ParticleFilterer pFilterer=new ParticleFilterer(_rawImage.getProcessor(), structRois);
		pFilterer.filterThr(ParticleFilterer.MAX, structureMinPeakIntensity, Double.MAX_VALUE);
		pFilterer.filterThr(ParticleFilterer.INTEGRATED, structureMinIntegratedIntensity, Double.MAX_VALUE);
		
		structRois=pFilterer.getPassedRois();
		int nFilteredRois=(structRois==null)?0:structRois.length;
		_data.showDebugMessage("N ROIs after filtering="+nFilteredRois);
		
		_data.showDebugImage(_rawImage, "Filtered image with overlay", true, structRois);
		
		
		return structRois;
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-1,10,10,2);
	}
}
