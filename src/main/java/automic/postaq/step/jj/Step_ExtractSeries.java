package automic.postaq.step.jj;

import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.ImagePlus;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

public class Step_ExtractSeries implements StepFunctions {
	//default parameters values
	private final String DEFAULT_RAW_IMAGE_TAG=	"Raw.Data";
	private final String DEFAULT_SERIES_NUMBER_TAG="Series";
	private final String DEFAULT_EXTRACTED_SERIES_TAG="Extracted.Series";
	private final String DEFAULT_TREATMENT_TAG="Treatment";
	
	//input parameters
	
	String rawImageTag;
	String seriesNumberTag;
	String extractedSeriesTag;
	String treatmentTag;
	
	TableModel newTable;
	TableProcessor newTableProcessor;
	
	@Override
	public String getStepInformation(){
		return "Extract Series";
	}
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		//parameters without default values
		
		//parameters with default values
		newParameterCollection.addParameter("Raw File Tag", 		null, DEFAULT_RAW_IMAGE_TAG, 		ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Series Number Tag",	null, DEFAULT_SERIES_NUMBER_TAG, 	ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Extracted Series Tag", null, DEFAULT_EXTRACTED_SERIES_TAG, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Treatment Tag", 		null, DEFAULT_TREATMENT_TAG, 		ParameterType.STRING_PARAMETER);

		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		
		rawImageTag=		(String)_stepParameterCollection.getParameterValue("Raw File Tag");
		seriesNumberTag=	(String)_stepParameterCollection.getParameterValue("Series Number Tag");
		extractedSeriesTag=	(String)_stepParameterCollection.getParameterValue("Extracted Series Tag");
		treatmentTag=		(String)_stepParameterCollection.getParameterValue("Treatment Tag");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableModel oldTable=_data.getWorkingTable();
		
		oldTable.setRootPathDeep(_data.getAnalysisPath());
		
		newTable=new TableModel(oldTable.getRootPath());
	
		newTableProcessor=new TableProcessor(newTable);
		
		newTable.addFileColumns(rawImageTag, "IMG");
		newTableProcessor.addFileColumns(extractedSeriesTag, "IMG");
		newTableProcessor.addValueColumn(seriesNumberTag, "NUM");
		newTableProcessor.addValueColumn(treatmentTag, "FACT");
		newTableProcessor.addValueColumn("Success", "BOOL");
		
		
	}

	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		TableModel workingTable=_data.getWorkingTable();
		
		
		File inputFile=workingProcessor.getFile(_rowIndex, rawImageTag, "IMG");
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COLORIZED;
		ImagePlus[] allSeries=ImageOpenerWithBioformats.openAllSeries(inputFile);
		
		int nSeries=allSeries.length;
		
		for (int iSeries=0; iSeries<nSeries; iSeries++) {
			int newTableIndex=newTable.getRowCount();
			newTable.addRow();
			newTable.setFileAbsolutePath(inputFile, newTableIndex, rawImageTag, "IMG");
			newTable.setFactorValue(workingTable.getFactorValue(_rowIndex, treatmentTag), newTableIndex, treatmentTag);
			newTable.setNumericValue(iSeries+1, newTableIndex, seriesNumberTag);
			newTable.setBooleanValue(true, newTableIndex, "Success");
			
			final String outputFileName=String.format("%s--%02d", FileUtils.cutExtension(inputFile.getName()),iSeries+1);
			
			ImagePlus targetSeries=allSeries[iSeries];
			newTableProcessor.saveImageToTable(newTableIndex, targetSeries, extractedSeriesTag, outputFileName);
			
		}
		
		//ImagePlus targetSeries=allSeries[(int)workingTable.getNumericValue(_rowIndex, seriesNumberTag)-1];
		
		//workingProcessor.saveImageToTable(_rowIndex, targetSeries, extractedSeriesTag, outputFileName);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{
		_data.setWorkingTable(newTable);
	}

	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return null;
	}
}
