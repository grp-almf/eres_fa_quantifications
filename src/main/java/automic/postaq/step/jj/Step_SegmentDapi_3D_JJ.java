package automic.postaq.step.jj;

import java.awt.Color;
import java.awt.Rectangle;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.Binner;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.MaximumFinder;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageFloat;
//import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.utils.ThreadUtil;
//import process.segment.ParticleSegmentor;

import fiji.threshold.Auto_Local_Threshold;

public class Step_SegmentDapi_3D_JJ implements StepFunctions {
	//input parameters
	
	String segmentedNucleiTag;
	String dapiImageTag;
	
	//non-passed parameters
	String nucleiCountTag="Nuclei.Count";
	
	double minimalNucleusSize=3000;
	//double nucFilterSize=10;
	double nucThresh=0;
	
	// local-only parameters
	int dapiChannelIndex=1;
	
	//int opening_radius_xy=10;
	//int opening_radius_z=1;
	int median_radius_xy=3;
	int median_radius_z=1;
	
	int radiusNiblack=40;
	double minMaximumProjectedImage=20;

	double minimalNucleusCircularity=0.15;
	
	int binningFactor=5;
	
	//double minimalSolidity=0.5;
	double minimalIntegratedInensity=3e3;
	double minimalIntensityRange=10;
	double watershedNoiseTolerance=1.0;
	//String nucleiMaskImageTag;
	
	//?variables referenced by different functions
	//ImagePlus inputImage=null;
	//Roi [] nucRois=null;
	
	Duplicator duplicator;
	ImageOpener imageOpener;
	ZProjector meanProjector;
	Auto_Local_Threshold localThresholder;
	MaximumFinder maximumFinder;
	
	@Override
	public String getStepInformation(){
		return "Segment nuclei 3D";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Segmented Nuclei Roi Tag", null, null, 		ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("DAPI Image Tag", 			null, null, 		ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		newParameterCollection.addParameter("Nuclei Count Tag", 		null, nucleiCountTag, 	ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Dapi channel index", 		null, dapiChannelIndex, 	ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Minimal nucleus size", 	null, minimalNucleusSize, 	ParameterType.DOUBLE_PARAMETER);
		//newParameterCollection.addParameter("Filter Size Nucleus",		null, nucFilterSize,ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Nucleus Threshold",		null, nucThresh,			ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Radius Niblack",				null, radiusNiblack,			ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Watershed Noise Tolerance",				null, watershedNoiseTolerance,			ParameterType.DOUBLE_PARAMETER);

		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		segmentedNucleiTag=	(String)_stepParameterCollection.getParameterValue("Segmented Nuclei Roi Tag");
		dapiImageTag=		(String)_stepParameterCollection.getParameterValue("DAPI Image Tag");
			
		nucleiCountTag=		(String)_stepParameterCollection.getParameterValue("Nuclei Count Tag");
		dapiChannelIndex=	(Integer)_stepParameterCollection.getParameterValue("Dapi channel index");
		minimalNucleusSize=	(Double)_stepParameterCollection.getParameterValue("Minimal nucleus size");
		//nucFilterSize=	(Double)_stepParameterCollection.getParameterValue("Filter Size Nucleus");
		nucThresh=			(Double)_stepParameterCollection.getParameterValue("Nucleus Threshold");
		radiusNiblack=			(Integer)_stepParameterCollection.getParameterValue("Radius Niblack");
		watershedNoiseTolerance=(Double)_stepParameterCollection.getParameterValue("Watershed Noise Tolerance");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		duplicator=new Duplicator();
		imageOpener=new ImageOpener();
		meanProjector=new ZProjector();
		meanProjector.setMethod(ZProjector.AVG_METHOD);
		
		localThresholder=new Auto_Local_Threshold();
		maximumFinder=new MaximumFinder();
		
		
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		//workingProcessor.addFileColumns(nucleiMaskImageTag, "IMG");
		workingProcessor.addFileColumns(segmentedNucleiTag, "ROI");
		workingProcessor.addValueColumn(nucleiCountTag, "NUM");
		
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		ImagePlus inputImage=imageOpener.openImage(workingTable.getFile(_rowIndex, dapiImageTag, "IMG"));
		int nSlices=inputImage.getNSlices();
		if(inputImage.getNDimensions()>3)
			inputImage=duplicator.run(inputImage, dapiChannelIndex, dapiChannelIndex, 1, nSlices, 1, 1);
		
		//dapiImage.show();
		
		Roi[] nucRois=segmentNuclei3D(inputImage,_data);
		
		if (ROIManipulator2D.isEmptyRoiArr(nucRois)){
			workingTable.setBooleanValue(false, _rowIndex, "Success");
		}else{
			workingTable.setBooleanValue(true, _rowIndex, "Success");
			for (Roi r:nucRois)
				r.setStrokeColor(Color.CYAN);
			workingProcessor.saveRoisToTable(_rowIndex, nucRois, segmentedNucleiTag, String.format("Dataset_%05d", _rowIndex+1));
		}
		//count the number of nuclei
		Double nucleiCount=0.0;
		int imageWidth=inputImage.getWidth();
		int imageHeight=inputImage.getHeight();
		if (!ROIManipulator2D.isEmptyRoiArr(nucRois)) {
			for (Roi r:nucRois) {
				Rectangle bounds=r.getBounds();
				if((bounds.x==0)||(bounds.y==0)||(bounds.x+bounds.width==imageWidth)||(bounds.y+bounds.height==imageHeight)) {
					nucleiCount+=0.5;
				}else {
					nucleiCount+=1;
				}
			}
		}
		
		workingProcessor.setValue(nucleiCount, _rowIndex, nucleiCountTag, "NUM");
		
		return;
	}
	
	private Roi[] segmentNuclei3D(ImagePlus _dapiStackImage,StepData _data)throws Exception{

		_data.showDebugImage(_dapiStackImage, "Original image", true);
		
		int ncpu=ThreadUtil.getNbCpus();
		
		IJ.run(_dapiStackImage, "32-bit", "");
		ImageFloat HandOrig=new ImageFloat(_dapiStackImage);//new ImageShort(_dapiStackImage).convertToFloat(true);
		
		ImageFloat HandMedian=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.MEDIAN, median_radius_xy,median_radius_xy, median_radius_z, ncpu,false);
		ImagePlus medianImage=HandMedian.getImagePlus();

		_data.showDebugImage(medianImage, "Median result", true);
		
		
		//ImageFloat HandOpened=FastFilters3D.filterFloatImage(HandMedian, FastFilters3D.OPENGRAY, opening_radius_xy,opening_radius_xy, opening_radius_z, ncpu,false);
		//ImagePlus openedImage=HandOpened.getImagePlus();

		//_data.showDebugImage(openedImage, "Opening result", true, null);

		//ImageFloat HandClosed=FastFilters3D.filterFloatImage(HandOpened, FastFilters3D.CLOSEGRAY, opening_radius_xy,opening_radius_xy, opening_radius_z, ncpu,false);
		//ImagePlus closedImage=HandClosed.getImagePlus();
	
		
		//_data.showDebugImage(closedImage, "Closing result", true, null);
		

		
		meanProjector.setImage(medianImage);
		meanProjector.doProjection();
		
		ImagePlus projectedImage=meanProjector.getProjection();
		
		_data.showDebugImage(projectedImage, "Projecting result", true);
		
		ImagePlus maskImage=projectedImage.duplicate();
		
		//avoid segmentation of empty images. Set
		ImageProcessor maskProcessor=maskImage.getProcessor();
		double maxValue=maskProcessor.getMax();
		_data.showDebugMessage("maxValue of projected image is "+maxValue);
		_data.showDebugMessage("nucleus threshold is "+nucThresh);
		if(maxValue<minMaximumProjectedImage)
			maskProcessor.setMinAndMax(nucThresh,minMaximumProjectedImage);
		else
			maskProcessor.setMinAndMax(nucThresh,maxValue);
		
		IJ.run(maskImage, "8-bit", "");

		
		_data.showDebugImage(maskImage, "Projecting result 8-bit", true);

		int originalWidth=maskImage.getWidth();
	    int originalHeight=maskImage.getHeight();

	    ImagePlus maskImageBinned=new Binner().shrink(maskImage,binningFactor,binningFactor,1,Binner.MEDIAN);
	    
		IJ.run("Options...", "iterations=1 count=1 black pad edm=Overwrite do=Nothing");//do not expand borders when eroding
		//IJ.run(maskImage, "Auto Local Threshold", String.format("method=Otsu radius=%d parameter_1=0 parameter_2=0 white",radiusOtsu));
		
		_data.showDebugMessage("Niblack radius "+radiusNiblack);
		localThresholder.exec(maskImageBinned, "Niblack", radiusNiblack/binningFactor, 0, 0, true);
		
		maskImage=new ImagePlus("Local Niblack mask",maskImageBinned.getProcessor().resize(originalWidth, originalHeight));
		
		_data.showDebugImage(maskImage, "Local Niblack mask", true);
		
		//IJ.run("Options...", "iterations=1 count=1 black pad edm=Overwrite do=Nothing");//do not expand borders when eroding
		
		IJ.run(maskImage, "Dilate", "");
		IJ.run(maskImage, "Dilate", "");
		IJ.run(maskImage, "Dilate", "");
		
		
		IJ.run(maskImage, "Erode", "");
		IJ.run(maskImage, "Erode", "");
		IJ.run(maskImage, "Erode", "");
		
		_data.showDebugImage(maskImage, "Closed mask", true);
		
		//IJ.run("Options...", "iterations=1 count=1 black pad edm=Overwrite do=Nothing");
		IJ.run(maskImage, "Fill Holes", "");
		_data.showDebugImage(maskImage, "Holes filled", true);

		IJ.run(maskImage, "Distance Map", "");
		_data.showDebugImage(maskImage, "Distance map", true);

		IJ.setThreshold(maskImage, 1, 255);
		maskProcessor=maskImage.getProcessor();
		ImageProcessor newMaskProcessor=maximumFinder.findMaxima(maskProcessor, watershedNoiseTolerance, 3, MaximumFinder.SEGMENTED, false, false);
		ImagePlus newMaskImage=new ImagePlus("WaterShedMaskImage", newMaskProcessor);

		_data.showDebugImage(newMaskImage, "watershedMaskImage", true);
		
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		
		int part_opt=ParticleAnalyzer.SHOW_NONE|ParticleAnalyzer.INCLUDE_HOLES|ParticleAnalyzer.ADD_TO_MANAGER/*|ParticleAnalyzer.FOUR_CONNECTED*/;
		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,minimalNucleusSize,Double.POSITIVE_INFINITY,minimalNucleusCircularity,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(newMaskImage,newMaskProcessor);
		
		Roi[] nucRois=rMan.getRoisAsArray();
		ROIManipulator2D.removeSliceInfo(nucRois);

		_data.showDebugImage(projectedImage, "Projected with overlay", true, nucRois);
		
		//subtract minimal value from projected image
		projectedImage.setRoi((Roi)null);
		double backValue=projectedImage.getStatistics().min;
		IJ.run(projectedImage, "Subtract...", "value="+backValue);
		
		
		//filter Rois based on range of values and integrated intensity
		ParticleFilterer particleFilterer=new ParticleFilterer(projectedImage.getProcessor(),nucRois);
		particleFilterer.filterThr(ParticleFilterer.INTEGRATED, minimalIntegratedInensity, 	Double.POSITIVE_INFINITY);
		particleFilterer.filterThr(ParticleFilterer.RANGE, 		minimalIntensityRange,		Double.POSITIVE_INFINITY);		
		
		nucRois=particleFilterer.getPassedRois();
		
		_data.showDebugImage(projectedImage, "Projected with filtered ROIs overlay", true,nucRois);
		
		return nucRois;
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(0,10,10,2);
	}
}
