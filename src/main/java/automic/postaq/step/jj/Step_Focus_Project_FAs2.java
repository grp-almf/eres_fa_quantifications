package automic.postaq.step.jj;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

public class Step_Focus_Project_FAs2  implements StepFunctions{
	
	
	//input parameters
	String inputImageTag;
	String outputImageTag;
	String bestSliceIndexTag;
	int channelIndex;
	int projectionHalfHeight;
	double filterRadius;
	
	Duplicator duplicator=null;
	ZProjector maxProjector=null;
	
	@Override
	public String getStepInformation(){
		return "Get slice from (hyper-)stack";
	}
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		newParameterCollection.addParameter("Input Image Tag", 	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Output Image Tag", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Best Slice Index Tag", null, null, ParameterType.STRING_PARAMETER);
		
		newParameterCollection.addParameter("Channel Index", 	null, null, ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Projection Half Height",	null, null, ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Filter radius in pixels",	null, null, ParameterType.DOUBLE_PARAMETER);
		
		return newParameterCollection;
	}

	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=	(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		outputImageTag=	(String)_stepParameterCollection.getParameterValue("Output Image Tag");
		bestSliceIndexTag=	(String)_stepParameterCollection.getParameterValue("Best Slice Index Tag");
		channelIndex=	(int)	_stepParameterCollection.getParameterValue("Channel Index");
		projectionHalfHeight=	(int)	_stepParameterCollection.getParameterValue("Projection Half Height");
		filterRadius= (double ) _stepParameterCollection.getParameterValue("Filter radius in pixels");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		duplicator=new Duplicator();
		maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);

		TableProcessor tableProcessor=_data.getWorkingProcessor();
		tableProcessor.addFileColumns(outputImageTag, "IMG");
		tableProcessor.addValueColumn(bestSliceIndexTag, "NUM");

	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		ImagePlus inputImage,channelImage,outputImage;
		
		final String fileName=String.format("Dataset_%05d", _rowIndex+1);
		
		inputImage=ImageOpenerWithBioformats.openImage(workingProcessor.getFile(_rowIndex, inputImageTag, "IMG"));
		
		int nSlicesInput=inputImage.getNSlices();

		channelImage=duplicator.run(inputImage, channelIndex, channelIndex, 1, nSlicesInput, 1, 1);
		IJ.run(channelImage, "32-bit", "");
		
		//channelImage.show();
		ImagePlus varianceImage=channelImage.duplicate();
		IJ.run(varianceImage, "Variance...", String.format("radius=%f stack", filterRadius));
		
		
		Integer bestSliceIndex=0;
		double bestNormalisedVariance=0;
		
		ImageStack channelStack=channelImage.getStack();
		ImageStack varianceStack=varianceImage.getStack();
		
		for (int sliceIndex=1;sliceIndex<=nSlicesInput; sliceIndex++) {
			ImageProcessor channelSliceProcessor=channelStack.getProcessor(sliceIndex);
			ImageProcessor varianceSliceProcessor=varianceStack.getProcessor(sliceIndex);
			//ImageStatistics sliceStatistics=sliceProcessor.getStats();
			double normVar=varianceSliceProcessor.getStats().mean/channelSliceProcessor.getStats().mean;
			if (normVar>bestNormalisedVariance) {
				bestNormalisedVariance=normVar;
				bestSliceIndex=sliceIndex;
			}
		}
		
		workingProcessor.setValue(bestSliceIndex, _rowIndex, bestSliceIndexTag, "NUM");
		
		maxProjector.setImage(channelImage);
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		maxProjector.setStartSlice(bestSliceIndex-projectionHalfHeight);
		maxProjector.setStopSlice(bestSliceIndex+projectionHalfHeight);
		maxProjector.doProjection();
		outputImage=maxProjector.getProjection();
		
		
		workingProcessor.saveImageToTable(_rowIndex, outputImage, outputImageTag, fileName);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return null;
	}
}
