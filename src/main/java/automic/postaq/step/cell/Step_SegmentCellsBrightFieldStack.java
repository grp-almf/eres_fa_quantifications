package automic.postaq.step.cell;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
//import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
//import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.RoiEnlarger;
import ij.plugin.ZProjector;
import ij.plugin.filter.ThresholdToSelection;
//import ij.process.AutoThresholder;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.image3d.regionGrowing.Watershed3D;
import mcib3d.utils.ThreadUtil;

public class Step_SegmentCellsBrightFieldStack implements StepFunctions {
	//input parameters
	
	String nucleiRoiTag;
	String brightFieldImageTag;
	String cellsRoiTag;
	String backgroundRoiTag;
	int brightFieldChannelIndex;
	
	//?variables referenced by different functions
	Duplicator duplicator;
	ImageOpener imageOpener;
	ZProjector averageProjector;
	ThresholdToSelection thresholdToSelection;
	
	int nucleiMaskBorderWidth=5;
	int variance_Radius_xy=3;
	int variance_Radius_z=1;
	double brightfieldThreshold=150;
	int backgroundDilationPixels=3;
	int cellClosingRadius=10;
	
	//double varianceImageCutOffIntensity=20000;

	
	@Override
	public String getStepInformation(){
		return "Segment cells brightField";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Pre-Segmented Nuclei Roi Tag",	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Cells Roi Tag",		 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Background Roi Tag",	 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Bright Field Image Tag", 		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Bright Field Channel Index",	null, null, ParameterType.INT_PARAMETER);
		
		//parameters with default values
		newParameterCollection.addParameter("Brightfield Threshold", 	null, brightfieldThreshold, 	ParameterType.DOUBLE_PARAMETER);

		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		nucleiRoiTag=			(String)_stepParameterCollection.getParameterValue("Pre-Segmented Nuclei Roi Tag");
		cellsRoiTag=			(String)_stepParameterCollection.getParameterValue("Cells Roi Tag");
		backgroundRoiTag=		(String)_stepParameterCollection.getParameterValue("Background Roi Tag");
		brightFieldImageTag=	(String)_stepParameterCollection.getParameterValue("Bright Field Image Tag");
		brightFieldChannelIndex=(int)	_stepParameterCollection.getParameterValue("Bright Field Channel Index");
		
		brightfieldThreshold=(Double)	_stepParameterCollection.getParameterValue("Brightfield Threshold");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(cellsRoiTag, "ROI");
		workingProcessor.addFileColumns(backgroundRoiTag, "ROI");
		
		duplicator=new Duplicator();
		imageOpener=new ImageOpener();
		averageProjector=new ZProjector();
		averageProjector.setMethod(ZProjector.AVG_METHOD);
		thresholdToSelection=new ThresholdToSelection();
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		String outputFileName=String.format("Dataset_%05d", _rowIndex+1);
		
		if(!workingTable.getBooleanValue(_rowIndex, "Success")) return;
		
		ImagePlus brightImageStack=imageOpener.openImage(workingTable.getFile(_rowIndex, brightFieldImageTag, "IMG"));
		
		if(brightImageStack.getNDimensions()>3)
			brightImageStack=duplicator.run(brightImageStack, brightFieldChannelIndex, brightFieldChannelIndex, 1, brightImageStack.getNSlices(), 1, 1);
			
		IJ.run(brightImageStack, "Grays", "");
		_data.showDebugImage(brightImageStack, "Original image in gray", true);

		//preparing seed image
		Roi[] nucRois=ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, nucleiRoiTag,"ROI"));
		_data.showDebugImage(brightImageStack, "Original image in gray with nucRois", true, nucRois);
		
		ImagePlus nucleiMask=createNucleiMask(brightImageStack.getWidth(), brightImageStack.getHeight(), nucRois);
		_data.showDebugImage(nucleiMask, "Nuclei Masks", true);
		
		drawWhiteRectangle(nucleiMask, nucleiMaskBorderWidth);
		_data.showDebugImage(nucleiMask, "Nuclei Masks with rectangle", true);
		
		//preparing image for watershed
		int ncpu=ThreadUtil.getNbCpus();
		
		ImageFloat HandOrig=new ImageShort(brightImageStack).convertToFloat(true);
		
		ImageFloat HandVariance=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.VARIANCE, variance_Radius_xy,variance_Radius_xy, variance_Radius_z, ncpu,false);
		ImagePlus varianceImage=HandVariance.getImagePlus();
		IJ.run(varianceImage, "Square Root"/*"Log"*/, "stack");

		_data.showDebugImage(varianceImage, "Standard deviation 3d filter result", true);
		
		averageProjector.setImage(varianceImage);
		averageProjector.doProjection();

		ImagePlus averageProjectedSdImage=averageProjector.getProjection();
		_data.showDebugImage(averageProjectedSdImage, "Average Projected SD image", true);

		ImageProcessor projectedProcessor=averageProjectedSdImage.getProcessor();
		projectedProcessor.setThreshold(0, brightfieldThreshold, projectedProcessor.getLutUpdateMode());
		//projectedProcessor.setAutoThreshold(AutoThresholder.Method.MinError/*.Triangle*//*.MinError*/, false);
		//looks like optimal threshold will be minimum between two peaks on distribution 
		
		double backgroundThreshold=projectedProcessor.getMaxThreshold();
		_data.showDebugMessage("Threshold level is "+backgroundThreshold);
		//IJ.setAutoThreshold(averageProjectedSdImage, "Triangle");
		
		Roi backgroundRoi;
		if (projectedProcessor.getStatistics().min<backgroundThreshold)
			backgroundRoi=thresholdToSelection.convert(projectedProcessor);
		else
			backgroundRoi=null;
		_data.showDebugImage(averageProjectedSdImage, "Background Roi", true, backgroundRoi);
		Roi erodedBackGroundRoi;
		try{
			erodedBackGroundRoi=RoiEnlarger.enlarge(backgroundRoi, -backgroundDilationPixels);
		}catch(Exception ex){//this exception appears if there is no background on the image
			erodedBackGroundRoi=null;
		}
		if(erodedBackGroundRoi!=null){
			erodedBackGroundRoi.setStrokeColor(Color.BLUE);
			workingProcessor.saveRoiToTable(_rowIndex, erodedBackGroundRoi, backgroundRoiTag, outputFileName);
		}
		_data.showDebugImage(averageProjectedSdImage, "Background Roi", true, backgroundRoi);
		_data.showDebugImage(averageProjectedSdImage, "Eroded Background Roi", true, erodedBackGroundRoi);
		
		
		//run watershed
		
		Watershed3D watershed3D=new Watershed3D(averageProjectedSdImage.getImageStack(), nucleiMask.getImageStack(), (int)backgroundThreshold, 125);
		ImagePlus cellMask16=watershed3D.getWatershedImage3D().getImagePlus();
		
		_data.showDebugImage(cellMask16, "Cell Masks", true);
		
		Roi[] cellRois=this.getCellRoisFrom16BitMask(cellMask16);
		int nCells=(cellRois==null)?0:cellRois.length;
		
		if(nCells>0){
			for (int iCell=0; iCell<nCells; iCell++){
				cellRois[iCell]=RoiEnlarger.enlarge(cellRois[iCell], cellClosingRadius);
				cellRois[iCell]=RoiEnlarger.enlarge(cellRois[iCell], -cellClosingRadius);
				cellRois[iCell].setStrokeColor(Color.GREEN);;
			}
			workingProcessor.saveRoisToTable(_rowIndex, cellRois, cellsRoiTag, outputFileName);
			workingTable.setBooleanValue(true, _rowIndex, "Success");
		}else
			workingTable.setBooleanValue(false, _rowIndex, "Success");

		_data.showDebugMessage("Number of found cells is"+nCells);
		_data.showDebugImage(brightImageStack, "Original Stack Overlayed with cell masks", true, cellRois);
		
		return;
	}
	
//	@Override
//	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
//		TableModel workingTable=_data.getWorkingTable();
//		TableProcessor workingProcessor=_data.getWorkingProcessor();
//		
//		String outputFileName=String.format("Dataset_%05d", _rowIndex+1);
//		
//		if(!workingTable.getBooleanValue(_rowIndex, "Success")) return;
//		
//		ImagePlus brightImageStack=imageOpener.openImage(workingTable.getFile(_rowIndex, brightFieldImageTag, "IMG"));
//		
//		if(brightImageStack.getNDimensions()>3)
//			brightImageStack=duplicator.run(brightImageStack, brightFieldChannelIndex, brightFieldChannelIndex, 1, brightImageStack.getNSlices(), 1, 1);
//			
//		IJ.run(brightImageStack, "Grays", "");
//		_data.showDebugImage(brightImageStack, "Original image in gray", true, null);
//
//		Roi[] nucRois=ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, nucleiRoiTag,"ROI"));
//		_data.showDebugImage(brightImageStack, "Original image in gray with nucRois", true, ROIManipulator2D.roisToOverlay(nucRois));
//		
//		ImagePlus nucleiMask=createNucleiMask(brightImageStack.getWidth(), brightImageStack.getHeight(), nucRois);
//		_data.showDebugImage(nucleiMask, "Nuclei Masks", true, null);
//		
//		drawWhiteRectangle(nucleiMask, nucleiMaskBorderWidth);
//		_data.showDebugImage(nucleiMask, "Nuclei Masks with rectangle", true, null);
//		
//		sdProjector.setImage(brightImageStack);
//		sdProjector.doProjection();
//		ImagePlus sdProjectedImage=sdProjector.getProjection();
//		_data.showDebugImage(sdProjectedImage, "SD Projected BrightFieldStack", true, null);
//		
//		ImagePlus logSdProjectedImage=sdProjectedImage.duplicate();
//		IJ.run(logSdProjectedImage, "Log", "");
//		_data.showDebugImage(logSdProjectedImage, "Log of SD Projected BrightFieldStack", true, null);
//
//		IJ.setAutoThreshold(logSdProjectedImage, "Triangle");
//		Roi backgroundRoi=thresholdToSelection.convert(logSdProjectedImage.getProcessor());
//		Roi erodedBackGroundRoi=RoiEnlarger.enlarge(backgroundRoi, -2);
//		
//		_data.showDebugImage(logSdProjectedImage, "Background Roi", true, new Overlay(backgroundRoi));
//		_data.showDebugImage(logSdProjectedImage, "Eroded Background Roi", true, new Overlay(erodedBackGroundRoi));
//		workingProcessor.saveRoiToTable(_rowIndex, erodedBackGroundRoi, backgroundRoiTag, outputFileName);
//		
//		
//		//IJ.run(logSdProjectedImage, "Make Binary", "");
//		//IJ.run(logSdProjectedImage, "Dilate", "");
//		//IJ.run(logSdProjectedImage, "Dilate", "");
//		//IJ.run(imp, "Invert", "");
//		//IJ.run(imp, "Create Selection", "");
//		
//		return;
//	}
//
	
	private ImagePlus createNucleiMask(int _imageWidth, int _imageHeight, Roi[] _nucRois){
		ByteProcessor processor=new ByteProcessor(_imageWidth, _imageWidth);
		
		if (ROIManipulator2D.isEmptyRoiArr(_nucRois))
			return new ImagePlus("Nuclei Mask Image", processor);
		
		processor.setColor(Color.WHITE);

		for (Roi roi:_nucRois)
			processor.fill(roi);
		
		return new ImagePlus("Nuclei Mask Image", processor);
	}
	
	private void drawWhiteRectangle(ImagePlus _image, int _borderWidth){
		Roi allRoi=new Roi(0,0,_image.getWidth(),_image.getHeight());
		Roi smallerRoi=RoiEnlarger.enlarge(allRoi, -_borderWidth);
		Roi frameRoi=new ShapeRoi(allRoi).not(new ShapeRoi(smallerRoi));
		
		ImageProcessor processor=_image.getProcessor();

		processor.setColor(Color.white);
		processor.fill(frameRoi);
	}
	
	private Roi[] getCellRoisFrom16BitMask(ImagePlus _countMaskImage){
		List<Roi> foundRois=new ArrayList<Roi>();
		
		ShortProcessor processor=(ShortProcessor)_countMaskImage.getProcessor();
		int maxValue=(int)processor.getStatistics().max;
		int[] histogram=processor.getHistogram();

		
		Roi extractedRoi;
		Rectangle roiBounds;
		for (int roiValue=1;roiValue<=maxValue;roiValue++){
			if (histogram[roiValue]==0)continue;
			processor.setThreshold(roiValue, roiValue, processor.getLutUpdateMode());
			extractedRoi=thresholdToSelection.convert(processor);
			if (ROIManipulator2D.isEmptyRoi(extractedRoi))continue;
			roiBounds=extractedRoi.getBounds();
			if ((roiBounds.x==0)&&(roiBounds.y==0))continue;
			foundRois.add(extractedRoi);
		}
		
		int nRois=foundRois.size();
		if (nRois==0)
			return null;
		else
			return foundRois.toArray(new Roi[nRois]);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-3,10,10,2);
	}
}
