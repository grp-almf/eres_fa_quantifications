package process.spotseg;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.plugin.Duplicator;
import ij.plugin.PlugIn;
import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.Segment3DSpots;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.utils.ThreadUtil;

import java.io.File;
import java.lang.Exception;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;

//import ah.utils.roi.ROIManipulator3D;
import automic.table.TableModel;
import automic.utils.DebugVisualiser;
import automic.utils.imagefiles.ImageOpenerWithBioformats;


public class ERESSegm_3D_TopHat implements PlugIn {
	private boolean Debug=false;
	private DebugVisualiser DebugVis=null;
	
	private static float sm_xy=1.0f;			//median filter to remove noise
	private static float sm_z=1.0f;

	private static float sm_th_xy=5.0f;		//median filter to create background
	private static float sm_th_z=3.0f;
	
	private static float lmax_xy=3.0f;									//local maximum filter to create seeds
	private static float lmax_z=2.0f;
	
	private static int locThr=0;
	private static int seedThr=25;
	private static int vMin=10;
	private static int vMax=1000;
	
	//special for Local mean Segmentation
	private static double r0=2;
	private static double r1=4;
	private static double r2=6;
	private static double wth=0.5;
	
	//special for Local Gauss segmentation
	private static int maxRad=10;
	private static double sd=1.285;
	
	private Segment3DSpots SegSpots=null;
	private ImageFloat handClean=null;
	
	
	@Override
	public void run(String arg){
		ImagePlus im=IJ.getImage();
		Debug=true;
		try{
			setFiltParams(1,1,10.0,4.0,3.0,2.0);
			this.setSegLocalGauss(maxRad, sd,vMin,vMax,seedThr,locThr);
			Objects3DPopulation Objects = segment3D(im);
				
			IJ.log("Number of segmented spots is "+ Objects.getNbObjects());
			ImagePlus segImg=new ImagePlus("Segmentation results", SegSpots.getLabelImage().getImageStack());
			showDebug(segImg);
			//ROIManipulator3D.objToRMnan3D(Objects);
			
			// test for the object centroid
			if((Objects==null)||(Objects.getNbObjects()<1))
				return;
			Object3D firstObj=Objects.getObject(0);//Objects.getObject(10);
			System.out.println("Object center of mass is: ");
			System.out.println(firstObj.getCenterAsPoint());
			System.out.println("Area is: "+firstObj.getAreaPixels());
			System.out.println("Volume is: "+firstObj.getVolumePixels());
		}
		catch (Exception ex){
			String msg=ex.getMessage();
			if(msg==null)
				msg="null";
			IJ.error("ERES segmentation: exception raised", msg);
		}
	}
	
	public ERESSegm_3D_TopHat(){
		//SegSpots=new Segment3DSpots(null,null);
		//Debug=false;
	}
	public ERESSegm_3D_TopHat(boolean _debug){
		this();
		Debug=_debug;
		
	}
	/**
	 * parameters for smoothing, background calculation and local maximum filter for seeds; all for XY and for Z
	 * @param _smXY
	 * @param _smZ
	 * @param _thXY
	 * @param _backZ
	 * @param _maxXY
	 * @param _maxZ
	 */
	public void setFiltParams(double _smXY,double _smZ, double _thXY, double _thZ, double _maxXY,double _maxZ){
		sm_xy=(float)_smXY;
		sm_z=(float)_smZ;
		
		sm_th_xy=(float)_thXY;
		sm_th_z=(float)_thZ;
		
		lmax_xy=(float)_maxXY;
		lmax_z=(float)_maxZ;
	}
	

	
	public void setSegLocalMean(double _r0,double _r1, double _r2, double _weigth, int _vmin, int _vmax, int _seedThr, int _locThr){
		SegSpots=new Segment3DSpots(null,null);
		SegSpots.setMethodLocal(Segment3DSpots.LOCAL_MEAN);
		SegSpots.setRadiusLocalMean((float)_r0, (float)_r1, (float)_r2, _weigth);
		
		SegSpots.setVolumeMin(_vmin);
		SegSpots.setVolumeMax(_vmax);
		SegSpots.setSeedsThreshold(_seedThr);
		SegSpots.setLocalThreshold(_locThr);
	}
	
	public void setSegLocalMean(){
		this.setSegLocalMean(r0, r1, r2, wth, vMin, vMax, seedThr, locThr);
	}
	

	
	public void setSegLocalGauss(int _maxRad,double _sd, int _vmin, int _vmax, int _seedThr, int _locThr){
		SegSpots=new Segment3DSpots(null,null);
		SegSpots.setMethodLocal(Segment3DSpots.LOCAL_GAUSS);
		SegSpots.setGaussMaxr(_maxRad);
		SegSpots.setGaussPc(_sd);
		
		SegSpots.setVolumeMin(_vmin);
		SegSpots.setVolumeMax(_vmax);
		SegSpots.setSeedsThreshold(_seedThr);
		SegSpots.setLocalThreshold(_locThr);
	}
	
	public void setSegLocalGauss(){
		this.setSegLocalGauss(maxRad, sd, vMin, vMax, seedThr, locThr);
	}
	
	
	protected void showDebug(ImagePlus _img, String _title){
		if (!Debug) return;
		if (DebugVis==null) DebugVis=new DebugVisualiser(1,10,10,2);
		DebugVis.addImage(_img, _title);
	}
	
	protected void showDebug(ImagePlus _img){
		if (!Debug) return;
		if (DebugVis==null) DebugVis=new DebugVisualiser(1,10,10,2);
		DebugVis.addImage(_img);
	}
	
	
	public Objects3DPopulation segment3D(ImagePlus _img, boolean _watershed) throws Exception{
		if (SegSpots==null)
			throw new Exception("Spot segmentor is not yet configured");
		
		int ncpu=ThreadUtil.getNbCpus();
		
		int ndim=_img.getNDimensions();
		if (ndim!=3) throw new Exception("Job_ERESSegm_3D.Seg3D:wrong dimensions of input image");
		int nZ=_img.getNSlices();
		if (nZ<2) throw new Exception("Job_ERESSegm_3D.Seg3D:wrong dimensions of input image");
		showDebug(_img,"Original");
		
		ImageFloat HandOrig=null;
		switch(_img.getType()){
			case ImagePlus.GRAY8:	HandOrig=new ImageByte(_img).convertToFloat(true);	break;
			case ImagePlus.GRAY16:	HandOrig=new ImageShort(_img).convertToFloat(true);	break;
			case ImagePlus.GRAY32:	HandOrig=new ImageFloat(_img);						break;
			default:
				throw new Exception("Unsupported format of input image");
		}
		
		
		ImageFloat HandSmooth=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.MEDIAN, sm_xy,sm_xy, sm_z, ncpu,false);//.filterIntImage(HandOrig, FastFilters3D.MEDIAN, sm_xy, sm_z, ncpu,false);
		ImagePlus imgSmooth=HandSmooth.getImagePlus();
		showDebug(imgSmooth,"Filtered");
		//ImageFloat HandBack=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.MEDIAN, sm_back_xy,sm_back_xy, sm_back_z, ncpu,false);//.filterIntImage(HandOrig, FastFilters3D.MEDIAN, sm_xy, sm_z, ncpu,false);
		//showDebug(HandBack.getImagePlus(),"BackGround");
		//handClean=(ImageFloat)HandSmooth.addImage(HandBack, 1, -1);
		handClean=FastFilters3D.filterFloatImage(HandSmooth, FastFilters3D.TOPHAT, sm_th_xy,sm_th_xy, sm_th_z, ncpu,false);
		showDebug(handClean.getImagePlus(),"Cleaned");
		
		//ImageFloat HandSeed=FastFilters3D.filterFloatImage(HandClean,FastFilters3D.MAXLOCAL,lmax_xy,lmax_xy,lmax_z,ncpu,false);
		ImageShort HandSeed= new ImageShort(FastFilters3D.filterFloatImage(handClean,FastFilters3D.MAXLOCAL,lmax_xy,lmax_xy,lmax_z,ncpu,false),false);
		showDebug(HandSeed.getImagePlus(),"Local maxima for seeds");
		//segmentation
		
		//SegSpots=new Segment3DSpots(null,null);//new Segment3DSpots(HandClean,HandSeed);
		SegSpots.setRawImage(handClean);
		SegSpots.setSeeds(HandSeed);
		SegSpots.show=false;//Debug;

		SegSpots.setWatershed(_watershed);
		SegSpots.segmentAll();
	
		return new Objects3DPopulation(SegSpots.getObjects());
	}
	
	public Objects3DPopulation segment3D(ImagePlus _img) throws Exception{
		return this.segment3D(_img, true);
	}
	
	public Objects3DPopulation segment2D(ImagePlus _img, boolean _watershed) throws Exception{
		if (SegSpots==null)
			throw new Exception("Spot segmentor is not yet configured");
		
		int ncpu=ThreadUtil.getNbCpus();
		if(ncpu>3) ncpu=3;
		
		int ndim=_img.getNDimensions();
		if (ndim!=2) throw new Exception("Job_ERESSegm_3D.Seg3D:wrong dimensions of input image");
		//int nZ=_img.getNSlices();
		//if (nZ<2) throw new Exception("Job_ERESSegm_3D.Seg3D:wrong dimensions of input image");
		showDebug(_img,"Original");
		ImageFloat HandOrig=new ImageByte(_img).convertToFloat(true);
		ImageFloat HandSmooth=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.MEDIAN, sm_xy,sm_xy, sm_z, ncpu,false);//.filterIntImage(HandOrig, FastFilters3D.MEDIAN, sm_xy, sm_z, ncpu,false);
		ImagePlus imgSmooth=HandSmooth.getImagePlus();
		showDebug(imgSmooth,"Filtered");
		//ImageFloat HandBack=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.MEDIAN, sm_back_xy,sm_back_xy, sm_back_z, ncpu,false);//.filterIntImage(HandOrig, FastFilters3D.MEDIAN, sm_xy, sm_z, ncpu,false);
		//showDebug(HandBack.getImagePlus(),"BackGround");
		//handClean=(ImageFloat)HandSmooth.addImage(HandBack, 1, -1);
		handClean=FastFilters3D.filterFloatImage(HandOrig, FastFilters3D.TOPHAT, sm_th_xy,sm_th_xy, sm_th_z, ncpu,false);
		showDebug(handClean.getImagePlus(),"Cleaned");
		
		//ImageFloat HandSeed=FastFilters3D.filterFloatImage(HandClean,FastFilters3D.MAXLOCAL,lmax_xy,lmax_xy,lmax_z,ncpu,false);
		ImageShort HandSeed= new ImageShort(FastFilters3D.filterFloatImage(handClean,FastFilters3D.MAXLOCAL,lmax_xy,lmax_xy,lmax_z,ncpu,false),false);
		showDebug(HandSeed.getImagePlus(),"Local maxima for seeds");
		//segmentation
		
		//SegSpots=new Segment3DSpots(null,null);//new Segment3DSpots(HandClean,HandSeed);
		SegSpots.setRawImage(handClean);
		SegSpots.setSeeds(HandSeed);
		SegSpots.show=false;//Debug;

		SegSpots.setWatershed(_watershed);
		SegSpots.segmentAll();
		
		if (Debug)
			showDebug(this.getMaskImage());
	
		return new Objects3DPopulation(SegSpots.getObjects());
	}
	
	public Objects3DPopulation segment2D(ImagePlus _img) throws Exception{
		return this.segment2D(_img,true);
	}
	
	public ImageHandler getCleanedImageHandler(){
		return handClean;
	}
	
	public ImagePlus getCleanedImagePlus(){
		return handClean.getImagePlus();
	}
	
	public ImagePlus getMaskImage(){
		return SegSpots.getLabelImage().getImagePlus();
	}
	
//	public static void Measure_3D(){
//		
//	}
	
	public static void testFile(File _Fl){
		ImagePlus img=null;
		try{
			img=ImageOpenerWithBioformats.openImage(_Fl,false);
		}catch (Exception ex){
			IJ.error("Can not open test image");
			return;
		}
		img.show();
		//Debug=true;
	}	

	public static void testFileChannel(File _Fl,int _channel){
		testFile(_Fl);
		
		ImagePlus img=IJ.getImage();
		img.hide();
		
		img=new Duplicator().run(img, _channel, _channel, 1, img.getNSlices(), 1, 1);
		img.show();
	}	
	
	
	public static void testFilePath(){
		// path to the test image
		String in_pth="D:\\tempDat\\Juan\\test-eres-segm";
		String in_fnm="Crop-Image.tif";
		testFile(new File (in_pth,in_fnm));
	}

	public static void testFilePathChannel(){
		// path to the test image
		String in_pth="C:/tempDat/VSVG experiments/14032016--newImgJobsForFRAPCargo/high zoom tests 1803";
		String in_fnm="2 chan no aver 7 slices.lsm";
		testFileChannel(new File (in_pth,in_fnm),1);
	}

	
	public static void testFileName()throws Exception{
		File targFl=null;
		String[] extensions = {"lsm"};
//		final String dirPth="C:/tempDat/2colExp/22122015-AutoFRAP-2col-control";
//		final String imFnm="gr1_TR1_1_W0001_P0017_T0001.lsm";
		final String dirPth="C:/tempDat/2colExp/Jan-Feb2016/22012016-AutoFRAP-2col-siRNA";
		final String imFnm="gr1_TR1_1_W0001_P0133_T0001.lsm";
		
		Collection<File> files = FileUtils.listFiles(new File(dirPth), extensions,true);
		for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
            File fl = iterator.next();
            if (fl.getName().equals(imFnm)){
            	targFl=fl;
            	break;
            }
        }
		
		if(targFl==null)
			IJ.log("TestFile is not found");
		else
			testFile(targFl);
	}
	
	public static void testMicTable()throws Exception{
		
		final String tblAPth="C:/tempDat/AFRAP CHX/12052016--AutoFRAP--Sec31--CXH/summary_exp1_.txt";
		final String imgTag="HZ.YFP";
		final String fileNm="exp1_TR1_1_W0001_P0012_T0001.lsm";
		
		TableModel tbl=new TableModel(new File(tblAPth));
		final int dind=tbl.getRowIndexByFileName(fileNm, imgTag, "IMG");
		testFile(tbl.getFile(dind, imgTag, "IMG"));
	}
	
	
	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args)throws Exception{
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = ERESSegm_3D_TopHat.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		// open the Clown sample
		//ImagePlus image = IJ.openImage("http://imagej.net/images/clown.jpg");
		//image.show();
		
		//if (DEBUG)
		//	ImageWindow.setNextLocation((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth()-600,120);
		
		testFilePath();
//		testFilePathChannel();
//		testFileName();
		//testMicTable();
		
		//Debug=true;
		IJ.runPlugIn(clazz.getName(), "no dialog");
	}

}
