package eres_time;

import automic.table.TableValidator;

import ij.ImageJ;

public abstract class ValidateEresSummaryFile {
	
	static String summaryTableFolder="C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Sec23-YFP--live imaging--drug/20170307--Sec23-YFP--live imaging--drug";
	static String summaryTableFileName="summary_manual.txt";

	public static String[] validateFileExtensions={"czi"};
	
	public static void main(String[] args) throws Exception{

		// start ImageJ
		new ImageJ();
		
		TableValidator.fileExtensions=validateFileExtensions;
		TableValidator.validateAll(summaryTableFolder, summaryTableFileName, summaryTableFolder);
		

	}
}
