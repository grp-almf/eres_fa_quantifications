package eres_time;

import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;

import automic.table.ManualControlFrame;
import automic.table.TableModel;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;

import ij.ImageJ;

public abstract class RunEresTimePipeline {
	
	public static PipelineInput getDefaultInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath="C:/tempDat/STC1_project_(FV)/Sec23-YFP--live imaging--drug/20170307--Sec23-YFP--live imaging--drug";
		protocolInput.subPathAnalysis ="Analysis_02";
		protocolInput.globalExperimentTableName="summary_manual.txt";
		protocolInput.startStep=1;
		protocolInput.endStep=1;
		protocolInput.startDataset=0;
		protocolInput.endDataset=1;
		
		return protocolInput;
	}
	
	public static void main(String[] args) {

		// start ImageJ
		new ImageJ();

		//create logger and set it as application logger
		Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		ApplicationLogger.setLogger(logger);
		
		// create pipeline and pipelineRunner objects
		PipelineInterface	pipelineObject;
		PipelineRunner		pipelineRunner;
		try{
			pipelineObject=new PipelineFactory(new EresTimePipeline()).getPipeline();//new Process_fixed_YFP_stacks();//new Process_FRAP_ERES_Cargo();//
			pipelineRunner=new PipelineRunner(pipelineObject, getDefaultInput());
		}catch (Exception ex){
			logger.sendExceptionMessage("Pipeline initialisation crashed", ex);
			return;
		}
		
		//run pipeline
		try{
			pipelineRunner.runPipeline();
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Pipeline execution crashed", ex);
			return;
		}
		
		//visualise results
		try{
			ManualControlFrame frame=new ManualControlFrame(new TableModel(pipelineRunner.getLastOutputTableFile()),false);
			frame.setVisible(true);
		}catch(Exception ex){
			logger.sendExceptionMessage("Result visualisation crashed", ex);
			return;
		}
	}
}
