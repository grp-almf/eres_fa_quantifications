package eres_time;

import java.util.ArrayList;
import java.util.List;

import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.cell.Step_GetStackMaxProjectionTime;
import automic.postaq.step.eres.time.Step_SegmentTrackQuantifyEres;

public class EresTimePipeline implements PredefinedPipelineSteps {
	
	@Override
	public List<RunningStep> createStepsList()throws Exception{
		
		List<RunningStep> stepList=new ArrayList<RunningStep>();
		RunningStep step;
		
		
		//create max projections
        step=new RunningStepFactory(Step_GetStackMaxProjectionTime.class).create();
        step.setInputSettingValue("Input Image Tag", "Time");
        step.setInputSettingValue("Output Image Tag", "Time.Max");
        step.setInputSettingValue("Channel Index", 1);
        
        stepList.add(step);


		//Segment track quantify ERES
        step=new RunningStepFactory(Step_SegmentTrackQuantifyEres.class).create();
        step.setInputSettingValue("Input Image Tag", "Time");
        
        stepList.add(step);

        
		return stepList;
	}
}

