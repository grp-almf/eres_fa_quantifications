package eres_time;

import java.io.File;

import automic.table.TableModel;
import automic.table.TableTools;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;

import ij.ImageJ;

public abstract class MergeProcessedReplicates {
	
	static String inOutFolderPath="C:/tempDat/STC1_project_(FV)/Sec23-YFP--live imaging--drug/summary2replicates--20170307--20170221";
	static String inputTableFileName="summary_replicates.txt";
	static String outputTableFileName="datasets_replicates.txt";
	static String replicateTableTag="Processed.Table";
	
	
	public static void main(String[] args) {

		// start ImageJ
		new ImageJ();
		
		Logger logger=new TextWindowLogger("Merge tables operation"); 
		
		//run table modification
		try{
			TableModel inputTable=new TableModel(new File(inOutFolderPath, inputTableFileName));
			TableModel outputTable=TableTools.mergeTables(inputTable, replicateTableTag, inOutFolderPath);
			outputTable.writeNewFile(outputTableFileName, true);
		}
		catch (Exception ex){
			
			logger.sendExceptionMessage("Pipeline execution crashed", ex);
			return;
		}
		logger.sendInfoMessage("Merge operation was finished and results were stored");
	}
}
