package stc1_time;

import java.io.File;


import automic.table.TableModel;
import automic.utils.FileUtils;
import automic.utils.roi.ROIManipulator2D;
import ij.ImageJ;
import ij.gui.Roi;

public abstract class RenameBackgroundRois {
	
	static String mainTableFolder="C:/tempDat/STC1_project_(FV)/STC1_overexpression--live_imaging--drug/20170509--Live-cell imaging/Analysis_20170528";
	static String mainTableFileName="analysisSummary_Step_DefineManualROIs_step4.txt";
	static String roiTag="Background.Roi";
	
	public static void main(String[] args)throws Exception {

		// start ImageJ
		new ImageJ();

		//create logger and set it as application logger
		//Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		//ApplicationLogger.setLogger(logger);

		TableModel mainTable=new TableModel(new File(mainTableFolder, mainTableFileName));
		
		int nDatasets=mainTable.getRowCount();
		File roiFile;
		Roi[] targetRois;
		for (int datasetIndex=0;datasetIndex<nDatasets;datasetIndex++){
			roiFile=mainTable.getFile(datasetIndex, roiTag, "ROI");
			targetRois=ROIManipulator2D.flsToRois(roiFile);
			if(ROIManipulator2D.isEmptyRoiArr(targetRois))
				continue;
			for (int roiIndex=0;roiIndex<targetRois.length;roiIndex++){
				targetRois[roiIndex].setName(String.format("r_%04d_%04d", datasetIndex,roiIndex));
			}
			ROIManipulator2D.saveRoisToFile(roiFile.getParent(), FileUtils.cutExtension(roiFile.getName()), targetRois);
			
		}
	}
}
