package stc1_time;

import java.util.ArrayList;
import java.util.List;

import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.stc1.Step_DefineManualROIs;
import automic.postaq.step.stc1.Step_GetTimeValues;
import automic.postaq.step.stc1.Step_ProcessStc1Time;
import automic.postaq.step.stc1.Step_QuantifyCellIntensities;
import automic.postaq.step.table.Step_ExpandSingleRoiObjects;

public class Stc1TimePipeline implements PredefinedPipelineSteps {
	
	@Override
	public List<RunningStep> createStepsList()throws Exception{
		
		List<RunningStep> stepList=new ArrayList<RunningStep>();
		RunningStep step;
		
		
		//create max projections
        step=new RunningStepFactory(Step_ProcessStc1Time.class).create();
        stepList.add(step);

        //Get time values
        step=new RunningStepFactory(Step_GetTimeValues.class).create();
        stepList.add(step);
        
        
        //define background ROIs manually
        step=new RunningStepFactory(Step_DefineManualROIs.class).create();
        step.setInputSettingValue("Input Image Tag", "Projected");
        step.setInputSettingValue("Output Rois Tag", "Background.Roi");
        step.setInputSettingValue("Dialog Message", "Define background Roi");
        step.setInputSettingValue("Roi Stroke Color", "#0000FF");
        stepList.add(step);

        
        //segment cells manually
        step=new RunningStepFactory(Step_DefineManualROIs.class).create();
        step.setInputSettingValue("Input Image Tag", "Projected");
        step.setInputSettingValue("Output Rois Tag", "Cells.Rois");
        step.setInputSettingValue("Dialog Message", "Define cell ROIs");
        step.setInputSettingValue("Roi Stroke Color", "#FFC0CB");
        stepList.add(step);
        
        
        //split single cell ROIs
        step=new RunningStepFactory(Step_ExpandSingleRoiObjects.class).create();
        step.setInputSettingValue("Grouped Rois column Tag", "Cells.Rois");
        step.setInputSettingValue("Individual Rois column Tag", "Single.Cell.Roi");
        step.setInputSettingValue("Process flag column tag", "Success");
        stepList.add(step);
        
        
        //Get Cell Intensity Values
        step=new RunningStepFactory(Step_QuantifyCellIntensities.class).create();
        step.setInputSettingValue("Input Image Tag", "Stacks.Processed");
        step.setInputSettingValue("Cell Roi Tag", "Single.Cell.Roi");
        step.setInputSettingValue("Background Roi Tag", "Background.Roi");
        step.setInputSettingValue("Output Table Tag", "Intensity.Values");
        stepList.add(step);

		
        
        return stepList;
	}
}

