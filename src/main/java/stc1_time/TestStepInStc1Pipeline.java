package stc1_time;

import java.lang.reflect.Constructor;

import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineReader;
import automic.postaq.pipeline.PipelineRunner;
import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
//import automic.postaq.pipeline.predefined.Process_FRAP_ERES;
//import automic.postaq.pipeline.predefined.Process_fixed_YFP_stacks_3D;
//import automic.postaq.pipeline.predefined.Process_fixed_YFP_stacks_steps;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
//import eres_time.EresTimePipeline;
//import flip_anita.FLIPRPipeline_MD;
//import flip_anita.FLIPRPipeline;
import ij.IJ;
import ij.ImageJ;

public abstract class TestStepInStc1Pipeline {
	static final String testDataPath			="C:/tempDat/STC1_project_(FV)/STC1_overexpression--live_imaging--drug/20170505--Live-cell imaging";
	static final String testSubPathAnalysis 	="Analysis_20170528";
	static final String testExperimentTableName	="summary_manual.txt";
	static final int	testStepIndex			=1;
	static final int	testDatasetStart		=0;
	static final int	testDatasetEnd			=1;
	static final Class<?> testPipelineClass		=Stc1TimePipeline.class;
	
	
	public static void printPipelineSteps(PipelineInterface _pipeline){
		Logger logger=ApplicationLogger.getLogger();
		
		PipelineReader pipelineReader=new PipelineReader(_pipeline);
		String[] stepNames=pipelineReader.getStepNames();
		int nSteps=stepNames.length;
		logger.sendMessage("=====================================");
		logger.sendMessage(String.format("%d steps in the tested pipeline", nSteps));
		for (int i=0;i<nSteps;i++)
			logger.sendMessage(String.format("%d: \"%s\"",i,stepNames[i]));
		if((testStepIndex<0)||(testStepIndex>=nSteps))
			throw new ArrayIndexOutOfBoundsException("Wrong test step index");

		logger.sendMessage(String.format("Tested Step: index=%d;name=\"%s\"",testStepIndex,stepNames[testStepIndex]));
		logger.sendMessage("=====================================");
	}
	
	public static PipelineInput getTestInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath=testDataPath;
		protocolInput.subPathAnalysis =testSubPathAnalysis;
		protocolInput.globalExperimentTableName=testExperimentTableName;
		protocolInput.startStep=testStepIndex;
		protocolInput.endStep=testStepIndex;
		protocolInput.startDataset=testDatasetStart;
		protocolInput.endDataset=testDatasetEnd;
		
		return protocolInput;
	}
	

	
	public static void runTest()throws Exception{
		IJ.run("Synchronize Windows", "");
		
		Constructor<?> firstConstructor=testPipelineClass.getConstructor();  
		//Constructor<?>[] constructors=testPipelineClass.getConstructors();
		Object testPipeline=(PredefinedPipelineSteps)firstConstructor.newInstance();//constructors[0].newInstance();
		PipelineInterface	pipelineObject=new PipelineFactory((PredefinedPipelineSteps)testPipeline).getPipeline();//new Process_fixed_YFP_stacks();//new Process_FRAP_ERES_Cargo();//
		
		printPipelineSteps(pipelineObject);
		
		PipelineRunner	pipelineRunner=new PipelineRunner(pipelineObject, getTestInput());
		pipelineRunner.runPipeline(true);
}
	
	
	public static void main(String[] args)throws Exception {
		// start ImageJ
		new ImageJ();

		Logger logger=new TextWindowLogger("Run Predefined pipeline test");
		ApplicationLogger.setLogger(logger);
		
		try{
			runTest();
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Crash During Pipeline Test", ex);
		}
	}
}
